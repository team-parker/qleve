// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file include/qleve/matrix_multiply.hpp
///
/// Generic interface to general matrix multiplies (GEMM)

#ifndef QLEVE_MATRIX_MULTIPLY_HPP
#define QLEVE_MATRIX_MULTIPLY_HPP

#include <algorithm>
#include <complex>
#include <utility>

#include <qleve/blas_interface.hpp>
#include <qleve/pod_types.hpp>
#include <qleve/tensor.hpp>
#include <qleve/tensor_traits.hpp>

namespace qleve
{

namespace
{

void gemm(const char* tra, const char* trb, const int m, const int n, const int k,
          const double alpha, const double* a, const int lda, const double* b, const int ldb,
          const double beta, double* c, const int ldc)
{
  dgemm_(tra, trb, m, n, k, alpha, a, lda, b, ldb, beta, c, ldc);
}

void gemm(const char* tra, const char* trb, const int m, const int n, const int k,
          const dcomplex alpha, const dcomplex* a, const int lda, const dcomplex* b, const int ldb,
          const dcomplex beta, dcomplex* c, const int ldc)
{
  zgemm_(tra, trb, m, n, k, alpha, a, lda, b, ldb, beta, c, ldc);
}

} // namespace

template <const_matrixable MatTypeA, const_matrixable MatTypeB, matrixable MatTypeC, typename T,
          typename U>
  requires(have_same_datatype_v<MatTypeA, MatTypeB, MatTypeC>
           && are_convertible_scalars_v<MatTypeA, T, U>)
void gemm(const char* tra, const char* trb, const T& alpha, const MatTypeA& A, const MatTypeB& B,
          const U& beta, MatTypeC&& C)
{
  const bool transA = (*tra == 't' || *tra == 'T');
  const bool transB = (*trb == 't' || *trb == 'T');

  const qtens_len_t ndim = transA ? A.extent(1) : A.extent(0);
  const qtens_len_t mdim = transB ? B.extent(0) : B.extent(1);
  const qtens_len_t kdim = transA ? A.extent(0) : A.extent(1);

  assert(ndim == C.extent(0));
  assert(mdim == C.extent(1));
  assert(kdim == (transB ? B.extent(1) : B.extent(0)));

  gemm(tra, trb, ndim, mdim, kdim, alpha, A.data(), A.stride(1), B.data(), B.stride(1), beta,
       C.data(), C.stride(1));
}

/// simplified interface that returns the result in newly allocated Tensor<2>

template <const_matrixable MatTypeA, const_matrixable MatTypeB, typename T>
  requires(have_same_datatype_v<MatTypeA, MatTypeB> && are_convertible_scalars_v<MatTypeA, T>)
auto gemm(const char* tra, const char* trb, const T& alpha, const MatTypeA& A, const MatTypeB& B)
{
  using U = tensor_datatype<MatTypeA>;

  const bool transA = (*tra == 't' || *tra == 'T');
  const bool transB = (*trb == 't' || *trb == 'T');

  const qtens_len_t ndim = transA ? A.extent(1) : A.extent(0);
  const qtens_len_t mdim = transB ? B.extent(0) : B.extent(1);
  const qtens_len_t kdim = transA ? A.extent(0) : A.extent(1);

  assert(kdim == (transB ? B.extent(1) : B.extent(0)));

  Tensor_<U, 2> out(ndim, mdim);

  gemm(tra, trb, ndim, mdim, kdim, alpha, A.data(), A.stride(1), B.data(), B.stride(1), 0.0,
       out.data(), out.stride(1));

  return out;
}

} // namespace qleve

#endif // QLEVE_MATRIX_MULTIPLY_HPP
