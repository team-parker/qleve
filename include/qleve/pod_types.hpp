// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file include/qleve/pod_types.hpp
///
/// Type definitions for basic plain-old-data types like qtens_len_t and
/// aliases for complex numbers

#ifndef QLEVE_POD_TYPES_HPP
#define QLEVE_POD_TYPES_HPP

#include <complex>

namespace qleve
{

using qtens_len_t = ptrdiff_t;

using scomplex = std::complex<float>;
using dcomplex = std::complex<double>;

} // namespace qleve

#endif // QLEVE_POD_TYPES_HPP
