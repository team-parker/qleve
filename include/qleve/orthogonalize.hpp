// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file include/qleve/orthogonalize.hpp
///
/// Utility algorithm to orthogonalize based on an overlap matrix

#ifndef QLEVE_ORTHOGONALIZE_HPP
#define QLEVE_ORTHOGONALIZE_HPP

#include <memory>
#include <vector>

#include <qleve/array.hpp>
#include <qleve/diagonalize.hpp>
#include <qleve/matrix_multiply.hpp>
#include <qleve/tensor.hpp>
#include <qleve/tensor_traits.hpp>
#include <qleve/vector_ops.hpp>
#include <qleve/weighted_matrix_multiply.hpp>

namespace qleve::linalg
{

/// Given a metric, S, returns a transformation that transforms
/// S into the unit matrix, and hence orthogonalizes the vectors
/// used to construct S
template <const_matrixable MatType>
qleve::Tensor_<qleve::tensor_datatype<MatType>, 2> orthogonalize_metric(
    const MatType& S, const double thresh = 1.0e-8)
{
  using MatrixT = qleve::Tensor_<qleve::tensor_datatype<MatType>, 2>;

  const auto [ndim, mdim] = S.shape();
  assert(ndim == mdim);

  // simplest thing to start with is symmetric orthogonalization (i.e., Lowdin)
  MatrixT U(S);

  // normalize diagonals to 1 for improved numerical stability
  qleve::Array norms(ndim);
  for (size_t i = 0; i < ndim; ++i) {
    const double nrm = std::abs(S(i, i));
    norms(i) = nrm > thresh ? 1.0 / std::sqrt(nrm) : 1.0;
  }

  for (size_t j = 0; j < mdim; ++j) {
    U.pluck(j) *= norms * norms(j);
  }

  // diagonalize U
  auto eigs = qleve::linalg::diagonalize(U);

  // assume first element is smallest
  if (eigs(0) > thresh) { // do symmetric orthogonalization
    eigs = 1.0 / qleve::sqrt(eigs);

    MatrixT out(U.extent(0), U.extent(1));
    qleve::weighted_gemm("n", "t", 1.0, U, eigs, U, 0.0, out);

    for (size_t j = 0; j < mdim; ++j) {
      out.pluck(j) *= norms;
    }

    return out;
  } else { // do canonical orthogonalization
    throw std::runtime_error("Canonical Orthogonalization Not yet implemented!");
  }
}

/// Given a set of vectors, orthogonalizes the vectors in place.
/// Returns the number of linearly independent vectors.
template <matrixable MatType>
ptrdiff_t orthogonalize_vectors(MatType&& C, const double thresh = 1.0e-12)
{
  using MatrixT = qleve::Tensor_<qleve::tensor_datatype<MatType>, 2>;

  const auto [ndim, mdim] = C.shape();

  // normalize all of the columns
  for (size_t j = 0; j < mdim; ++j) {
    auto col = C.pluck(j);
    col /= col.norm();
  }

  ptrdiff_t j = 0;       // current counter
  ptrdiff_t ntot = mdim; // current known number of linearly independent vectors
  MatrixT inner(1, mdim);

  // start orthogonalizing
  for (ptrdiff_t jvec = 0; jvec < mdim; ++jvec) {
    auto col = C.slice(j, j + 1);

    // normalize column
    const double nrm = col.norm();
    if (nrm < thresh) {
      // slide all the remaining columns down
      for (ptrdiff_t k = j; k < ntot - 1; ++k) {
        C.pluck(k) = C.pluck(k + 1);
      }
      ntot -= 1;
      continue;
    }
    col /= nrm;

    ptrdiff_t nrest = ntot - j - 1;
    if (nrest > 0) { // project this vector out from remaining
      auto rest = C.slice(j + 1, ntot);
      auto tmp = inner.subtensor({0, 0}, {1, ntot - j - 1});
      qleve::gemm("t", "n", 1.0, col, rest, 0.0, tmp);
      qleve::gemm("n", "n", -1.0, col, tmp, 1.0, rest);
    }
    j += 1;
  }

  return ntot;
}

} // namespace qleve::linalg

#endif // QLEVE_ORTHOGONALIZE_HPP
