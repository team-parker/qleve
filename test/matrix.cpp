#include <sstream>

#include "catch.hpp"
#include "util.hpp"

#include <qleve/checks.hpp>
#include <qleve/cholesky.hpp>
#include <qleve/determinant.hpp>
#include <qleve/diagonalize.hpp>
#include <qleve/matrix_multiply.hpp>
#include <qleve/matrix_phase_adjust.hpp>
#include <qleve/matrix_transform.hpp>
#include <qleve/svd.hpp>
#include <qleve/tensor.hpp>
#include <qleve/tensor_contract.hpp>
#include <qleve/tensor_traits.hpp>
#include <qleve/trace.hpp>
#include <qleve/weighted_matrix_multiply.hpp>

using namespace std;
using namespace qleve;

TEST_CASE("Matrix Linear Algebra", "[matrix]")
{
  Tensor<2> A(2, 2);
  A(0, 0) = 0.0;
  A(0, 1) = A(1, 0) = 0.5;
  A(1, 1) = 4.0;

  Tensor<2> Aref(A);

  REQUIRE(A.size() == 4);
  REQUIRE(A.shape()[0] == 2);
  REQUIRE(A.shape()[1] == 2);

  SECTION("Tensor2 objects are matrixlike")
  {
    CHECK(is_nonconst_matrixlike<Tensor<2>>::value);
    CHECK(is_nonconst_matrixlike<TensorView<2>>::value);
    CHECK(is_const_matrixlike<ConstTensorView<2>>::value);
    CHECK(is_nonconst_matrixlike<SubTensor<2>>::value);
    CHECK(is_const_matrixlike<ConstSubTensor<2>>::value);
  }

  SECTION("print_tensor produces correct output")
  {
    std::stringstream ss;
    A.print_tensor(ss, "Test Matrix", A.extent(0));
    std::string ref_output = " --- Test Matrix: [2, 2] ---\n"
                             "               0             0.5\n"
                             "             0.5               4\n";
    CHECK(ss.str() == ref_output);
  }

  SECTION("traits")
  {
    CHECK(have_same_datatype<Tensor<2>, TensorView<2>, SubTensor<2>>::value);
    CHECK(!have_same_datatype<Tensor<2>, ZTensor<2>>::value);
  }

  SECTION("view functions as alias")
  {
    TensorView<2> aview(A);
    aview *= 2.0;

    CHECK(A(0, 0) == aview(0, 0));
    CHECK(A(0, 1) == aview(0, 1));
    CHECK(A(1, 0) == aview(1, 0));
    CHECK(A(1, 1) == aview(1, 1));

    aview = 0.0;

    REQUIRE(A.norm() == 0.0);

    ConstTensorView<2> acview(aview);

    REQUIRE(acview.norm() == 0.0);

    ConstTensorView<2> acview2(acview);

    REQUIRE(acview2.norm() == 0.0);
  }

  SECTION("view can be rhs")
  {
    auto aview = A.view();

    Tensor<2> B = qleve::cos(std::acos(-1) * aview);

    REQUIRE(B.norm() == Approx(1.4142135624));
  }

  SECTION("can copy construct")
  {
    Tensor<2> B(A);

    REQUIRE(A.norm() == Approx(B.norm()));
  }

  SECTION("can fill")
  {
    A = 2.0;

    REQUIRE(A.norm() == Approx(4.0));
  }

  SECTION("can copy")
  {
    Tensor<2> B(2, 2);
    B = A;

    REQUIRE(A.norm() == Approx(B.norm()));
  }

  SECTION("can create matrix from template expression")
  {
    Tensor<2> B(2, 2);
    B(0, 0) = 10.0;
    B(0, 1) = 12.0;
    B(1, 0) = 14.0;
    B(1, 1) = 16.0;

    Tensor<2> C = 2.0 * A - B / 2.0;

    std::vector<double> ref_result = {-5.0, -6.0, -5.0, 0.0};
    REQUIRE(indexable_equals(C, ref_result));
  }

  SECTION("can reshape")
  {
    auto b = A.reshape(4);

    CHECK(b.at(2) == A.at(0, 1));
  }

  SECTION("can const reshape")
  {
    auto b = A.const_reshape(4);

    CHECK(b.at(2) == A.at(0, 1));
  }

  SECTION("zeros_like")
  {
    Tensor<2> B = A.zeros_like();

    REQUIRE(B.size() == A.size());
    REQUIRE(B.norm() == 0.0);
  }

  SECTION("can make const view")
  {
    ConstTensorView<2> aview(A);

    REQUIRE(aview(0, 1) == A(0, 1));
    REQUIRE(aview.at(0, 1) == A.at(0, 1));
  }

  SECTION("antisymmetrizing gives zero")
  {
    A = A - A.transpose();
    CHECK(A.norm() == Approx(0.0));
  }

  SECTION("transform with swapping matrix")
  {
    Tensor<2> U(A.shape());
    U(0, 1) = U(1, 0) = 1.0;

    Tensor<2> C(A.shape());

    matrix_transform(2.0, A, U, 0.0, C);

    CHECK(C(0, 0) == Approx(2.0 * A(1, 1)));
    CHECK(C(0, 1) == Approx(2.0 * A(1, 0)));
    CHECK(C(1, 0) == Approx(2.0 * A(0, 1)));
    CHECK(C(1, 1) == Approx(2.0 * A(0, 0)));
  }

  SECTION("transform with view and subtensor")
  {
    Tensor<2> U(A.shape());
    U(0, 1) = U(1, 0) = 1.0;

    Tensor<2> C(4, 4);

    matrix_transform(2.0, A.const_view(), U.view(), 0.0, C.subtensor({1, 1}, {3, 3}));

    CHECK(C(1, 1) == Approx(2.0 * A(1, 1)));
    CHECK(C(1, 2) == Approx(2.0 * A(1, 0)));
    CHECK(C(2, 1) == Approx(2.0 * A(0, 1)));
    CHECK(C(2, 2) == Approx(2.0 * A(0, 0)));
  }

  SECTION("transposed transform")
  {
    Tensor<2> U(4, 2);
    U(0, 1) = 1.0;
    U(1, 0) = -1.0;

    Tensor<2> C(4, 4);
    matrix_transform("t", 1.0, A, U, 0.0, C);

    CHECK(C(0, 0) == Approx(A(1, 1)));
    CHECK(C(1, 0) == Approx(-A(0, 1)));
    CHECK(C(2, 2) == Approx(0.0));
  }

  SECTION("nonsymmetric transform")
  {
    Tensor<2> U1(2, 2);
    U1(0, 1) = 1.0;
    U1(1, 0) = -1.0;

    Tensor<2> U2(2, 1);
    U2(0, 0) = 1.0;
    U2(1, 0) = -1.0;

    Tensor<2> C(2, 1);
    matrix_transform(1.0, U1, A, U2, 0.0, C);

    CHECK(C(0, 0) == Approx(A(1, 1) - A(1, 0)));
    CHECK(C(1, 0) == Approx(A(0, 0) - A(0, 1)));

    Tensor<2> D(1, 2);
    matrix_transform(1.0, U2, A, U1, 0.0, D);

    CHECK(D(0, 0) == Approx(A(1, 1) - A(0, 1)));
    CHECK(D(0, 1) == Approx(A(0, 0) - A(1, 0)));
  }

  SECTION("most general transform")
  {
    Tensor<2> U1(2, 2);
    U1(0, 1) = 1.0;
    U1(1, 0) = -1.0;

    Tensor<2> U2(2, 1);
    U2(0, 0) = 1.0;
    U2(1, 0) = -1.0;

    Tensor<2> C(2, 1);
    matrix_transform("t", "n", 1.0, U1, A, U2, 0.0, C);

    CHECK(C(0, 0) == Approx(A(1, 1) - A(1, 0)));
    CHECK(C(1, 0) == Approx(A(0, 0) - A(0, 1)));

    Tensor<2> D(1, 2);
    matrix_transform("t", "n", 1.0, U2, A, U1, 0.0, D);

    CHECK(D(0, 0) == Approx(A(1, 1) - A(0, 1)));
    CHECK(D(0, 1) == Approx(A(0, 0) - A(1, 0)));
  }

  SECTION("a general stacked transform")
  {
    Tensor<3> A3(2, 2, 2);
    A3.pluck(0) = A;
    A3.pluck(1) = A;

    Tensor<3> U(2, 2, 2);
    U(0, 1, 0) = 1.0;
    U(1, 0, 0) = -1.0;

    U(0, 0, 1) = 1.0;
    U(1, 0, 1) = -1.0;

    Tensor<3> C(2, 2, 2);
    matrix_transform("t", "n", 1.0, U, A3, U, 0.0, C);

    CHECK(C(0, 0, 0) == Approx(A(1, 1)));
    CHECK(C(1, 0, 0) == Approx(-A(0, 1)));
    CHECK(C(0, 1, 0) == Approx(-A(1, 0)));
    CHECK(C(1, 1, 0) == Approx(A(0, 0)));

    CHECK(C(0, 0, 1) == Approx(A(0, 0) + A(1, 1) - A(0, 1) - A(1, 0)));
    CHECK(C(0, 1, 1) == Approx(0.0));
    CHECK(C(1, 0, 1) == Approx(0.0));
    CHECK(C(1, 1, 1) == Approx(0.0));
  }

  SECTION("slice exposes a view")
  {
    TensorView<2> aslice = A.slice(1, 2);

    CHECK(aslice.shape()[0] == 2);
    CHECK(aslice.shape()[1] == 1);
  }

  SECTION("slice can copy to slice")
  {
    A.slice(0, 1) = A.slice(1, 2);

    CHECK(A(0, 0) == A(0, 1));
    CHECK(A(1, 0) == A(1, 1));
  }

  SECTION("subtensor exposes a view")
  {
    SubTensor<2> submat = A.subtensor({0, 1}, {2, 2});

    CHECK(submat(0, 0) == A(0, 1));
    CHECK(submat(1, 0) == A(1, 1));
  }

  SECTION("subtensor can scale")
  {
    SubTensor<2> submat = A.subtensor({0, 0}, {2, 1});
    submat *= 2.0;

    CHECK(A(0, 0) == Approx(2.0 * Aref(0, 0)));
    CHECK(A(1, 0) == Approx(2.0 * Aref(1, 0)));
    CHECK(A(0, 1) == Approx(1.0 * Aref(0, 1)));
    CHECK(A(1, 1) == Approx(1.0 * Aref(1, 1)));
  }

  SECTION("subtensor on RHS")
  {
    A *= A.subtensor({0, 0}, {2, 2});

    CHECK(A(0, 0) == Approx(Aref(0, 0) * Aref(0, 0)));
    CHECK(A(1, 0) == Approx(Aref(1, 0) * Aref(1, 0)));
    CHECK(A(0, 1) == Approx(Aref(0, 1) * Aref(0, 1)));
    CHECK(A(1, 1) == Approx(Aref(1, 1) * Aref(1, 1)));
  }

  SECTION("subtensor = subtensor")
  {
    A.subtensor({0, 0}, {2, 1}) = A.subtensor({0, 1}, {2, 2});

    CHECK(A(0, 0) == A(0, 1));
    CHECK(A(1, 0) == A(1, 1));
  }

  SECTION("const subtensor on RHS")
  {
    auto aview = A.const_view();
    A *= aview.const_subtensor({0, 0}, {2, 2});

    CHECK(A(0, 0) == Approx(Aref(0, 0) * Aref(0, 0)));
    CHECK(A(1, 0) == Approx(Aref(1, 0) * Aref(1, 0)));
    CHECK(A(0, 1) == Approx(Aref(0, 1) * Aref(0, 1)));
    CHECK(A(1, 1) == Approx(Aref(1, 1) * Aref(1, 1)));
  }

  SECTION("construct from subtensor")
  {
    Tensor<2> B = A.subtensor({0, 0}, {2, 1});

    CHECK(B.extent(0) == 2);
    CHECK(B.extent(1) == 1);
  }

  SECTION("multiply using subtensor")
  {
    Tensor<2> C(1, 1);

    SubTensor<2> arow = A.subtensor({1, 0}, {2, 2});

    CHECK(arow.extent(0) == 1);
    CHECK(arow.extent(1) == 2);
    CHECK(arow(0, 0) == A(1, 0));
    CHECK(arow(0, 1) == A(1, 1));

    qleve::gemm("N", "T", 1.0, arow, arow, 0.0, C);

    CHECK(C(0, 0) == A(1, 0) * A(1, 0) + A(1, 1) * A(1, 1));
  }

  SECTION("multiply onto subtensor")
  {
    Tensor<2> C(A);

    qleve::gemm("T", "N", 1.0, A.subtensor({0, 0}, {2, 1}), A.subtensor({0, 0}, {2, 1}), 1.0,
                C.subtensor({1, 1}, {2, 2}));

    CHECK(C(0, 0) == A(0, 0));
    CHECK(C(1, 1) == A(1, 1) + A(0, 0) * A(0, 0) + A(1, 0) * A(1, 0));
  }

  SECTION("multiply with allocation")
  {
    auto C = qleve::gemm("t", "n", 1.0, A, A);

    CHECK(C(0, 0) == Approx(A(0, 0) * A(0, 0) + A(1, 0) * A(1, 0)));
    CHECK(C(0, 1) == Approx(A(0, 0) * A(0, 1) + A(1, 0) * A(1, 1)));
    CHECK(C(1, 0) == Approx(A(0, 1) * A(0, 0) + A(1, 1) * A(1, 0)));
  }

  SECTION("assign to subtensor")
  {
    Tensor<2> B(2, 1);

    A.subtensor({0, 0}, {2, 1}) = B;

    CHECK(A(0, 0) == B(0, 0));
  }

  SECTION("multiply with diagonal matrix")
  {
    Tensor<2> B(A);
    Tensor<1> d(A.extent(0));
    d(0) = 0.5;
    d(1) = -1.0;

    Tensor<2> C = A.zeros_like();

    qleve::weighted_gemm("n", "n", 1.0, A, d, B, 0.0, C);

    CHECK(C(0, 0) == Approx(A(0, 0) * d(0) * B(0, 0) + A(0, 1) * d(1) * B(1, 0)));
    CHECK(C(0, 1) == Approx(A(0, 0) * d(0) * B(0, 1) + A(0, 1) * d(1) * B(1, 1)));
    CHECK(C(1, 0) == Approx(A(1, 0) * d(0) * B(0, 0) + A(1, 1) * d(1) * B(1, 0)));
    CHECK(C(1, 1) == Approx(A(1, 0) * d(0) * B(0, 1) + A(1, 1) * d(1) * B(1, 1)));
  }

  SECTION("contract as multiply")
  {
    Tensor<2> C(A);
    qleve::contract(1.0, A, "pq", A, "qr", 0.0, C, "pr");

    CHECK(C(0, 0) == Approx(A(0, 0) * A(0, 0) + A(0, 1) * A(1, 0)));
    CHECK(C(1, 0) == Approx(A(1, 0) * A(0, 0) + A(1, 1) * A(1, 0)));
    CHECK(C(0, 1) == Approx(A(0, 0) * A(0, 1) + A(0, 1) * A(1, 1)));
    CHECK(C(1, 1) == Approx(A(1, 0) * A(0, 1) + A(1, 1) * A(1, 1)));
  }

  SECTION("can make a tensor view")
  {
    TensorView<2> aview(A.data(), A.shape());

    CHECK(A(0, 0) == aview(0, 0));
    CHECK(A(1, 0) == aview(1, 0));
    CHECK(A(0, 1) == aview(0, 1));
    CHECK(A(1, 1) == aview(1, 1));
  }

  SECTION("can make a const tensor view")
  {
    ConstTensorView<2> aview(A.data(), A.shape());

    CHECK(A(0, 0) == aview(0, 0));
    CHECK(A(1, 0) == aview(1, 0));
    CHECK(A(0, 1) == aview(0, 1));
    CHECK(A(1, 1) == aview(1, 1));
  }

  SECTION("can symmetrize views")
  {
    TensorView<2> Aview(A);

    Aview = Aview - Aview.transpose();

    const double anorm = Aview.norm();
    CHECK(anorm == Approx(0.0).margin(1e-13));

    ConstTensorView<2> Acview(A);

    Aview = Acview - Acview.transpose();
    const double anormc = Aview.norm();
    CHECK(anormc == Approx(0.0).margin(1e-13));
  }

  SECTION("can symmetrize subtensors")
  {
    Tensor<2> B(8, 8);
    B.subtensor({0, 0}, {2, 2}) = A;
    B.subtensor({2, 2}, {4, 4}) = A;
    B.subtensor({4, 4}, {6, 6}) = A;
    B.subtensor({6, 6}, {8, 8}) = A;

    SubTensor<2> Bsub = B.subtensor({2, 2}, {6, 6});

    Bsub = Bsub - Bsub.transpose();
    const double bnormsub = Bsub.norm();
    CHECK(bnormsub == Approx(0.0).margin(1e-13));

    B.subtensor({2, 2}, {4, 4}) = A;
    B.subtensor({4, 4}, {6, 6}) = A;
    ConstSubTensor<2> Bcsub = B.const_subtensor({2, 2}, {6, 6});

    Bsub = Bcsub - Bcsub.transpose();
    const double bnormcsub = Bsub.norm();
    CHECK(bnormcsub == Approx(0.0).margin(1e-13));
  }

  SECTION("adjust phase of real matrix")
  {
    Tensor<2> B = A;
    B.pluck(0) *= -1.0;
    B.pluck(1) *= -1.0;

    qleve::phase_adjust(B);
    CHECK(A(0, 0) == Approx(B(0, 0)));
    CHECK(A(0, 1) == Approx(B(0, 1)));
    CHECK(A(1, 0) == Approx(B(1, 0)));
    CHECK(A(1, 1) == Approx(B(1, 1)));
  }

  SECTION("math with diagonal")
  {
    Tensor<2> B = A;
    B.diagonal() = -1.0;

    CHECK(B(0, 0) == -1.0);
    CHECK(B(1, 1) == -1.0);
    CHECK(B(0, 1) == A(0, 1));
    CHECK(B(1, 0) == A(1, 0));

    B.diagonal() = 1.0;
    B.diagonal() *= 2.0;

    CHECK(B(0, 0) == 2.0);
    CHECK(B(1, 1) == 2.0);
    CHECK(B(0, 1) == A(0, 1));
    CHECK(B(1, 0) == A(1, 0));

    B.diagonal() = A.diagonal();

    CHECK(B(0, 0) == A(0, 0));
    CHECK(B(1, 1) == A(1, 1));
    CHECK(B(0, 1) == A(0, 1));
    CHECK(B(1, 0) == A(1, 0));

    B.diagonal() += A.diagonal();

    CHECK(B(0, 0) == 2.0 * A(0, 0));
    CHECK(B(1, 1) == 2.0 * A(1, 1));
    CHECK(B(0, 1) == A(0, 1));
    CHECK(B(1, 0) == A(1, 0));

    B.diagonal() -= A.diagonal();

    CHECK(B(0, 0) == A(0, 0));
    CHECK(B(1, 1) == A(1, 1));
    CHECK(B(0, 1) == A(0, 1));
    CHECK(B(1, 0) == A(1, 0));

    B.diagonal() *= A.diagonal();

    CHECK(B(0, 0) == A(0, 0) * A(0, 0));
    CHECK(B(1, 1) == A(1, 1) * A(1, 1));
    CHECK(B(0, 1) == A(0, 1));
    CHECK(B(1, 0) == A(1, 0));

    B.diagonal() = A.diagonal() + 1.0;

    CHECK(B(0, 0) == A(0, 0) + 1.0);
    CHECK(B(1, 1) == A(1, 1) + 1.0);
    CHECK(B(0, 1) == A(0, 1));
    CHECK(B(1, 0) == A(1, 0));

    B.diagonal() = A.view().diagonal();

    CHECK(B(0, 0) == A(0, 0));
    CHECK(B(1, 1) == A(1, 1));
    CHECK(B(0, 1) == A(0, 1));
    CHECK(B(1, 0) == A(1, 0));

    B.diagonal() = -10.0;
    B.diagonal() = A.view().const_diagonal();
    CHECK(B(0, 0) == A(0, 0));
    CHECK(B(1, 1) == A(1, 1));

    B.diagonal() = -10.0;
    B.diagonal() = A.const_view().const_diagonal();
    CHECK(B(0, 0) == A(0, 0));
    CHECK(B(1, 1) == A(1, 1));

    B.diagonal() = A.diagonal();
    const double bdnorm = B.diagonal().norm();
    CHECK(bdnorm == Approx(std::sqrt(A(0, 0) * A(0, 0) + A(1, 1) * A(1, 1))));
  }

  SECTION("pop left is a row")
  {
    Tensor<2> B = A;
    auto B0 = B.pop_left(1);

    REQUIRE(B0.rank() == 1);
    CHECK(B0.extent(0) == 2);
    B0 *= 2.0;

    CHECK(B0(0) == Approx(2.0 * A(1, 0)));
    CHECK(B0(1) == Approx(2.0 * A(1, 1)));
  }

  SECTION("const pop left is a const row")
  {
    auto A0 = A.const_pop_left(0);

    REQUIRE(A0.rank() == 1);
    CHECK(A0.extent(0) == 2);

    CHECK(A0(0) == A(0, 0));
    CHECK(A0(1) == A(0, 1));
  }
}
