// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file include/qleve/diagonalize.hpp
///
/// Generic interface to Hermitian diagonalization of MatrixLike objects

#ifndef QLEVE_DIAGONALIZE_HPP
#define QLEVE_DIAGONALIZE_HPP

#include <vector>

#include <qleve/blas_interface.hpp>
#include <qleve/pod_types.hpp>
#include <qleve/tensor.hpp>
#include <qleve/tensor_traits.hpp>

namespace qleve::linalg
{

void diagonalize(real_matrixable auto&& A, double* const eval)
{
  assert(A.extent(0) == A.extent(1));

  const ptrdiff_t n = A.extent(1);

  if (n == 0)
    return;

  std::unique_ptr<double[]> work(new double[n * 6]);
  int info = dsyev_("V", "L", n, A.data(), A.stride(1), eval, work.get(), n * 6);

  if (info)
    throw std::runtime_error("dsyev failed");
}

Tensor<1> diagonalize(real_matrixable auto&& A)
{
  assert(A.extent(0) == A.extent(1));
  Tensor<1> out(A.extent(0));

  diagonalize(A, out.data());
  return out;
}

void diagonalize_blocks(real_matrixable auto&& A, const std::vector<ptrdiff_t>& blocksizes,
                        double* eval)
{
  assert(A.extent(0) == A.extent(1));
  assert(A.extent(0) == std::accumulate(blocksizes.begin(), blocksizes.end(), 0ll));

  ptrdiff_t ioff = 0;
  for (ptrdiff_t iblock : blocksizes) {
    if (iblock == 0)
      continue;

    Tensor<2> block = A.subtensor({ioff, ioff}, {ioff + iblock, ioff + iblock});
    diagonalize(block, eval);

    A.slice(ioff, ioff + iblock) = 0.0;
    A.subtensor({ioff, ioff}, {ioff + iblock, ioff + iblock}) = block;

    eval += iblock;
    ioff += iblock;
  }
}

Tensor<1> diagonalize_blocks(real_matrixable auto&& A, const std::vector<ptrdiff_t>& blocksizes)
{
  Tensor<1> out(A.extent(0));

  diagonalize_blocks(A, blocksizes, out.data());

  return out;
}

} // namespace qleve::linalg

#endif // QLEVE_DIAGONALIZE_HPP
