// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file include/qleve/pivoted_lu.hpp
///
/// Interface to Lapack pivoted LU decomposition

#ifndef QLEVE_PIVOTED_LU_HPP
#define QLEVE_PIVOTED_LU_HPP

#include <vector>

#include <qleve/blas_interface.hpp>
#include <qleve/tensor_traits.hpp>

namespace qleve::linalg
{

std::vector<blasint_t> pivoted_lu(real_matrixable auto&& A)
{
  const auto [ndim, mdim] = A.shape();

  const ptrdiff_t min_nm = std::min(ndim, mdim);
  std::vector<blasint_t> ipiv(min_nm);

  const int info = dgetrf_(ndim, mdim, A.data(), A.stride(1), ipiv.data());
  return ipiv;
}

} // namespace qleve::linalg

#endif // QLEVE_PIVOTED_LU_HPP
