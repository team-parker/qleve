// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file include/qleve/rotate.hpp
///
/// Generic interface to rotate two arrays
///   x, y <- c x + s y, c y - sx
/// for x, y vectors and c, s constants

#ifndef QLEVE_ROTATE_HPP
#define QLEVE_ROTATE_HPP

#include <array>
#include <cassert>
#include <cmath>
#include <type_traits>

#include <qleve/blas_interface.hpp>
#include <qleve/pod_types.hpp>

namespace qleve
{

namespace
{
void rotate(const ptrdiff_t& n, double* x, const ptrdiff_t& incx, double* y, const ptrdiff_t& incy,
            const double& c, const double& s)
{
  drot_(n, x, incx, y, incy, c, s);
}
} // namespace

// requires that X and Y are the same type and that c * X is still of type X
template <typename X, typename Y>
  requires requires(X xx, Y yy, const double c) {
    {
      c* xx
    } -> std::same_as<X>;
    {
      c* yy
    } -> std::same_as<Y>;
  }
void rotate(const ptrdiff_t& n, X* x, const ptrdiff_t& incx, Y* y, const ptrdiff_t& incy,
            const double& c, const double& s)
{
  assert(incx > 0 && incy > 0);

  for (ptrdiff_t i = 0; i < n; ++i) {
    const auto ix = i * incx;
    const auto iy = i * incy;

    const auto xtmp = c * x[ix] + s * y[iy];
    y[iy] = -s * x[ix] + c * y[iy];
    x[ix] = xtmp;
  }
}

template <typename X, typename Y>
  requires requires(X xx, Y yy, const double c) {
    {
      c* xx
    } -> std::same_as<X>;
    {
      c* yy
    } -> std::same_as<Y>;
  }
void rotate(const ptrdiff_t& n, X* x, Y* y, const double& theta)
{
  rotate(n, x, 1ll, y, 1ll, std::cos(theta), std::sin(theta));
}

} // namespace qleve

#endif // QLEVE_ROTATE_HPP
