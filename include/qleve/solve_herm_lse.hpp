// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file include/qleve/solve_herm_lse.hpp
///
/// Interface to Lapack LSE solve with symmetric/hermitian matrix

#ifndef QLEVE_SOLVE_HERM_LSE_HPP
#define QLEVE_SOLVE_HERM_LSE_HPP

#include <qleve/blas_interface.hpp>
#include <qleve/tensor_traits.hpp>

namespace qleve::linalg
{

auto solve_herm_lse(const const_real_matrixable auto& A, const const_real_matrixable auto& B)
{
  const ptrdiff_t ndim = A.extent(0);
  const ptrdiff_t nrhs = B.extent(1);
  assert(ndim == A.extent(1) && ndim == B.extent(0));

  double workval = 0.0;
  std::vector<blasint_t> ipiv(ndim);

  auto Acp = A.clone();
  auto out = B.clone();

  // lwork size query
  dsysv_("L", ndim, nrhs, Acp.data(), Acp.stride(1), ipiv.data(), out.data(), out.stride(1),
         &workval, -1);

  const blasint_t lwork = static_cast<blasint_t>(workval);
  qleve::Tensor<1> work(lwork);
  const int info = dsysv_("L", ndim, nrhs, Acp.data(), Acp.stride(1), ipiv.data(), out.data(),
                          out.stride(1), work.data(), lwork);

  return out;
}

} // namespace qleve::linalg

#endif // QLEVE_SOLVE_HERM_LSE_HPP
