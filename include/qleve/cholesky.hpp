// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file include/qleve/cholesky.hpp
///
/// Generic interface to cholesky decomposition for MatrixLike objects

#ifndef QLEVE_CHOLESKY_HPP
#define QLEVE_CHOLESKY_HPP

#include <tuple>

#include <qleve/blas_interface.hpp>
#include <qleve/pod_types.hpp>
#include <qleve/tensor.hpp>
#include <qleve/tensor_traits.hpp>

namespace qleve::linalg
{

void cholesky(real_matrixable auto&& A)
{
  const auto [ndim, mdim] = A.shape();
  assert(ndim == mdim);

  int info = dpotrf_("L", ndim, A.data(), A.stride(1));
}

void cholesky_solve(const char* leftright, const char* lowerupper, const char* opL,
                    const char* diag, const const_real_matrixable auto& L, real_matrixable auto&& B)
{
  assert(*leftright == 'l' || *leftright == 'L' || *leftright == 'r' || *leftright == 'R');
  assert(*lowerupper == 'l' || *lowerupper == 'L' || *lowerupper == 'u' || *lowerupper == 'U');
  assert(*opL == 'n' || *opL == 'N' || *opL == 't' || *opL == 'T');
  assert(*diag == 'n' || *diag == 'N' || *diag == 'u' || *diag == 'U');

  // warning, this does no check that L is indeed lower triangular
  const auto [mdim, ndim] = B.shape();

  assert((*leftright == 'l' || *leftright == 'L') ? L.extent(1) == mdim : L.extent(1) == ndim);

  dtrsm_(leftright, lowerupper, opL, diag, mdim, ndim, 1.0, L.data(), L.stride(1), B.data(),
         B.stride(1));
}

} // namespace qleve::linalg

#endif // QLEVE_CHOLESKY_HPP
