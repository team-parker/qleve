// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file include/qleve/tblis_interface.hpp
///
/// Interface for TBLIS tensors

#ifndef QLEVE_TBLIS_INTERFACE_HPP
#define QLEVE_TBLIS_INTERFACE_HPP

#include <algorithm>
#include <complex>
#include <utility>

#include <qleve/pod_types.hpp>
#include <qleve/trait_util.hpp>

// TBLIS_DONT_USE_CXX11 was a previous workaround to avoid some name clash
// somewhere, but as of release 1.2 doesn't seem necessary. Keeping it
// here for now anyways.
//#define TBLIS_DONT_USE_CXX11
#include <tblis/tblis.h>

// Enable this to forcably copy stride/shape data into a new vector
// with correct data types
//#define USE_SAFE_TBLIS_CONVERSION

namespace qleve
{

namespace
{
template <typename tblis_type, int N>
std::array<tblis_type, N> copy_array(const std::array<qtens_len_t, N>& arr)
{
  std::array<tblis_type, N> out;
  std::copy(arr.begin(), arr.end(), out.begin());
  return out;
}
} // namespace

template <typename DataType>
struct tblis_vector_wrap_ {
#ifdef USE_SAFE_TBLIS_CONVERSION
  std::array<tblis::len_type, 1> shape_;
  std::array<tblis::stride_type, 1> stride_;
#endif

  tblis::tblis_vector tblis_vector_;

  template <class ArrayType, typename T = typename ArrayType::DataType>
  tblis_vector_wrap_(const bool& conj, const T& scalar, ArrayType& v)
#ifdef USE_SAFE_TBLIS_CONVERSION
      :
      shape_(copy_array<tblis::len_type, 1>(v.shape())),
      stride_(copy_array<tblis::stride_type, 1>(v.stride()))
#endif
  {
    static_assert(is_any<DataType, float, double, scomplex, dcomplex>::value,
                  "TBLIS wrapper only accepts float, double, complex<float>, complex<double>");
#ifdef USE_SAFE_TBLIS_CONVERSION
    auto shape_data = shape_.data();
    auto stride_data = stride_.data();
#else
    static_assert(
        std::is_same<tblis::len_type, qtens_len_t>::value
            && std::is_same<tblis::stride_type, qtens_len_t>::value,
        "qleve and TBLIS want different data types. Turn on USE_SAFE_TBLIS_CONVERSION macro.");
    auto shape_data = const_cast<tblis::len_type*>(v.shape().data());
    auto stride_data = const_cast<tblis::stride_type*>(v.stride().data());
#endif

    if constexpr (std::is_same<DataType, float>::value) {
      tblis::tblis_init_vector_scaled_s(&tblis_vector_, scalar, shape_data[0], v.data(),
                                        stride_data[0]);
    } else if constexpr (std::is_same<DataType, double>::value) {
      tblis::tblis_init_vector_scaled_d(&tblis_vector_, scalar, shape_data[0], v.data(),
                                        stride_data[0]);
    } else if constexpr (std::is_same<DataType, scomplex>::value) {
      tblis::tblis_init_vector_scaled_c(&tblis_vector_, scalar, shape_data[0], v.data(),
                                        stride_data[0]);
    } else if constexpr (std::is_same<DataType, dcomplex>::value) {
      tblis::tblis_init_vector_scaled_z(&tblis_vector_, scalar, shape_data[0], v.data(),
                                        stride_data[0]);
    }

    this->tblis_vector_.conj = conj ? 1 : 0;
  }

  tblis::tblis_vector* tblis_vector(const bool conj = false) { return &tblis_vector_; }
};

template <typename DataType>
struct tblis_const_vector_wrap_ {
#ifdef USE_SAFE_TBLIS_CONVERSION
  std::array<tblis::len_type, 1> shape_;
  std::array<tblis::stride_type, 1> stride_;
#endif

  tblis::tblis_vector tblis_vector_;

  template <class MatrixType, typename T = typename MatrixType::DataType>
  tblis_const_vector_wrap_(const bool& conj, const T& scalar, MatrixType& v)
#ifdef USE_SAFE_TBLIS_CONVERSION
      :
      shape_(copy_array<tblis::len_type, 1>(v.shape())),
      stride_(copy_array<tblis::stride_type, 1>(v.stride()))
#endif
  {
    static_assert(is_any<DataType, float, double, scomplex, dcomplex>::value,
                  "TBLIS wrapper only accepts float, double, complex<float>, complex<double>");
#ifdef USE_SAFE_TBLIS_CONVERSION
    auto shape_data = shape_.data();
    auto stride_data = stride_.data();
#else
    static_assert(
        std::is_same<tblis::len_type, qtens_len_t>::value
            && std::is_same<tblis::stride_type, qtens_len_t>::value,
        "qleve and TBLIS want different data types. Turn on USE_SAFE_TBLIS_CONVERSION macro.");
    auto shape_data = const_cast<tblis::len_type*>(v.shape().data());
    auto stride_data = const_cast<tblis::stride_type*>(v.stride().data());
#endif

    if constexpr (std::is_same<DataType, float>::value) {
      tblis::tblis_init_vector_scaled_s(&tblis_vector_, scalar, shape_data[0],
                                        const_cast<DataType*>(v.data()), stride_data[0]);
    } else if constexpr (std::is_same<DataType, double>::value) {
      tblis::tblis_init_vector_scaled_d(&tblis_vector_, scalar, shape_data[0],
                                        const_cast<DataType*>(v.data()), stride_data[0]);
    } else if constexpr (std::is_same<DataType, scomplex>::value) {
      tblis::tblis_init_vector_scaled_c(&tblis_vector_, scalar, shape_data[0],
                                        const_cast<DataType*>(v.data()), stride_data[0]);
    } else if constexpr (std::is_same<DataType, dcomplex>::value) {
      tblis::tblis_init_vector_scaled_z(&tblis_vector_, scalar, shape_data[0],
                                        const_cast<DataType*>(v.data()), stride_data[0]);
    }

    this->tblis_vector_.conj = conj ? 1 : 0;
  }

  const tblis::tblis_vector* tblis_vector(const bool conj = false) { return &tblis_vector_; }
};

template <typename DataType>
struct tblis_matrix_wrap_ {
#ifdef USE_SAFE_TBLIS_CONVERSION
  std::array<tblis::len_type, 2> shape_;
  std::array<tblis::stride_type, 2> stride_;
#endif

  tblis::tblis_matrix tblis_matrix_;

  template <class MatrixType, typename T = typename MatrixType::DataType>
  tblis_matrix_wrap_(const bool& trans, const bool& conj, const T& scalar, MatrixType& m)
#ifdef USE_SAFE_TBLIS_CONVERSION
      :
      shape_(copy_array<tblis::len_type, 2>(m.shape())),
      stride_(copy_array<tblis::stride_type, 2>(m.stride()))
#endif
  {
    static_assert(is_any<DataType, float, double, scomplex, dcomplex>::value,
                  "TBLIS wrapper only accepts float, double, complex<float>, complex<double>");
#ifdef USE_SAFE_TBLIS_CONVERSION
    auto shape_data = shape_.data();
    auto stride_data = stride_.data();
#else
    static_assert(
        std::is_same<tblis::len_type, qtens_len_t>::value
            && std::is_same<tblis::stride_type, qtens_len_t>::value,
        "qleve and TBLIS want different data types. Turn on USE_SAFE_TBLIS_CONVERSION macro.");
    auto shape_data = const_cast<tblis::len_type*>(m.shape().data());
    auto stride_data = const_cast<tblis::stride_type*>(m.stride().data());
#endif

    auto sh1 = trans ? shape_data[1] : shape_data[0];
    auto sh2 = trans ? shape_data[0] : shape_data[1];

    auto st1 = trans ? stride_data[1] : stride_data[0];
    auto st2 = trans ? stride_data[0] : stride_data[1];

    if constexpr (std::is_same<DataType, float>::value) {
      tblis::tblis_init_matrix_scaled_s(&tblis_matrix_, scalar, sh1, sh2, m.data(), st1, st2);
    } else if constexpr (std::is_same<DataType, double>::value) {
      tblis::tblis_init_matrix_scaled_d(&tblis_matrix_, scalar, sh1, sh2, m.data(), st1, st2);
    } else if constexpr (std::is_same<DataType, scomplex>::value) {
      tblis::tblis_init_matrix_scaled_c(&tblis_matrix_, scalar, sh1, sh2, m.data(), st1, st2);
    } else if constexpr (std::is_same<DataType, dcomplex>::value) {
      tblis::tblis_init_matrix_scaled_z(&tblis_matrix_, scalar, sh1, sh2, m.data(), st1, st2);
    }

    this->tblis_matrix_.conj = conj ? 1 : 0;
  }

  tblis::tblis_matrix* tblis_matrix(const bool conj = false) { return &tblis_matrix_; }
};

template <typename DataType>
struct tblis_const_matrix_wrap_ {
#ifdef USE_SAFE_TBLIS_CONVERSION
  std::array<tblis::len_type, 2> shape_;
  std::array<tblis::stride_type, 2> stride_;
#endif

  tblis::tblis_matrix tblis_matrix_;

  template <class MatrixType, typename T = typename MatrixType::DataType>
  tblis_const_matrix_wrap_(const bool& trans, const bool& conj, const T& scalar, MatrixType& m)
#ifdef USE_SAFE_TBLIS_CONVERSION
      :
      shape_(trans ? {m.extent(1), m.extent(0)} : {m.extent(0), m.extent(1)}),
      stride_(trans ? {m.stride(1), m.stride(0)} : {m.stride(0), m.stride(1)})
#endif
  {
    static_assert(is_any<DataType, float, double, scomplex, dcomplex>::value,
                  "TBLIS wrapper only accepts float, double, complex<float>, complex<double>");
#ifdef USE_SAFE_TBLIS_CONVERSION
    auto shape_data = shape_.data();
    auto stride_data = stride_.data();
#else
    static_assert(
        std::is_same<tblis::len_type, qtens_len_t>::value
            && std::is_same<tblis::stride_type, qtens_len_t>::value,
        "qleve and TBLIS want different data types. Turn on USE_SAFE_TBLIS_CONVERSION macro.");
    auto shape_data = const_cast<tblis::len_type*>(m.shape().data());
    auto stride_data = const_cast<tblis::stride_type*>(m.stride().data());
#endif
    auto sh1 = trans ? shape_data[1] : shape_data[0];
    auto sh2 = trans ? shape_data[0] : shape_data[1];

    auto st1 = trans ? stride_data[1] : stride_data[0];
    auto st2 = trans ? stride_data[0] : stride_data[1];

    if constexpr (std::is_same<DataType, float>::value) {
      tblis::tblis_init_matrix_scaled_s(&tblis_matrix_, scalar, sh1, sh2,
                                        const_cast<DataType*>(m.data()), st1, st2);
    } else if constexpr (std::is_same<DataType, double>::value) {
      tblis::tblis_init_matrix_scaled_d(&tblis_matrix_, scalar, sh1, sh2,
                                        const_cast<DataType*>(m.data()), st1, st2);
    } else if constexpr (std::is_same<DataType, scomplex>::value) {
      tblis::tblis_init_matrix_scaled_c(&tblis_matrix_, scalar, sh1, sh2,
                                        const_cast<DataType*>(m.data()), st1, st2);
    } else if constexpr (std::is_same<DataType, dcomplex>::value) {
      tblis::tblis_init_matrix_scaled_z(&tblis_matrix_, scalar, sh1, sh2,
                                        const_cast<DataType*>(m.data()), st1, st2);
    }

    this->tblis_matrix_.conj = conj ? 1 : 0;
  }

  const tblis::tblis_matrix* tblis_matrix(const bool conj = false) { return &tblis_matrix_; }
};

template <typename DataType, int N>
struct tblis_tensor_wrap_ {
#ifdef USE_SAFE_TBLIS_CONVERSION
  std::array<tblis::len_type, N> shape_;
  std::array<tblis::stride_type, N> stride_;
#endif

  tblis::tblis_tensor tblis_tensor_;

  template <class TensorType, typename T = typename TensorType::DataType>
  tblis_tensor_wrap_(const bool& conj, const T& scalar, TensorType& t)
#ifdef USE_SAFE_TBLIS_CONVERSION
      :
      shape_(copy_array<tblis::len_type, N>(t.shape())),
      stride_(copy_array<tblis::stride_type, N>(t.stride()))
#endif
  {
    static_assert(N == TensorType::rank(),
                  "Ranks do not match in tblis_tensor_wrap_ and provided Tensor");
    static_assert(is_any<DataType, float, double, scomplex, dcomplex>::value,
                  "TBLIS wrapper only accepts float, double, complex<float>, complex<double>");
#ifdef USE_SAFE_TBLIS_CONVERSION
    auto shape_data = shape_.data();
    auto stride_data = stride_.data();
#else
    static_assert(
        std::is_same<tblis::len_type, qtens_len_t>::value
            && std::is_same<tblis::stride_type, qtens_len_t>::value,
        "qleve and TBLIS want different data types. Turn on USE_SAFE_TBLIS_CONVERSION macro.");
    auto shape_data = const_cast<tblis::len_type*>(t.shape().data());
    auto stride_data = const_cast<tblis::stride_type*>(t.stride().data());
#endif

    if constexpr (std::is_same<DataType, float>::value) {
      tblis::tblis_init_tensor_scaled_s(&tblis_tensor_, scalar, N, shape_data, t.data(),
                                        stride_data);
    } else if constexpr (std::is_same<DataType, double>::value) {
      tblis::tblis_init_tensor_scaled_d(&tblis_tensor_, scalar, N, shape_data, t.data(),
                                        stride_data);
    } else if constexpr (std::is_same<DataType, scomplex>::value) {
      tblis::tblis_init_tensor_scaled_c(&tblis_tensor_, scalar, N, shape_data, t.data(),
                                        stride_data);
    } else if constexpr (std::is_same<DataType, dcomplex>::value) {
      tblis::tblis_init_tensor_scaled_z(&tblis_tensor_, scalar, N, shape_data, t.data(),
                                        stride_data);
    }

    this->tblis_tensor_.conj = conj ? 1 : 0;
  }

  tblis::tblis_tensor* tblis_tensor(const bool conj = false) { return &tblis_tensor_; }
};

template <typename DataType, int N>
struct tblis_const_tensor_wrap_ {
#ifdef USE_SAFE_TBLIS_CONVERSION
  std::array<tblis::len_type, N> shape_;
  std::array<tblis::stride_type, N> stride_;
#endif

  tblis::tblis_tensor tblis_tensor_;

  template <class TensorType, typename T = typename TensorType::DataType>
  tblis_const_tensor_wrap_(const bool& conj, const T& scalar, const TensorType& t)
#ifdef USE_SAFE_TBLIS_CONVERSION
      :
      shape_(copy_array<tblis::len_type, N>(t.shape())),
      stride_(copy_array<tblis::stride_type, N>(t.stride()))
#endif
  {
    static_assert(N == TensorType::rank(),
                  "Ranks do not match in tblis_tensor_wrap_ and provided Tensor");
    static_assert(is_any<DataType, float, double, scomplex, dcomplex>::value,
                  "TBLIS wrapper only accepts float, double, complex<float>, complex<double>");

#ifdef USE_SAFE_TBLIS_CONVERSION
    auto shape_data = shape_.data();
    auto stride_data = stride_.data();
#else
    static_assert(
        std::is_same<tblis::len_type, qtens_len_t>::value
            && std::is_same<tblis::stride_type, qtens_len_t>::value,
        "qleve and TBLIS want different data types. Turn on USE_SAFE_TBLIS_CONVERSION macro.");
    auto shape_data = const_cast<tblis::len_type*>(t.shape().data());
    auto stride_data = const_cast<tblis::stride_type*>(t.stride().data());
#endif

    // This is really ugly, but it's not my interface
    if constexpr (std::is_same<DataType, float>::value) {
      tblis::tblis_init_tensor_scaled_s(&tblis_tensor_, scalar, N, shape_data,
                                        const_cast<DataType*>(t.data()), stride_data);
    } else if constexpr (std::is_same<DataType, double>::value) {
      tblis::tblis_init_tensor_scaled_d(&tblis_tensor_, scalar, N, shape_data,
                                        const_cast<DataType*>(t.data()), stride_data);
    } else if constexpr (std::is_same<DataType, scomplex>::value) {
      tblis::tblis_init_tensor_scaled_c(&tblis_tensor_, scalar, N, shape_data,
                                        const_cast<DataType*>(t.data()), stride_data);
    } else if constexpr (std::is_same<DataType, dcomplex>::value) {
      tblis::tblis_init_tensor_scaled_z(&tblis_tensor_, scalar, N, shape_data,
                                        const_cast<DataType*>(t.data()), stride_data);
    }

    this->tblis_tensor_.conj = conj ? 1 : 0;
  } // namespace

  const tblis::tblis_tensor* tblis_tensor() { return &tblis_tensor_; }
}; // namespace

template <typename ArrayType>
using tblis_vector_wrap = tblis_vector_wrap_<typename std::remove_reference_t<ArrayType>::DataType>;

template <typename ArrayType>
using tblis_const_vector_wrap =
    tblis_const_vector_wrap_<typename std::remove_reference_t<ArrayType>::DataType>;

template <typename MatrixType>
using tblis_matrix_wrap =
    tblis_matrix_wrap_<typename std::remove_reference_t<MatrixType>::DataType>;

template <typename MatrixType>
using tblis_const_matrix_wrap =
    tblis_const_matrix_wrap_<typename std::remove_reference_t<MatrixType>::DataType>;

template <typename TensorType>
using tblis_tensor_wrap = tblis_tensor_wrap_<typename std::remove_reference_t<TensorType>::DataType,
                                             std::remove_reference_t<TensorType>::rank()>;

template <typename TensorType>
using tblis_const_tensor_wrap =
    tblis_const_tensor_wrap_<typename std::remove_reference_t<TensorType>::DataType,
                             std::remove_reference_t<TensorType>::rank()>;

template <typename T>
tblis::tblis_scalar make_tblis_scalar(const T& a)
{
  static_assert(is_any<T, float, double, scomplex, dcomplex>::value,
                "tblis_scalar must be one of float, double, scomplex, dcomplex");

  tblis::tblis_scalar out;

  if constexpr (std::is_same<T, float>::value) {
    tblis::tblis_init_scalar_s(&out, a);
  } else if constexpr (std::is_same<T, double>::value) {
    tblis::tblis_init_scalar_d(&out, a);
  } else if constexpr (std::is_same<T, scomplex>::value) {
    tblis::tblis_init_scalar_c(&out, a);
  } else if constexpr (std::is_same<T, dcomplex>::value) {
    tblis::tblis_init_scalar_d(&out, a);
  }
  return out;
}

} // namespace qleve

#endif // QLEVE_TBLIS_INTERFACE_HPP
