#include <numeric>
#include <ranges>

#include "catch.hpp"

#include <qleve/tensor.hpp>
#include <qleve/tensor_contract.hpp>
#include <qleve/tensor_dot.hpp>
#include <qleve/tensor_traits.hpp>
#include <qleve/vector_ops.hpp>

using namespace std;
using namespace qleve;

TEST_CASE("Tensor Tests", "[tensor]")
{

  Tensor<3> tensor(3, 4, 5);
  std::iota(tensor.data(), tensor.data() + tensor.size(), 0);

  REQUIRE(tensor.extent(0) == 3);
  REQUIRE(tensor.extent(1) == 4);
  REQUIRE(tensor.extent(2) == 5);

  REQUIRE(tensor.size() == 3 * 4 * 5);

  REQUIRE(&tensor.at(1, 2, 3) == &tensor(1, 2, 3));

  const double refnorm = tensor.norm();
  REQUIRE(refnorm == Approx(264.9716966017));

  SECTION("Is nonconst and const tensor")
  {
    REQUIRE(is_const_tensor<Tensor<3>>::value);
    REQUIRE(is_nonconst_tensor<Tensor<3>>::value);
  }

  SECTION("View is nonconst and const tensor")
  {
    REQUIRE(is_const_tensor<TensorView<3>>::value);
    REQUIRE(is_nonconst_tensor<TensorView<3>>::value);
  }

  SECTION("ConstView is nonconst and not const tensor")
  {
    REQUIRE(is_const_tensor<ConstTensorView<3>>::value);
    REQUIRE(!is_nonconst_tensor<ConstTensorView<3>>::value);
  }

  SECTION("Layouts")
  {
    REQUIRE(is_contiguous<Tensor<3>>::value);
    REQUIRE_FALSE(is_columnwise<Tensor<3>>::value);
    REQUIRE_FALSE(is_strided<Tensor<3>>::value);
    REQUIRE(is_at_least_columnwise<Tensor<3>>::value);

    REQUIRE(is_contiguous<TensorView<3>>::value);
    REQUIRE_FALSE(is_columnwise<TensorView<3>>::value);
    REQUIRE_FALSE(is_strided<TensorView<3>>::value);
    REQUIRE(is_at_least_columnwise<TensorView<3>>::value);

    REQUIRE(is_contiguous<ConstTensorView<3>>::value);
    REQUIRE_FALSE(is_columnwise<ConstTensorView<3>>::value);
    REQUIRE_FALSE(is_strided<ConstTensorView<3>>::value);
    REQUIRE(is_at_least_columnwise<ConstTensorView<3>>::value);

    REQUIRE_FALSE(is_contiguous<SubTensor<3>>::value);
    REQUIRE(is_columnwise<SubTensor<3>>::value);
    REQUIRE_FALSE(is_strided<SubTensor<3>>::value);
    REQUIRE(is_at_least_columnwise<SubTensor<3>>::value);

    REQUIRE_FALSE(is_contiguous<ConstSubTensor<3>>::value);
    REQUIRE(is_columnwise<ConstSubTensor<3>>::value);
    REQUIRE_FALSE(is_strided<ConstSubTensor<3>>::value);
    REQUIRE(is_at_least_columnwise<ConstSubTensor<3>>::value);

    REQUIRE_FALSE(is_contiguous<StridedTensor<3>>::value);
    REQUIRE_FALSE(is_columnwise<StridedTensor<3>>::value);
    REQUIRE(is_strided<StridedTensor<3>>::value);
    REQUIRE_FALSE(is_at_least_columnwise<StridedTensor<3>>::value);

    REQUIRE_FALSE(is_contiguous<ConstStridedTensor<3>>::value);
    REQUIRE_FALSE(is_columnwise<ConstStridedTensor<3>>::value);
    REQUIRE(is_strided<ConstStridedTensor<3>>::value);
    REQUIRE_FALSE(is_at_least_columnwise<ConstStridedTensor<3>>::value);
  }

  SECTION("print_tensor prints elements")
  {
    std::stringstream ss;
    tensor.print_tensor(ss, "Tensor Test", tensor.size(), true);
    std::string ref_result = " --- Tensor Test: [3, 4, 5] ---\n"
                             "0\n1\n2\n3\n4\n5\n6\n7\n8\n9\n"
                             "10\n11\n12\n13\n14\n15\n16\n17\n18\n19\n"
                             "20\n21\n22\n23\n24\n25\n26\n27\n28\n29\n"
                             "30\n31\n32\n33\n34\n35\n36\n37\n38\n39\n"
                             "40\n41\n42\n43\n44\n45\n46\n47\n48\n49\n"
                             "50\n51\n52\n53\n54\n55\n56\n57\n58\n59\n";

    CHECK(ss.str() == ref_result);
  }

  SECTION("can sum over")
  {
    ConstTensorView<3> ctv(tensor);
    const double sum = asum(ctv.size(), ctv.data());

    REQUIRE(sum == Approx(0.5 * (ctv.size()) * (ctv.size() - 1)));
  }

  SECTION("Can fill")
  {
    tensor = 4.0;

    REQUIRE(tensor(0, 1, 2) == 4.0);
    REQUIRE(tensor.norm() == Approx(4.0 * std::sqrt(3 * 4 * 5)));
  }

  SECTION("tensor = scalar * tensor")
  {
    Tensor<3> t2(tensor);

    tensor = t2 * 1.1;

    REQUIRE(tensor(0, 0, 0) == Approx(1.1 * t2(0, 0, 0)));
  }

  SECTION("conj(tensor) == tensor")
  {
    Tensor<3> t2 = qleve::conj(tensor) - tensor;

    REQUIRE(t2.norm() == Approx(0.0).margin(1e-15));
  }

  SECTION("real(tensor) == tensor")
  {
    Tensor<3> t2 = qleve::real(tensor) - tensor;

    REQUIRE(t2.norm() == Approx(0.0).margin(1e-15));
  }

  SECTION("imag(tensor) == 0.0")
  {
    Tensor<3> t2 = qleve::imag(tensor);

    REQUIRE(t2.norm() == 0.0);
  }

  SECTION("tensor = cos^2(tensor) + sin^2(tensor) - 1 == 0")
  {
    Tensor<3> t2 = qleve::pow(qleve::cos(tensor), 2.0) + qleve::pow(qleve::sin(tensor), 2.0) - 1.0;

    REQUIRE(t2.norm() == Approx(0.0).margin(1e-15));
  }

  SECTION("tensor = cosh^2(tensor) - sinh^2(tensor) - 1 == 0")
  {
    tensor /= 10.0;
    Tensor<3> t2 =
        qleve::pow(qleve::cosh(tensor), 2.0) - qleve::pow(qleve::sinh(tensor), 2.0) - 1.0;

    REQUIRE(t2.norm() == Approx(0.0).margin(1e-10));
  }

  SECTION("tensor = pow(2.0, tensor);")
  {
    Tensor<3> t2 = qleve::pow(2.0, tensor);

    REQUIRE(t2(0, 0, 0) == Approx(1.0));
    REQUIRE(t2(1, 0, 0) == Approx(2.0));
    REQUIRE(t2(2, 0, 0) == Approx(4.0));
    REQUIRE(t2(0, 1, 0) == Approx(8.0));
    REQUIRE(t2(1, 1, 0) == Approx(16.0));
    REQUIRE(t2(2, 1, 0) == Approx(32.0));
    REQUIRE(t2(0, 2, 0) == Approx(64.0));
    REQUIRE(t2(1, 2, 0) == Approx(128.0));
    REQUIRE(t2(2, 2, 0) == Approx(256.0));
    REQUIRE(t2(0, 3, 0) == Approx(512.0));
    REQUIRE(t2(1, 3, 0) == Approx(1024.0));
    REQUIRE(t2(2, 3, 0) == Approx(2048.0));

    Tensor<3> testtensor = qleve::log2(t2) - tensor;
    REQUIRE(testtensor.norm() == Approx(0.0).margin(1e-15));
  }

  SECTION("tensor = min(tensor, 2.0)")
  {
    Tensor<3> t2 = qleve::min(tensor, 2.0);

    REQUIRE(t2(0, 0, 0) == Approx(0.0));
    REQUIRE(t2(1, 0, 0) == Approx(1.0));
    REQUIRE(t2(2, 0, 0) == Approx(2.0));
    REQUIRE(t2(0, 1, 0) == Approx(2.0));
    REQUIRE(t2(1, 1, 0) == Approx(2.0));
    REQUIRE(t2(2, 1, 0) == Approx(2.0));
  }

  SECTION("tensor = max(tensor, 2.0)")
  {
    Tensor<3> t2 = qleve::max(tensor, 2.0);

    REQUIRE(t2(0, 0, 0) == Approx(2.0));
    REQUIRE(t2(1, 0, 0) == Approx(2.0));
    REQUIRE(t2(2, 0, 0) == Approx(2.0));
    REQUIRE(t2(0, 1, 0) == Approx(3.0));
    REQUIRE(t2(1, 1, 0) == Approx(4.0));
    REQUIRE(t2(2, 1, 0) == Approx(5.0));
  }

  SECTION("Unary operation")
  {
    tensor *= -1.0;

    Tensor<3> abstensor = qleve::abs(tensor);

    REQUIRE(tensor(0, 0, 0) == -abstensor(0, 0, 0));
    REQUIRE(tensor(0, 2, 1) == -abstensor(0, 2, 1));
  }

  SECTION("Square root")
  {
    tensor -= sqrt(tensor * tensor);

    REQUIRE(tensor.norm() == Approx(0.0));
  }

  SECTION("Can make view")
  {
    TensorView<3> tv(tensor);

    REQUIRE(tensor(1, 1, 1) == tv(1, 1, 1));
    REQUIRE(tv.norm() == Approx(refnorm));

    tensor = 2.0 * tv - 1.0;
    REQUIRE(tensor.norm() == Approx(523.2781287231));
  }

  SECTION("Can make const view")
  {
    ConstTensorView<3> ctv(tensor);

    REQUIRE(tensor(1, 1, 1) == ctv(1, 1, 1));
    REQUIRE(ctv.norm() == Approx(refnorm));
  }

  SECTION("Can reshape")
  {
    TensorView<2> t2 = tensor.reshape(12, 5);

    REQUIRE(tensor(0, 2, 3) == t2(2 * 3, 3));
  }

  SECTION("Can const reshape")
  {
    ConstTensorView<2> t2 = tensor.const_reshape(12, 5);

    REQUIRE(tensor(0, 2, 3) == t2(2 * 3, 3));
  }

  SECTION("ConstView can const reshape")
  {
    ConstTensorView<3> t2(tensor);

    auto tt = t2.const_reshape(12, 5);

    REQUIRE(tensor(0, 2, 3) == tt(2 * 3, 3));
  }

  SECTION("Can slice")
  {
    auto sliced = tensor.slice(2, 3);

    REQUIRE(tensor(0, 0, 2) == sliced(0, 0, 0));
    REQUIRE(sliced.size() == 3 * 4);
  }

  SECTION("View can slice")
  {
    TensorView<3> t2(tensor);
    auto sliced = t2.slice(2, 3);

    REQUIRE(tensor(0, 0, 2) == sliced(0, 0, 0));
    REQUIRE(sliced.size() == 3 * 4);
  }

  SECTION("Can const slice")
  {
    auto sliced = tensor.const_slice(2, 3);

    REQUIRE(tensor(0, 0, 2) == sliced(0, 0, 0));
    REQUIRE(sliced.size() == 3 * 4);
  }

  SECTION("ConstView can const slice")
  {
    ConstTensorView<3> t2(tensor);
    auto sliced = t2.const_slice(2, 3);

    REQUIRE(tensor(0, 0, 2) == sliced(0, 0, 0));
    REQUIRE(sliced.size() == 3 * 4);
  }

  SECTION("Slice can copy")
  {
    tensor.slice(0, 1) = tensor.slice(1, 2);

    REQUIRE(tensor(0, 0, 0) == tensor(0, 0, 1));
    REQUIRE(tensor(1, 0, 0) == tensor(1, 0, 1));
    REQUIRE(tensor(0, 1, 0) == tensor(0, 1, 1));
    REQUIRE(tensor(1, 1, 0) == tensor(1, 1, 1));
    REQUIRE(tensor(0, 2, 0) == tensor(0, 2, 1));
    REQUIRE(tensor(1, 2, 0) == tensor(1, 2, 1));
  }

  SECTION("pluck multiple ranks")
  {
    // pluck into matrix
    auto mat = tensor.pluck(1);

    REQUIRE(mat.size() == 3 * 4);
    REQUIRE(mat(0, 0) == tensor(0, 0, 1));
    REQUIRE(mat(0, 1) == tensor(0, 1, 1));

    mat(1, 0) = 2.0;
    REQUIRE(tensor(1, 0, 1) == 2.0);

    // pluck into vector
    auto vec = tensor.pluck(2, 1);
    REQUIRE(vec.size() == 3);
    REQUIRE(vec(1) == mat(1, 2));
  }

  SECTION("const_pluck")
  {
    // pluck into matrix
    auto mat = tensor.const_pluck(1);

    REQUIRE(mat.size() == 3 * 4);
    REQUIRE(mat(0, 0) == tensor(0, 0, 1));
    REQUIRE(mat(1, 1) == tensor(1, 1, 1));

    // pluck into vector
    auto vec = tensor.const_pluck(2, 1);
    REQUIRE(vec.size() == 3);
    REQUIRE(vec(0) == mat(0, 2));
    REQUIRE(vec(1) == mat(1, 2));
  }

  SECTION("View can reshape")
  {
    TensorView<3> tv(tensor);

    TensorView<2> t2 = tv.reshape(12, 5);

    REQUIRE(tv(0, 2, 3) == t2(2 * 3, 3));
  }

  SECTION("View can const reshape")
  {
    TensorView<3> tv(tensor);

    ConstTensorView<2> t2 = tv.const_reshape(12, 5);

    REQUIRE(tv(0, 2, 3) == t2(2 * 3, 3));
  }

  SECTION("Const view can const reshape")
  {
    ConstTensorView<3> ctv(tensor);

    ConstTensorView<2> t2 = ctv.const_reshape(12, 5);

    REQUIRE(ctv(0, 2, 3) == t2(2 * 3, 3));
  }

  SECTION("Can reorder")
  {
    Tensor<3> t2 = tensor.reorder<1, 0, 2>();

    CHECK(tensor(2, 3, 1) == t2(3, 2, 1));
    CHECK(tensor(0, 2, 4) == t2(2, 0, 4));
  }

  SECTION("Can instantiate from template expression")
  {
    Tensor<3> t2(3.0 * tensor * tensor - tensor);

    CHECK(t2(0, 0, 0) == Approx(0));
    CHECK(t2(1, 2, 3) == Approx(5504.0));
  }

  SECTION("Can view subtensor")
  {
    SubTensor<3> t2 = tensor.subtensor({0, 0, 0}, {2, 2, 2});

    CHECK(t2(0, 0, 0) == tensor(0, 0, 0));

    t2 = 0.0;
    CHECK(tensor(0, 0, 0) == 0.0);
    CHECK(tensor(2, 2, 2) == Approx(32.0));
  }

  SECTION("Can add/subtract from subtensor")
  {
    SubTensor<3> t2 = tensor.subtensor({0, 0, 0}, {2, 2, 2});

    Tensor<3> x(2, 2, 2);
    std::iota(x.data(), x.data() + x.size(), 10.0);

    t2 += x;
    CHECK(tensor.norm() == Approx(271.400073692));

    t2 -= 2.0 * x;
    CHECK(tensor.norm() == Approx(264.1249704212));

    t2 += x * 2.0;
    CHECK(tensor.norm() == Approx(271.400073692));

    t2 -= x * 2.0;
    CHECK(tensor.norm() == Approx(264.1249704212));

    t2 += 2.0 * x;
    CHECK(tensor.norm() == Approx(271.400073692));

    t2 -= x;
    CHECK(tensor.norm() == Approx(refnorm));
  }

  SECTION("Can multiply/divide from subtensor")
  {
    SubTensor<3> t2 = tensor.subtensor({1, 1, 1}, {3, 3, 3});

    Tensor<3> x(2, 2, 2);
    std::iota(x.data(), x.data() + x.size(), 2.0);

    t2 *= x;
    CHECK(tensor.norm() == Approx(546.4210830486));

    t2 /= x;
    CHECK(tensor.norm() == Approx(refnorm));

    t2 *= 0.5 * x;
    CHECK(tensor.norm() == Approx(351.5885379247));

    t2 /= 0.5 * x;
    CHECK(tensor.norm() == Approx(refnorm));

    t2 *= x * 0.25;
    CHECK(tensor.norm() == Approx(282.6201072111));

    t2 /= x / 4.0;
    CHECK(tensor.norm() == Approx(refnorm));
  }

  SECTION("Can assign from const subtensor")
  {
    auto t2 = tensor.const_subtensor({0, 0, 0}, {1, 1, 2});

    Tensor<3> x(tensor);
    auto subx = x.subtensor({1, 1, 1}, {2, 2, 3});
    subx = t2;

    CHECK(x(1, 1, 1) == tensor(0, 0, 0));
    CHECK(x(1, 1, 2) == tensor(0, 0, 1));

    TensorView<3> xview(x);
    auto subsubx = xview.const_subtensor({0, 0, 1}, {1, 1, 2});
    CHECK(subsubx(0, 0, 0) == x(0, 0, 1));
  }

  SECTION("contractable")
  {
    array<ptrdiff_t, 4> ashape{4, 8, 12, 16};
    array<ptrdiff_t, 3> bshape{5, 7, 16};
    array<ptrdiff_t, 5> cshape{4, 8, 12, 7, 5};

    REQUIRE(is_contractable(ashape, "ijkl", bshape, "abl", cshape, "ijkba"));
    REQUIRE_FALSE(is_contractable(ashape, "ijkl", bshape, "abl", cshape, "ijkab"));
    REQUIRE_FALSE(is_contractable(ashape, "ijlk", bshape, "abl", cshape, "ijkab"));
  }

  SECTION("Can contract")
  {
    Tensor<3> tt(tensor);
    tt *= 2.0;
    tt -= 1.4;

    Tensor<4> out(tensor.extent(0), tensor.extent(2), tt.extent(2), tt.extent(0));
    out = 1.0;

    contract(2.0, tensor, "iap", tt, "jaq", 0.5, out, "ipqj");

    const int i = 2, j = 1, p = 4, q = 3;

    CHECK(out(i, p, q, j)
          == Approx(2.0
                        * (tensor(i, 0, p) * tt(j, 0, q) + tensor(i, 1, p) * tt(j, 1, q)
                           + tensor(i, 2, p) * tt(j, 2, q) + tensor(i, 3, p) * tt(j, 3, q))
                    + 0.5));
  }

  SECTION("Contract a strided tensor")
  {
    auto strided = tensor.pop_left(0);
    Tensor<3> tt(tensor);

    Tensor<3> out(tt.extent(0), strided.extent(1), tt.extent(2));
    contract(2.0, strided, "bc", tt, "abd", 0.0, out, "acd");

    Tensor<2> unstrided = strided;

    auto out2 = out.clone();
    contract(2.0, unstrided, "bc", tt, "abd", 0.0, out2, "acd");

    CHECK(out.norm() == out2.norm());
    CHECK(out(0, 0, 0) == Approx(out2(0, 0, 0)));
    CHECK(out(0, 1, 0) == Approx(out2(0, 1, 0)));
  }

  SECTION("Can dot product")
  {
    auto tt = tensor.reorder<1, 2, 0>();

    const double tensordot = dot_product(tensor, "abc", tt, "bca");
    const double blasdot = dot_product(tensor.size(), tensor.data(), tensor.data());
    const double simpledot = dot_product(tensor, tensor);

    CHECK(tensordot == Approx(blasdot));
    CHECK(tensordot == Approx(simpledot));

    ConstTensorView<3> ct(tensor);
    const double cdot = dot_product(tensor, ct);

    CHECK(tensordot == Approx(cdot));
  }

  SECTION("Can dot product with strided tensor")
  {
    auto strided0 = tensor.pop_left(0);
    auto strided1 = tensor.pop_left(1);

    const double tensordot = dot_product(strided0, "bc", strided1, "bc");

    double ref = 0.0;
    for (ptrdiff_t j = 0; j < tensor.extent(1); ++j) {
      for (ptrdiff_t k = 0; k < tensor.extent(2); ++k) {
        ref += tensor(0, j, k) * tensor(1, j, k);
      }
    }

    CHECK(tensordot == Approx(ref));
  }

#ifdef __cpp_lib_span
  SECTION("get_column returns a span")
  {
    span<double, 3> col = tensor.get_column<3>(0, 0);
    CHECK(col[0] == tensor(0, 0, 0));
    col[1] += 2;
    CHECK(col[1] == tensor(1, 0, 0));

    ConstTensorView<3> ct(tensor);
    const span<const double, 3> ccol = ct.get_const_column<3>(0, 0);
    CHECK(ccol[0] == ct(0, 0, 0));
    col[1] += 2;
    CHECK(ccol[1] == ct(1, 0, 0));

    {
      const auto& [x, y, z] = tensor.get_column<3>(0, 0);
      CHECK(x == tensor(0, 0, 0));
      CHECK(y == tensor(1, 0, 0));
      CHECK(z == tensor(2, 0, 0));
    }

    {
      auto [x, y, z] = tensor.get_column<3>(1, 2);
      CHECK(x == tensor(0, 1, 2));
      CHECK(y == tensor(1, 1, 2));
      CHECK(z == tensor(2, 1, 2));

      z += 3;
      CHECK(z == tensor(2, 1, 2) + 3);
    }

    {
      const auto [x, y, z] = ct.get_const_column<3>(0, 2);
      CHECK(x == tensor(0, 0, 2));
      CHECK(y == tensor(1, 0, 2));
      CHECK(z == tensor(2, 0, 2));
    }
  }
#endif // __cpp_lib_span

  SECTION("read-only range-based for loops")
  {
    double y = 0.0;
    for (auto x : tensor) {
      y += x;
    }

    CHECK(y == Approx(0.5 * 60. * 59.));
  }

  SECTION("read-write range-based for loops")
  {
    for (auto& x : tensor) {
      x += 1.0;
    }

    double y = 0.0;
    for (const auto x : tensor) {
      y += x;
    }
    CHECK(y == Approx(0.5 * 61. * 60.));
  }

  SECTION("range-based for loop on subtensor")
  {
    auto sub = tensor.subtensor({1, 1, 1}, {2, 2, 3});

    double y = 0.0;
    for (const auto x : sub) {
      y += x;
    }
    CHECK(y == Approx(44.));
  }

  SECTION("range-based for loop on const subtensor")
  {
    const auto sub = tensor.const_subtensor({1, 1, 1}, {2, 2, 3});

    double y = 0.0;
    for (const auto x : sub) {
      y += x;
    }
    CHECK(y == Approx(44.));
  }

  SECTION("std::accumulate on const subtensor")
  {
    const auto sub = tensor.const_subtensor({1, 1, 1}, {2, 2, 3});

    const double y = std::accumulate(sub.begin(), sub.end(), 0ul);
    CHECK(y == Approx(44.));
  }

  SECTION("is a range")
  {
    STATIC_REQUIRE(std::ranges::range<decltype(tensor)>);
    STATIC_REQUIRE(std::ranges::range<TensorView<3>>);
    STATIC_REQUIRE(std::ranges::range<SubTensor<3>>);
    STATIC_REQUIRE(std::ranges::range<const ConstTensorView<3>>);
    STATIC_REQUIRE(std::ranges::range<const ConstSubTensor<3>>);
  }

  SECTION("range filter")
  {
    CHECK(std::ranges::range<decltype(tensor)>);

    double y = 0.0;
    std::ranges::for_each(tensor, [&y](const double& x) { y += x; });
    CHECK(y == Approx(0.5 * 60. * 59.));

    const auto [mn, mx] = std::ranges::minmax(tensor);
    CHECK(mn == 0.0);
    CHECK(mx == 59.0);

    // TODO viewable_range appears buggy in recent gcc, so disabling for now
    // CHECK(std::ranges::viewable_range<decltype(tensor)>);
    //
    // double z = 0.0;
    // for (auto& x : tensor | std::views::filter([](double x) { return x > 11.5; })) {
    //  z += x;
    //}
    // CHECK(z == Approx(0.5 * 60. * 59. - 0.5 * 12. * 11.));
  }

  SECTION("range filter on a const tensor")
  {
    const auto& sub = tensor.const_subtensor({1, 1, 1}, {2, 2, 3});
    CHECK(std::ranges::range<decltype(sub)>);

    // TODO I think this should work but it fails to compile. Bug in the standard library?
    // CHECK(std::ranges::viewable_range<decltype(sub)>);
    // for (const auto& x : sub | std::ranges::views::filter([](double x) { return x < 100.; })) {
    // double z = 0.0;
    // for (const auto& x : sub) {
    //   z += x;
    // }
    // CHECK(z == Approx(44.));

    double y = 0.0;
    for (const auto& x : sub) {
      y += x;
    }
    CHECK(y == Approx(44.));

    const auto [mn, mx] = std::ranges::minmax(sub);
    CHECK(mn == 16.);
    CHECK(mx == 28.);
  }
}
