// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file include/qleve/determinant.hpp
///
/// Generic interface to compute a matrix determinant

#ifndef QLEVE_EXCLUDED_PRODUCT_HPP
#define QLEVE_EXCLUDED_PRODUCT_HPP

#include <tuple>

#include <qleve/blas_interface.hpp>
#include <qleve/pivoted_lu.hpp>
#include <qleve/tensor.hpp>
#include <qleve/tensor_traits.hpp>

namespace qleve
{

auto excluded_product_1(const const_arrayable auto& A)
{
  auto out = A.zeros_like();
  const ptrdiff_t ndim = A.extent(0);

  auto left = A.zeros_like();
  double lf = 1.0;
  for (ptrdiff_t i = 0; i < ndim; ++i) {
    left(i) = lf;
    lf *= A(i);
  }

  auto right = A.zeros_like();
  double rt = 1.0;
  for (ptrdiff_t i = ndim - 1; i >= 0; --i) {
    right(i) = rt;
    rt *= A(i);
  }

  out = left * right;
  return out;
}

template <const_arrayable VecType>
auto excluded_product_2(const VecType& A, const double diagonal_value = 1.0)
{
  auto out1 = A.zeros_like();
  const ptrdiff_t ndim = A.extent(0);

  auto left = A.zeros_like();
  double lf = 1.0;
  for (ptrdiff_t i = 0; i < ndim; ++i) {
    left(i) = lf;
    lf *= A(i);
  }

  auto right = A.zeros_like();
  double rt = 1.0;
  for (ptrdiff_t i = ndim - 1; i >= 0; --i) {
    right(i) = rt;
    rt *= A(i);
  }

  out1 = left * right;

  auto out2 = Tensor_<tensor_datatype<VecType>, 2>(ndim, ndim);
  for (ptrdiff_t j = 0; j < ndim; ++j) {
    double xij = 1.0;
    for (ptrdiff_t i = j - 1; i >= 0; --i) {
      out2(i, j) = xij * left(i) * right(j);
      out2(j, i) = out2(i, j);

      xij *= A(i);
    }
    out2(j, j) = diagonal_value;
  }

  return std::make_tuple(out1, out2);
}

template <const_arrayable VecType>
auto excluded_product_3(const VecType& A, const double diagonal_value = 1.0)
{
  auto out1 = A.zeros_like();
  const ptrdiff_t ndim = A.extent(0);

  auto left = A.zeros_like();
  double lf = 1.0;
  for (ptrdiff_t i = 0; i < ndim; ++i) {
    left(i) = lf;
    lf *= A(i);
  }

  auto right = A.zeros_like();
  double rt = 1.0;
  for (ptrdiff_t i = ndim - 1; i >= 0; --i) {
    right(i) = rt;
    rt *= A(i);
  }

  out1 = left * right;

  auto out2 = Tensor_<tensor_datatype<VecType>, 2>(ndim, ndim);
  for (ptrdiff_t j = 0; j < ndim; ++j) {
    double xij = 1.0;
    for (ptrdiff_t i = j - 1; i >= 0; --i) {
      out2(i, j) = xij * left(i) * right(j);
      out2(j, i) = out2(i, j);

      xij *= A(i);
    }
    out2(j, j) = diagonal_value;
  }

  auto out3 = Tensor_<tensor_datatype<VecType>, 3>(ndim, ndim, ndim);
  out3 = diagonal_value;

  for (ptrdiff_t k = 0; k < ndim; ++k) {
    double xijk = 1.0;
    for (ptrdiff_t j = k - 1; j >= 0; --j) {
      for (ptrdiff_t i = j - 1; i >= 0; --i) {
        out3(i, j, k) = 1.0;
        for (ptrdiff_t m = 0; m < ndim; ++m) {
          if (m != i && m != j && m != k && i != j && j != k && k != i) {
            out3(i, j, k) *= A(m);
          }
        }
        out3(i, k, j) = out3(i, j, k);
        out3(j, i, k) = out3(i, j, k);
        out3(j, k, i) = out3(i, j, k);
        out3(k, i, j) = out3(i, j, k);
        out3(k, j, i) = out3(i, j, k);
      }
    }
  }

  return std::make_tuple(out1, out2, out3);
}

} // namespace qleve

#endif // QLEVE_EXCLUDED_PRODUCT_HPP
