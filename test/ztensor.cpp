#include <numeric>

#include "catch.hpp"

#include <qleve/tensor.hpp>
#include <qleve/tensor_contract.hpp>

using namespace std;
using namespace qleve;

TEST_CASE("ZTensor Tests", "[ztensor]")
{
  ZTensor<3> tensor(3, 4, 5);

  for (int i = 0; i < tensor.size(); ++i) {
    tensor.data(i) = i * i;
  }

  CHECK(tensor.extent(0) == 3);
  CHECK(tensor.extent(1) == 4);
  REQUIRE(tensor.extent(2) == 5);

  REQUIRE(tensor.size() == 3 * 4 * 5);

  const double refnorm = tensor.norm();
  REQUIRE(refnorm == Approx(12211.1423708022));

  SECTION("Is nonconst and const tensor")
  {
    REQUIRE(is_const_tensor<ZTensor<3>>::value);
    REQUIRE(is_nonconst_tensor<ZTensor<3>>::value);
  }

  SECTION("View is nonconst and const tensor")
  {
    REQUIRE(is_const_tensor<ZTensorView<3>>::value);
    REQUIRE(is_nonconst_tensor<ZTensorView<3>>::value);
  }

  SECTION("ConstView is nonconst and not const tensor")
  {
    REQUIRE(is_const_tensor<ConstZTensorView<3>>::value);
    REQUIRE(!is_nonconst_tensor<ConstZTensorView<3>>::value);
  }

  SECTION("Can fill")
  {
    tensor = std::complex<double>(4.0, 2.0);

    REQUIRE(tensor(0, 1, 2).real() == 4.0);
    REQUIRE(tensor(0, 1, 2).imag() == 2.0);

    REQUIRE(tensor.norm() == Approx(std::sqrt(20.0 * 3 * 4 * 5)));
  }

  SECTION("Can conjugate")
  {
    tensor *= std::complex<double>(0.0, 1.0);

    tensor = tensor + conj(tensor);

    REQUIRE(tensor.norm() == Approx(0.0));
  }

  SECTION("Can get real")
  {
    Tensor<3> t2 = qleve::real(tensor);

    REQUIRE(t2.norm() == Approx(refnorm));
  }

  SECTION("Can get imag")
  {
    Tensor<3> t2 = qleve::imag(tensor);

    REQUIRE(t2.norm() == Approx(0.0).margin(1e-16));
  }

  SECTION("Can make view")
  {
    ZTensorView<3> tv(tensor);

    REQUIRE(tensor(1, 1, 1) == tv(1, 1, 1));
    REQUIRE(tv.norm() == Approx(refnorm));
  }

  SECTION("Can make const view")
  {
    ConstZTensorView<3> ctv(tensor);

    REQUIRE(tensor(1, 1, 1) == ctv(1, 1, 1));
    REQUIRE(ctv.norm() == Approx(refnorm));
  }

  SECTION("Can reorder")
  {
    auto t2 = tensor.reorder<1, 0, 2>();

    CHECK(tensor(2, 3, 1) == t2(3, 2, 1));
    CHECK(tensor(0, 2, 4) == t2(2, 0, 4));
  }

  SECTION("Can instantiate from template expression")
  {
    ZTensor<3> t2(3.0 * tensor * tensor - tensor);

    CHECK(t2(0, 0, 0).real() == Approx(0));
    CHECK(t2(1, 2, 3).real() == Approx(10254554.0));
  }

  SECTION("Can contract")
  {
    ZTensor<3> tt(tensor);
    tt *= dcomplex(2.0, 1.0);
    tt -= dcomplex(0.0, 1.4);

    ZTensor<4> out(tensor.extent(0), tensor.extent(2), tt.extent(2), tt.extent(0));
    out = 1.0;

    contract(true, false, 2.0, tensor, "iap", tt, "jaq", 0.5, out, "ipqj");

    const int i = 2, j = 1, p = 4, q = 3;

    auto ref_result =
        2.0
            * (conj(tensor(i, 0, p)) * tt(j, 0, q) + conj(tensor(i, 1, p)) * tt(j, 1, q)
               + conj(tensor(i, 2, p)) * tt(j, 2, q) + conj(tensor(i, 3, p)) * tt(j, 3, q))
        + 0.5;

    CHECK(out(i, p, q, j).real() == Approx(ref_result.real()));
    CHECK(out(i, p, q, j).imag() == Approx(ref_result.imag()));
  }
}
