// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file include/qleve/tensor.hpp
///
/// ND Tensor classes, including associated Views and ConstViews

#ifndef QLEVE_TENSOR_HPP
#define QLEVE_TENSOR_HPP

#include <algorithm>
#include <array>
#include <complex>
#include <utility>

#include <qleve/indexable.hpp>
#include <qleve/pod_types.hpp>
#include <qleve/reorder_indices.hpp>
#include <qleve/storage.hpp>
#include <qleve/tensor_base.hpp>

namespace qleve
{

// Forward declarations
template <typename T, int Rank>
class TensorView_;
template <typename T, int Rank>
class ConstTensorView_;
template <typename T, int Rank>
class SubTensor_;
template <typename T, int Rank>
class ConstSubTensor_;
template <typename T, int Rank>
class StridedTensor_;
template <typename T, int Rank>
class ConstStridedTensor_;

/// Tensor of template specified rank and data type
template <typename T, int Rank>
class Tensor_ :
    public Indexable<T, Tensor_<T, Rank>, ContiguousLayout>,
    public TensorBase_<T, Rank, Tensor_<T, Rank>> {
 public:
  using DataType = T;
  using Layout = ContiguousLayout;

 protected:
  using TensorType = Tensor_<T, Rank>;
  using TensorViewType = TensorView_<T, Rank>;
  using ConstTensorViewType = ConstTensorView_<T, Rank>;
  using SubTensorType = SubTensor_<T, Rank>;
  using ConstSubTensorType = ConstSubTensor_<T, Rank>;
  using StridedTensorType = StridedTensor_<T, Rank>;
  using ConstStridedTensorType = ConstStridedTensor_<T, Rank>;

  using IndexableType = Indexable<T, TensorType, ContiguousLayout>;
  using TensorBaseType = TensorBase_<T, Rank, TensorType>;

  UniqueStorage<T> storage_;

  using TensorBaseType::extent_;
  using TensorBaseType::size_;
  using TensorBaseType::stride_;

 public:
  template <typename... Args>
    requires(sizeof...(Args) + 1 == Rank)
  Tensor_(const qtens_len_t& head, const Args&... args) :
      TensorBaseType(head, args...), storage_(size_)
  {
    static_assert((sizeof...(args) + 1) == Rank,
                  "Tensor_ constructors should accept one argument per tensor rank");
    std::fill_n(storage_.data(), size_, T(0.0));
  }

  Tensor_(const std::array<qtens_len_t, Rank>& ext) : TensorBaseType(ext), storage_(size_)
  {
    std::fill_n(storage_.data(), size_, T(0.0));
  }

  template <typename U, class E, class L>
  Tensor_(const Indexable<U, E, L>& x) : Tensor_(x.ishape())
  {
    this->assign_from(x);
  }

  template <typename U, class L, class E>
  Tensor_(const Xpr<U, L, E>& x) : Tensor_(x.ishape())
  {
    this->assign_from(x);
  }

  Tensor_(const TensorType& o) : TensorBaseType(o.shape(), o.stride(), o.size()), storage_(size())
  {
    std::copy_n(o.storage().data(), size_, storage_.data());
  }

  Tensor_(TensorType&& o) :
      TensorBaseType(o.shape(), o.stride(), o.size()), storage_(std::move(o.storage_))
  {}

  Tensor_(const TensorViewType& o) :
      TensorBaseType(o.shape(), o.stride(), o.size()), storage_(size())
  {
    std::copy_n(o.storage().data(), size_, storage_.data());
  }

  Tensor_(const ConstTensorViewType& o) :
      TensorBaseType(o.shape(), o.stride(), o.size()), storage_(size())
  {
    std::copy_n(o.storage().data(), size_, storage_.data());
  }

  T* data() { return storage_.data(); }
  const T* data() const { return storage_.data(); }

  T& data(const qtens_len_t& i) { return storage_.data(i); }
  const T& data(const qtens_len_t& i) const { return storage_.data(i); }

  T& data(const qtens_len_t& i, const qtens_len_t& j) { return storage_.data(j * extent_[0] + i); }
  const T& data(const qtens_len_t& i, const qtens_len_t& j) const
  {
    return storage_.data(j * extent_[0] + i);
  }

  qtens_len_t column_index(const qtens_len_t& i) const { return i * extent_[0]; }

  Tensor_& operator=(const Tensor_& x)
  {
    this->assign_from(x);
    return *this;
  }

  template <typename U, class L, class E>
  Tensor_& operator=(const Xpr<U, L, E>& x)
  {
    this->assign_from(x);
    return *this;
  }

  template <typename U, class A, class L>
  Tensor_& operator=(const Indexable<U, A, L>& x)
  {
    this->assign_from(x);
    return *this;
  }

  template <typename U>
  Tensor_& operator=(const U& x)
  {
    this->assign_from(x);
    return *this;
  }

  using TensorBaseType::operator();
  using TensorBaseType::at;
  using TensorBaseType::ncolumns;
  using TensorBaseType::nrows;
  using TensorBaseType::shape;
  using TensorBaseType::size;

  UniqueStorage<T>& storage() { return storage_; }
  const UniqueStorage<T>& storage() const { return storage_; }

  using TensorBaseType::_norm_contiguous;
  double norm() const { return _norm_contiguous(); }

  SubTensorType subtensor(const std::array<qtens_len_t, Rank>& lowerbounds,
                          const std::array<qtens_len_t, Rank>& upperbounds)
  {
    T* const d = &at(lowerbounds);
    std::array<qtens_len_t, Rank> subsize;
    for (int i = 0; i < Rank; ++i) {
      assert(lowerbounds[i] >= 0 && upperbounds[i] > lowerbounds[i]
             && upperbounds[i] <= extent_[i]);
      subsize[i] = upperbounds[i] - lowerbounds[i];
    }

    return SubTensorType(d, subsize, stride_, this->compute_size_(subsize));
  }

  const ConstSubTensorType const_subtensor(const std::array<qtens_len_t, Rank>& lowerbounds,
                                           const std::array<qtens_len_t, Rank>& upperbounds) const
  {
    const T* const d = &at(lowerbounds);
    std::array<qtens_len_t, Rank> subsize;
    for (int i = 0; i < Rank; ++i) {
      assert(lowerbounds[i] >= 0 && upperbounds[i] > lowerbounds[i]
             && upperbounds[i] <= extent_[i]);
      subsize[i] = upperbounds[i] - lowerbounds[i];
    }

    return ConstSubTensorType(d, subsize, stride_, this->compute_size_(subsize));
  }

  template <int... ranks>
    requires(sizeof...(ranks) == Rank)
  TensorType reorder(const T alpha = 1.0, const T beta = 0.0) const
  {
    TensorType out(std::get<ranks>(this->shape())...);
    reorder_indices<ranks...>(this->shape(), alpha, this->data(), beta, out.data());
    return std::move(out);
  }

  TensorType transpose() const
  {
    return this->transpose_impl(make_reversed_integer_sequence<int, Rank>{});
  }

  TensorType dagger() const
  {
    auto out = this->transpose();
    out = qleve::conj(out);
    return out;
  }

  TensorType clone() const { return (*this); }
  TensorType zeros_like() const { return TensorType(this->shape()); }

  template <typename... Args>
    requires(are_size_worthy_v<Args...>)
  auto reshape(const Args&... args) -> TensorView_<T, sizeof...(args)>
  {
    constexpr int reshape_N = sizeof...(args);
    TensorView_<T, reshape_N> out(this->data(), args...);
    assert(this->size() == out.size());
    return out;
  }

  template <typename... Args>
    requires(are_size_worthy_v<Args...>)
  auto reshape(const Args&... args) const -> const TensorView_<T, sizeof...(args)>
  {
    constexpr int reshape_N = sizeof...(args);
    const TensorView_<T, reshape_N> out(this->data(), args...);
    assert(this->size() == out.size());
    return out;
  }

  template <typename... Args>
    requires(are_size_worthy_v<Args...>)
  auto const_reshape(const Args&... args) const -> const ConstTensorView_<T, sizeof...(args)>
  {
    constexpr int reshape_N = sizeof...(args);
    ConstTensorView_<T, reshape_N> out(this->data(), args...);
    assert(this->size() == out.size());
    return out;
  }

  TensorViewType view() { return TensorViewType{this->data(), this->shape()}; }
  TensorViewType view() const { return TensorViewType{this->data(), this->shape()}; }
  ConstTensorViewType const_view() const
  {
    return ConstTensorViewType{this->data(), this->shape()};
  }

  TensorViewType slice(const ptrdiff_t left, const ptrdiff_t right)
  {
    assert(left >= 0 && right <= this->extent(Rank - 1));
    auto shape = this->shape();
    shape.at(Rank - 1) = right - left;
    TensorViewType out(&this->data(left * this->stride(Rank - 1)), shape);
    return out;
  }

  TensorViewType slice(const ptrdiff_t left, const ptrdiff_t right) const
  {
    assert(left >= 0 && right <= this->extent(Rank - 1));
    auto shape = this->shape();
    shape.at(Rank - 1) = right - left;
    TensorViewType out(&this->data(left * this->stride(Rank - 1)), shape);
    return out;
  }

  const ConstTensorViewType const_slice(const ptrdiff_t left, const ptrdiff_t right) const
  {
    assert(left >= 0 && right <= this->extent(Rank - 1));
    auto shape = this->shape();
    shape.at(Rank - 1) = right - left;
    ConstTensorViewType out(&this->data(left * this->stride(Rank - 1)), shape);
    return out;
  }

  [[deprecated("Deprecated in favor of pluck() instead")]] TensorView_<T, Rank - 1> slice(
      const ptrdiff_t i)
  {
    assert(i >= 0 && i < this->extent(Rank - 1));
    std::array<ptrdiff_t, Rank - 1> shape;
    for (int j = 0; j < Rank - 1; ++j) {
      shape[j] = this->extent(j);
    }
    TensorView_<T, Rank - 1> out(&this->data(i * this->stride(Rank - 1)), shape);
    return out;
  }

  [[deprecated("Deprecated in favor of pluck() instead")]] TensorView_<T, Rank - 1> slice(
      const ptrdiff_t i) const
  {
    assert(i >= 0 && i < this->extent(Rank - 1));
    std::array<ptrdiff_t, Rank - 1> shape;
    for (int j = 0; j < Rank - 1; ++j) {
      shape[j] = this->extent(j);
    }
    TensorView_<T, Rank - 1> out(&this->data(i * this->stride(Rank - 1)), shape);
    return out;
  }

  [[deprecated("Deprecated in favor of pluck() instead")]] const ConstTensorView_<T, Rank - 1>
  const_slice(const ptrdiff_t i) const
  {
    assert(i >= 0 && i < this->extent(Rank - 1));
    std::array<ptrdiff_t, Rank - 1> shape;
    for (int j = 0; j < Rank - 1; ++j) {
      shape[j] = this->extent(j);
    }
    ConstTensorView_<T, Rank - 1> out(&this->data(i * this->stride(Rank - 1)), shape);
    return out;
  }

  TensorView_<T, Rank - 1> pluck(const ptrdiff_t i)
  {
    assert(i >= 0 && i < this->extent(Rank - 1));
    std::array<ptrdiff_t, Rank - 1> shape;
    for (int j = 0; j < Rank - 1; ++j) {
      shape[j] = this->extent(j);
    }
    TensorView_<T, Rank - 1> out(&this->data(i * this->stride(Rank - 1)), shape);
    return out;
  }

  TensorView_<T, Rank - 1> pluck(const ptrdiff_t i) const
  {
    assert(i >= 0 && i < this->extent(Rank - 1));
    std::array<ptrdiff_t, Rank - 1> shape;
    for (int j = 0; j < Rank - 1; ++j) {
      shape[j] = this->extent(j);
    }
    TensorView_<T, Rank - 1> out(&this->data(i * this->stride(Rank - 1)), shape);
    return out;
  }

  const ConstTensorView_<T, Rank - 1> const_pluck(const ptrdiff_t i) const
  {
    assert(i >= 0 && i < this->extent(Rank - 1));
    std::array<ptrdiff_t, Rank - 1> shape;
    for (int j = 0; j < Rank - 1; ++j) {
      shape[j] = this->extent(j);
    }
    ConstTensorView_<T, Rank - 1> out(&this->data(i * this->stride(Rank - 1)), shape);
    return out;
  }

  template <typename... Args>
  auto pluck(const ptrdiff_t head, const Args... tail)
  {
    static_assert(Rank > (sizeof...(tail) + 1),
                  "Rank must be greater than the number of arguments in pluck()");
    return this->pluck(tail...).pluck(head);
  }

  template <typename... Args>
  auto pluck(const ptrdiff_t head, const Args... tail) const
  {
    static_assert(Rank > (sizeof...(tail) + 1),
                  "Rank must be greater than the number of arguments in pluck()");
    return this->pluck(tail...).pluck(head);
  }

  template <typename... Args>
  auto const_pluck(const ptrdiff_t head, const Args... tail) const
  {
    static_assert(Rank > (sizeof...(tail) + 1),
                  "Rank must be greater than the number of arguments in pluck()");
    return this->const_pluck(tail...).const_pluck(head);
  }

  StridedTensor_<T, Rank - 1> pop_left(const ptrdiff_t i)
  {
    std::array<qtens_len_t, Rank - 1> shape;
    std::copy_n(this->shape().begin() + 1, Rank - 1, shape.begin());
    std::array<qtens_len_t, Rank - 1> stride;
    std::copy_n(this->stride().begin() + 1, Rank - 1, stride.begin());
    assert(i >= 0 && i < this->extent(0));
    StridedTensor_<T, Rank - 1> out(&this->data(i), shape, stride, this->size() / this->extent(0));
    return out;
  }

  StridedTensor_<T, Rank - 1> pop_left(const ptrdiff_t i) const
  {
    std::array<qtens_len_t, Rank - 1> shape;
    std::copy_n(this->shape().begin() + 1, Rank - 1, shape.begin());
    std::array<qtens_len_t, Rank - 1> stride;
    std::copy_n(this->stride().begin() + 1, Rank - 1, stride.begin());
    assert(i >= 0 && i < this->extent(0));
    StridedTensor_<T, Rank - 1> out(&this->data(i), shape, stride, this->size() / this->extent(0));
    return out;
  }

  const ConstStridedTensor_<T, Rank - 1> const_pop_left(const ptrdiff_t i) const
  {
    std::array<qtens_len_t, Rank - 1> shape;
    std::copy_n(this->shape().begin() + 1, Rank - 1, shape.begin());
    std::array<qtens_len_t, Rank - 1> stride;
    std::copy_n(this->stride().begin() + 1, Rank - 1, stride.begin());
    assert(i >= 0 && i < this->extent(0));
    ConstStridedTensor_<T, Rank - 1> out(&this->data(i), shape, stride,
                                         this->size() / this->extent(0));
    return out;
  }

  StridedTensor_<T, 1> diagonal()
  {
    const qtens_len_t n = *std::min_element(extent_.begin(), extent_.end());
    std::array<qtens_len_t, 1> shape{n};

    const qtens_len_t stride = std::accumulate(stride_.begin(), stride_.end(), 0);
    return StridedTensor_<T, 1>(data(), shape, std::array<qtens_len_t, 1>{stride}, n);
  }
  ConstStridedTensor_<T, 1> const_diagonal() const
  {
    const qtens_len_t n = *std::min_element(extent_.begin(), extent_.end());
    std::array<qtens_len_t, 1> shape{n};

    const qtens_len_t stride = std::accumulate(stride_.begin(), stride_.end(), 0);
    return ConstStridedTensor_<T, 1>(data(), shape, std::array<qtens_len_t, 1>{stride}, n);
  }

 private:
  template <int... ranks>
  TensorType transpose_impl(const std::integer_sequence<int, ranks...>&) const
  {
    return this->reorder<ranks...>();
  }
};

template <int Rank>
using Tensor = Tensor_<double, Rank>;
template <int Rank>
using ZTensor = Tensor_<dcomplex, Rank>;

template <typename T, int Rank>
class TensorView_ :
    public Indexable<T, TensorView_<T, Rank>, ContiguousLayout>,
    public TensorBase_<T, Rank, TensorView_<T, Rank>> {
 public:
  using DataType = T;
  using Layout = ContiguousLayout;

 protected:
  using TensorType = Tensor_<T, Rank>;
  using TensorViewType = TensorView_<T, Rank>;
  using ConstTensorViewType = ConstTensorView_<T, Rank>;
  using SubTensorType = SubTensor_<T, Rank>;
  using ConstSubTensorType = ConstSubTensor_<T, Rank>;
  using StridedTensorType = StridedTensor_<T, Rank>;
  using ConstStridedTensorType = ConstStridedTensor_<T, Rank>;

  using IndexableType = Indexable<T, TensorViewType, ContiguousLayout>;
  using TensorBaseType = TensorBase_<T, Rank, TensorViewType>;

  WeakStorage<T> storage_;

  using TensorBaseType::extent_;
  using TensorBaseType::size_;
  using TensorBaseType::stride_;

 public:
  template <typename... Args>
    requires(sizeof...(Args) + 1 == Rank)
  TensorView_(T* const d, const qtens_len_t head, const Args&... args) :
      TensorBaseType(head, args...), storage_(d, size_)
  {}

  TensorView_(T* const d, const std::array<qtens_len_t, Rank>& shape) :
      TensorBaseType(shape), storage_(d, size_)
  {}

  TensorView_(TensorViewType& o) :
      TensorBaseType(o.shape(), o.stride(), o.size()), storage_(o.storage().data(), size_)
  {}

  TensorView_(TensorViewType&& o) :
      TensorBaseType(o.shape(), o.stride(), o.size()), storage_(o.storage().data(), size_)
  {}

  TensorView_(TensorType& o) :
      TensorBaseType(o.shape(), o.stride(), o.size()), storage_(o.storage().data(), size_)
  {}

  T* data() { return storage_.data(); }
  const T* data() const { return storage_.data(); }

  T& data(const qtens_len_t& i) { return storage_.data(i); }
  const T& data(const qtens_len_t& i) const { return storage_.data(i); }

  T& data(const qtens_len_t& i, const qtens_len_t& j) { return storage_.data(j * extent_[0] + i); }
  const T& data(const qtens_len_t& i, const qtens_len_t& j) const
  {
    return storage_.data(j * extent_[0] + i);
  }

  TensorView_& operator=(const TensorView_& x)
  {
    this->assign_from(x);
    return *this;
  }

  template <typename U, class L, class E>
  TensorView_& operator=(const Xpr<U, L, E>& x)
  {
    this->assign_from(x);
    return *this;
  }

  template <typename U, class A, class L>
  TensorView_& operator=(const Indexable<U, A, L>& x)
  {
    this->assign_from(x);
    return *this;
  }

  template <typename U>
  TensorView_& operator=(const U& x)
  {
    this->assign_from(x);
    return *this;
  }

  using TensorBaseType::operator();
  using TensorBaseType::at;

  qtens_len_t size() const { return size_; }

  WeakStorage<T>& storage() { return storage_; }
  const WeakStorage<T>& storage() const { return storage_; }

  using TensorBaseType::_norm_contiguous;
  double norm() const { return _norm_contiguous(); }

  TensorType clone() const { return (*this); }
  TensorType zeros_like() const { return TensorType(this->shape()); }

  template <int... ranks>
    requires(sizeof...(ranks) == Rank)
  TensorType reorder(const T alpha = 1.0, const T beta = 0.0) const
  {
    TensorType out(std::get<ranks>(this->shape())...);
    reorder_indices<ranks...>(this->shape(), alpha, this->data(), beta, out.data());
    return std::move(out);
  }

  TensorType transpose() const
  {
    return this->transpose_impl(make_reversed_integer_sequence<int, Rank>{});
  }

  TensorType dagger() const
  {
    auto out = this->transpose();
    out = qleve::conj(out);
    return out;
  }

  template <typename... Args>
    requires(are_size_worthy_v<Args...>)
  auto reshape(const Args&... args) -> TensorView_<T, sizeof...(args)>
  {
    constexpr int reshape_N = sizeof...(args);
    TensorView_<T, reshape_N> out(this->data(), args...);
    assert(this->size() == out.size());
    return out;
  }

  template <typename... Args>
    requires(are_size_worthy_v<Args...>)
  auto reshape(const Args&... args) const -> const TensorView_<T, sizeof...(args)>
  {
    constexpr int reshape_N = sizeof...(args);
    const TensorView_<T, reshape_N> out(this->data(), args...);
    assert(this->size() == out.size());
    return out;
  }

  template <typename... Args>
    requires(are_size_worthy_v<Args...>)
  auto const_reshape(const Args&... args) const -> ConstTensorView_<T, sizeof...(args)>
  {
    constexpr int reshape_N = sizeof...(args);
    ConstTensorView_<T, reshape_N> out(this->data(), args...);
    assert(this->size() == out.size());
    return out;
  }

  TensorViewType slice(const ptrdiff_t left, const ptrdiff_t right)
  {
    assert(left >= 0 && right <= this->extent(Rank - 1));
    auto shape = this->shape();
    shape.at(Rank - 1) = right - left;
    TensorViewType out(&this->data(left * this->stride(Rank - 1)), shape);
    return out;
  }

  TensorViewType slice(const ptrdiff_t left, const ptrdiff_t right) const
  {
    assert(left >= 0 && right <= this->extent(Rank - 1));
    auto shape = this->shape();
    shape.at(Rank - 1) = right - left;
    TensorViewType out(&this->data(left * this->stride(Rank - 1)), shape);
    return out;
  }

  ConstTensorViewType const_slice(const ptrdiff_t left, const ptrdiff_t right) const
  {
    assert(left >= 0 && right <= this->extent(Rank - 1));
    auto shape = this->shape();
    shape.at(Rank - 1) = right - left;
    ConstTensorViewType out(&this->data(left * this->stride(Rank - 1)), shape);
    return out;
  }

  TensorViewType view() { return TensorViewType{this->data(), this->shape()}; }
  TensorViewType view() const { return TensorViewType{this->data(), this->shape()}; }
  const ConstTensorViewType const_view() const
  {
    return ConstTensorViewType{this->data(), this->shape()};
  }

  [[deprecated("Deprecated in favor of pluck() instead")]] TensorView_<T, Rank - 1> slice(
      const ptrdiff_t i)
  {
    assert(i >= 0 && i < this->extent(Rank - 1));
    std::array<ptrdiff_t, Rank - 1> shape;
    for (int j = 0; j < Rank - 1; ++j) {
      shape[j] = this->extent(j);
    }
    TensorView_<T, Rank - 1> out(&this->data(i * this->stride(Rank - 1)), shape);
    return out;
  }

  [[deprecated("Deprecated in favor of pluck() instead")]] TensorView_<T, Rank - 1> slice(
      const ptrdiff_t i) const
  {
    assert(i >= 0 && i < this->extent(Rank - 1));
    std::array<ptrdiff_t, Rank - 1> shape;
    for (int j = 0; j < Rank - 1; ++j) {
      shape[j] = this->extent(j);
    }
    TensorView_<T, Rank - 1> out(&this->data(i * this->stride(Rank - 1)), shape);
    return out;
  }

  [[deprecated("Deprecated in favor of pluck() instead")]] ConstTensorView_<T, Rank - 1>
  const_slice(const ptrdiff_t i) const
  {
    assert(i >= 0 && i < this->extent(Rank - 1));
    std::array<ptrdiff_t, Rank - 1> shape;
    for (int j = 0; j < Rank - 1; ++j) {
      shape[j] = this->extent(j);
    }
    ConstTensorView_<T, Rank - 1> out(&this->data(i * this->stride(Rank - 1)), shape);
    return out;
  }

  TensorView_<T, Rank - 1> pluck(const ptrdiff_t i)
  {
    assert(i >= 0 && i < this->extent(Rank - 1));
    std::array<ptrdiff_t, Rank - 1> shape;
    for (int j = 0; j < Rank - 1; ++j) {
      shape[j] = this->extent(j);
    }
    TensorView_<T, Rank - 1> out(&this->data(i * this->stride(Rank - 1)), shape);
    return out;
  }

  TensorView_<T, Rank - 1> pluck(const ptrdiff_t i) const
  {
    assert(i >= 0 && i < this->extent(Rank - 1));
    std::array<ptrdiff_t, Rank - 1> shape;
    for (int j = 0; j < Rank - 1; ++j) {
      shape[j] = this->extent(j);
    }
    TensorView_<T, Rank - 1> out(&this->data(i * this->stride(Rank - 1)), shape);
    return out;
  }

  ConstTensorView_<T, Rank - 1> const_pluck(const ptrdiff_t i) const
  {
    assert(i >= 0 && i < this->extent(Rank - 1));
    std::array<ptrdiff_t, Rank - 1> shape;
    for (int j = 0; j < Rank - 1; ++j) {
      shape[j] = this->extent(j);
    }
    ConstTensorView_<T, Rank - 1> out(&this->data(i * this->stride(Rank - 1)), shape);
    return out;
  }

  template <typename... Args>
  auto pluck(const ptrdiff_t head, const Args... tail)
  {
    static_assert(Rank > (sizeof...(tail) + 1),
                  "Rank must be greater than the number of arguments in pluck()");
    return this->pluck(tail...).pluck(head);
  }

  template <typename... Args>
  auto pluck(const ptrdiff_t head, const Args... tail) const
  {
    static_assert(Rank > (sizeof...(tail) + 1),
                  "Rank must be greater than the number of arguments in pluck()");
    return this->pluck(tail...).pluck(head);
  }

  template <typename... Args>
  auto const_pluck(const ptrdiff_t head, const Args... tail) const
  {
    static_assert(Rank > (sizeof...(tail) + 1),
                  "Rank must be greater than the number of arguments in pluck()");
    return this->const_pluck(tail...).const_pluck(head);
  }

  StridedTensor_<T, Rank - 1> pop_left(const ptrdiff_t i)
  {
    std::array<qtens_len_t, Rank - 1> shape;
    std::copy_n(this->shape().begin() + 1, Rank - 1, shape.begin());
    std::array<qtens_len_t, Rank - 1> stride;
    std::copy_n(this->stride().begin() + 1, Rank - 1, stride.begin());
    assert(i >= 0 && i < this->extent(0));
    StridedTensor_<T, Rank - 1> out(&this->data(i), shape, stride, this->size() / this->extent(0));
    return out;
  }

  StridedTensor_<T, Rank - 1> pop_left(const ptrdiff_t i) const
  {
    std::array<qtens_len_t, Rank - 1> shape;
    std::copy_n(this->shape().begin() + 1, Rank - 1, shape.begin());
    std::array<qtens_len_t, Rank - 1> stride;
    std::copy_n(this->stride().begin() + 1, Rank - 1, stride.begin());
    assert(i >= 0 && i < this->extent(0));
    StridedTensor_<T, Rank - 1> out(&this->data(i), shape, stride, this->size() / this->extent(0));
    return out;
  }

  const ConstStridedTensor_<T, Rank - 1> const_pop_left(const ptrdiff_t i) const
  {
    std::array<qtens_len_t, Rank - 1> shape;
    std::copy_n(this->shape().begin() + 1, Rank - 1, shape.begin());
    std::array<qtens_len_t, Rank - 1> stride;
    std::copy_n(this->stride().begin() + 1, Rank - 1, stride.begin());
    assert(i >= 0 && i < this->extent(0));
    ConstStridedTensor_<T, Rank - 1> out(&this->data(i), shape, stride,
                                         this->size() / this->extent(0));
    return out;
  }

  SubTensorType subtensor(const std::array<qtens_len_t, Rank>& lowerbounds,
                          const std::array<qtens_len_t, Rank>& upperbounds)
  {
    T* const d = &at(lowerbounds);
    std::array<qtens_len_t, Rank> subsize;
    for (int i = 0; i < Rank; ++i) {
      assert(lowerbounds[i] >= 0 && upperbounds[i] > lowerbounds[i]
             && upperbounds[i] <= extent_[i]);
      subsize[i] = upperbounds[i] - lowerbounds[i];
    }

    return SubTensorType(d, subsize, stride_, this->compute_size_(subsize));
  }

  const ConstSubTensorType const_subtensor(const std::array<qtens_len_t, Rank>& lowerbounds,
                                           const std::array<qtens_len_t, Rank>& upperbounds) const
  {
    const T* const d = &at(lowerbounds);
    std::array<qtens_len_t, Rank> subsize;
    for (int i = 0; i < Rank; ++i) {
      assert(lowerbounds[i] >= 0 && upperbounds[i] > lowerbounds[i]
             && upperbounds[i] <= extent_[i]);
      subsize[i] = upperbounds[i] - lowerbounds[i];
    }

    return ConstSubTensorType(d, subsize, stride_, this->compute_size_(subsize));
  }

  StridedTensor_<T, 1> diagonal()
  {
    const qtens_len_t n = *std::min_element(extent_.begin(), extent_.end());
    std::array<qtens_len_t, 1> shape{n};

    const qtens_len_t stride = std::accumulate(stride_.begin(), stride_.end(), 0);
    return StridedTensor_<T, 1>(data(), shape, std::array<qtens_len_t, 1>{stride}, n);
  }
  ConstStridedTensor_<T, 1> const_diagonal()
  {
    const qtens_len_t n = *std::min_element(extent_.begin(), extent_.end());
    std::array<qtens_len_t, 1> shape{n};

    const qtens_len_t stride = std::accumulate(stride_.begin(), stride_.end(), 0);
    return ConstStridedTensor_<T, 1>(data(), shape, std::array<qtens_len_t, 1>{stride}, n);
  }

 private:
  template <int... ranks>
  TensorType transpose_impl(const std::integer_sequence<int, ranks...>&) const
  {
    return this->reorder<ranks...>();
  }
};

template <int Rank>
using TensorView = TensorView_<double, Rank>;
template <int Rank>
using ZTensorView = TensorView_<dcomplex, Rank>;

template <typename T, int Rank>
class ConstTensorView_ :
    public Indexable<T, ConstTensorView_<T, Rank>, ContiguousLayout>,
    public TensorBase_<T, Rank, ConstTensorView_<T, Rank>> {
 public:
  using DataType = T;
  using Layout = ContiguousLayout;

 protected:
  using TensorType = Tensor_<T, Rank>;
  using TensorViewType = TensorView_<T, Rank>;
  using ConstTensorViewType = ConstTensorView_<T, Rank>;
  using ConstSubTensorType = ConstSubTensor_<T, Rank>;
  using ConstStridedTensorType = ConstStridedTensor_<T, Rank>;

  using IndexableType = Indexable<T, ConstTensorViewType, ContiguousLayout>;
  using TensorBaseType = TensorBase_<T, Rank, ConstTensorViewType>;

  ConstWeakStorage<T> storage_;

  using TensorBaseType::extent_;
  using TensorBaseType::size_;
  using TensorBaseType::stride_;

 public:
  template <typename... Args>
    requires(sizeof...(Args) + 1 == Rank)
  ConstTensorView_(const T* const d, const qtens_len_t head, const Args&... args) :
      TensorBaseType(head, args...), storage_(d, size_)
  {}

  ConstTensorView_(const T* const d, const std::array<qtens_len_t, Rank>& shape) :
      TensorBaseType(shape), storage_(d, size_)
  {}

  ConstTensorView_(const ConstTensorViewType& o) :
      TensorBaseType(o.shape(), o.stride(), o.size()), storage_(o.storage())
  {}

  ConstTensorView_(const TensorViewType& o) :
      TensorBaseType(o.shape(), o.stride(), o.size()), storage_(o.storage(), size())
  {}

  ConstTensorView_(const TensorType& o) :
      TensorBaseType(o.shape(), o.stride(), o.size()), storage_(o.storage(), size())
  {}

  ConstTensorView_(TensorType&& o) :
      TensorBaseType(o.shape(), o.stride(), o.size()), storage_(nullptr)
  {
    assert(!"ConstTensorView must not be constructed from an rvalue-reference");
  }

  const T* data() const { return storage_.data(); }
  const T& data(const qtens_len_t& i) const { return storage_.data(i); }
  const T& data(const qtens_len_t& i, const qtens_len_t& j) const
  {
    return storage_.data(j * extent_[0] + i);
  }

  /// access operator is redefined here to hide the non-const versions in the base class
  template <typename... Args>
  const T& operator()(const Args&... args) const
  {
    return static_cast<const TensorBaseType*>(this)->operator()(args...);
  }

  /// at is redefined here to hide the non-const versions in the base class
  template <typename... Args>
  const T& at(const Args&... args) const
  {
    return this->operator()(args...);
  }

  qtens_len_t size() const { return size_; }

  const ConstWeakStorage<T>& storage() const { return storage_; }

  double norm() const
  {
    return std::sqrt(detail::real(qleve::dot_product(size(), data(), data())));
  }

  TensorType clone() const { return (*this); }
  TensorType zeros_like() const { return TensorType(this->shape()); }

  template <int... ranks>
    requires(sizeof...(ranks) == Rank)
  TensorType reorder(const T alpha = 1.0, const T beta = 0.0) const
  {
    TensorType out(std::get<ranks>(this->shape())...);
    reorder_indices<ranks...>(this->shape(), alpha, this->data(), beta, out.data());
    return std::move(out);
  }

  TensorType transpose() const
  {
    return this->transpose_impl(make_reversed_integer_sequence<int, Rank>{});
  }

  TensorType dagger() const
  {
    auto out = this->transpose();
    out = qleve::conj(out);
    return out;
  }

  template <typename... Args>
    requires(are_size_worthy_v<Args...>)
  auto const_reshape(const Args&... args) const -> ConstTensorView_<T, sizeof...(args)>
  {
    constexpr int reshape_N = sizeof...(args);
    ConstTensorView_<T, reshape_N> out(this->data(), args...);
    assert(this->size() == out.size());
    return out;
  }

  ConstTensorViewType const_slice(const ptrdiff_t left, const ptrdiff_t right) const
  {
    assert(left >= 0 && right <= this->extent(Rank - 1));
    auto shape = this->shape();
    shape.at(Rank - 1) = right - left;
    ConstTensorViewType out(&this->data(left * this->stride(Rank - 1)), shape);
    return out;
  }

  const ConstTensorViewType const_view() const
  {
    return ConstTensorViewType{this->data(), this->shape()};
  }


  [[deprecated("Deprecated in favor of pluck() instead")]] ConstTensorView_<T, Rank - 1>
  const_slice(const ptrdiff_t i) const
  {
    assert(i >= 0 && i < this->extent(Rank - 1));
    std::array<ptrdiff_t, Rank - 1> shape;
    for (int j = 0; j < Rank - 1; ++j) {
      shape[j] = this->extent(j);
    }
    ConstTensorView_<T, Rank - 1> out(&this->data(i * this->stride(Rank - 1)), shape);
    return out;
  }

  ConstTensorView_<T, Rank - 1> const_pluck(const ptrdiff_t i) const
  {
    assert(i >= 0 && i < this->extent(Rank - 1));
    std::array<ptrdiff_t, Rank - 1> shape;
    for (int j = 0; j < Rank - 1; ++j) {
      shape[j] = this->extent(j);
    }
    ConstTensorView_<T, Rank - 1> out(&this->data(i * this->stride(Rank - 1)), shape);
    return out;
  }

  template <typename... Args>
  auto const_pluck(const ptrdiff_t head, const Args... tail) const
  {
    static_assert(Rank > (sizeof...(tail) + 1),
                  "Rank must be greater than the number of arguments in pluck()");
    return this->const_pluck(tail...).const_pluck(head);
  }

  const ConstStridedTensor_<T, Rank - 1> const_pop_left(const ptrdiff_t i) const
  {
    std::array<qtens_len_t, Rank - 1> shape;
    std::copy_n(this->shape().begin() + 1, Rank - 1, shape.begin());
    std::array<qtens_len_t, Rank - 1> stride;
    std::copy_n(this->stride().begin() + 1, Rank - 1, stride.begin());
    assert(i >= 0 && i < this->extent(0));
    ConstStridedTensor_<T, Rank - 1> out(&this->data(i), shape, stride,
                                         this->size() / this->extent(0));
    return out;
  }

  const ConstSubTensorType const_subtensor(const std::array<qtens_len_t, Rank>& lowerbounds,
                                           const std::array<qtens_len_t, Rank>& upperbounds) const
  {
    const T* const d = &at(lowerbounds);
    std::array<qtens_len_t, Rank> subsize;
    for (int i = 0; i < Rank; ++i) {
      assert(lowerbounds[i] >= 0 && upperbounds[i] > lowerbounds[i]
             && upperbounds[i] <= extent_[i]);
      subsize[i] = upperbounds[i] - lowerbounds[i];
    }

    return ConstSubTensorType(d, subsize, stride_, this->compute_size_(subsize));
  }

  ConstStridedTensor_<T, 1> const_diagonal()
  {
    const qtens_len_t n = *std::min_element(extent_.begin(), extent_.end());
    std::array<qtens_len_t, 1> shape{n};

    const qtens_len_t stride = std::accumulate(stride_.begin(), stride_.end(), 0);
    return ConstStridedTensor_<T, 1>(data(), shape, std::array<qtens_len_t, 1>{stride}, n);
  }

 private:
  template <int... ranks>
  TensorType transpose_impl(const std::integer_sequence<int, ranks...>&) const
  {
    return this->reorder<ranks...>();
  }
};


template <int Rank>
using ConstTensorView = ConstTensorView_<double, Rank>;
template <int Rank>
using ConstZTensorView = ConstTensorView_<dcomplex, Rank>;


// --------------------------------------------------------------------------
/// \brief SubTensor_ is a class to address individual blocks of a Tensor_
///
/// SubTensor_ is inherently view-like, but allows for non-contiguous data
///
/// \tparam T data type (e.g., double and dcomplex)
/// \tparam Rank rank of tensor (i.e., a Matrix is a rank 2 tensor)
// ----------------------------------------------------------------------------
template <typename T, int Rank>
class SubTensor_ :
    public Indexable<T, SubTensor_<T, Rank>, ColumnwiseLayout>,
    public TensorBase_<T, Rank, SubTensor_<T, Rank>> {
 public:
  using DataType = T;
  using Layout = ColumnwiseLayout;

 protected:
  using TensorType = Tensor_<T, Rank>;
  using TensorViewType = TensorView_<T, Rank>;
  using SubTensorType = SubTensor_<T, Rank>;

  using IndexableType = Indexable<T, SubTensorType, ColumnwiseLayout>;
  using TensorBaseType = TensorBase_<T, Rank, SubTensorType>;

  WeakStorage<T> storage_;

  using TensorBaseType::extent_;
  using TensorBaseType::size_;
  using TensorBaseType::stride_;

 public:
  SubTensor_(T* const d, const std::array<qtens_len_t, Rank>& extent,
             const std::array<qtens_len_t, Rank>& stride, const qtens_len_t size) :
      TensorBaseType(extent, stride, size),
      storage_(d, std::get<Rank - 1>(stride) * std::get<Rank - 1>(extent))
  {}

  T* data() { return storage_.data(); }
  const T* data() const { return storage_.data(); }

  T& data(const qtens_len_t& i) { return storage_.data(i); }
  const T& data(const qtens_len_t& i) const { return storage_.data(i); }

  T& data(const qtens_len_t& i, const qtens_len_t& j)
  {
    return storage_.data(this->column_index(j) + i);
  }
  const T& data(const qtens_len_t& i, const qtens_len_t& j) const
  {
    return storage_.data(this->column_index(j) + i);
  }

  qtens_len_t column_index(qtens_len_t i) const
  {
    // This function comes down to mapping i -> (0, p, ...)
    // and then using (0, p, ...) and known strides to compute the index
    // Yikes.

    // what would strides be if the columns were numbered contiguously?

    qtens_len_t out = 0;
    for (qtens_len_t j = Rank - 1; j > 0; --j) {
      qtens_len_t dense_stride = 1ul;
      for (qtens_len_t is = 1; is < j; ++is) {
        dense_stride *= extent_[is];
      }

      const qtens_len_t idiv = i / dense_stride;
      i = i % dense_stride;
      out += idiv * stride_[j];
    }
    return out;
  }

  SubTensor_& operator=(const SubTensor_& x)
  {
    this->assign_from(x);
    return *this;
  }

  template <typename U, class L, class E>
  SubTensor_& operator=(const Xpr<U, L, E>& x)
  {
    this->assign_from(x);
    return *this;
  }

  template <typename U, class A, class L>
  SubTensor_& operator=(const Indexable<U, A, L>& x)
  {
    this->assign_from(x);
    return *this;
  }

  template <typename U>
  SubTensor_& operator=(const U& x)
  {
    this->assign_from(x);
    return *this;
  }

  using TensorBaseType::operator();
  using TensorBaseType::at;

  qtens_len_t size() const { return size_; }

  WeakStorage<T>& storage() { return storage_; }
  const WeakStorage<T>& storage() const { return storage_; }

  using TensorBaseType::_norm_columnwise;
  double norm() const { return _norm_columnwise(); }

  TensorType clone() const { return (*this); }
  TensorType zeros_like() const { return TensorType(this->shape()); }

  template <int... ranks>
    requires(sizeof...(ranks) == Rank)
  TensorType reorder(const T alpha = 1.0, const T beta = 0.0) const
  {
    // how to assure that this is a proper reorder, i.e., no repeated indices?
    TensorType out(std::get<ranks>(this->shape())...);
    reorder_indices_strided<ranks...>(extent_, stride_, alpha, this->data(), out.stride(), beta,
                                      out.data());
    return std::move(out);
  }

  TensorType transpose() const
  {
    return this->transpose_impl(make_reversed_integer_sequence<int, Rank>{});
  }

  TensorType dagger() const
  {
    auto out = this->transpose();
    out = qleve::conj(out);
    return out;
  }

 private:
  template <int... ranks>
  TensorType transpose_impl(const std::integer_sequence<int, ranks...>&) const
  {
    return this->reorder<ranks...>();
  }
}; // class SubTensor_

template <int Rank>
using SubTensor = SubTensor_<double, Rank>;
template <int Rank>
using SubZTensor = SubTensor_<dcomplex, Rank>;

// --------------------------------------------------------------------------
/// \brief ConstSubTensor_ is a class to address individual blocks of a Tensor_
///
/// ConstSubTensor_ is inherently const-view-like, but allows for non-contiguous data
///
/// \tparam T data type (e.g., double and dcomplex)
/// \tparam Rank rank of tensor (i.e., a Matrix is a rank 2 tensor)
// ----------------------------------------------------------------------------
template <typename T, int Rank>
class ConstSubTensor_ :
    public Indexable<T, ConstSubTensor_<T, Rank>, ColumnwiseLayout>,
    public TensorBase_<T, Rank, ConstSubTensor_<T, Rank>> {
 public:
  using DataType = T;
  using Layout = ColumnwiseLayout;

 protected:
  using TensorType = Tensor_<T, Rank>;
  using TensorViewType = TensorView_<T, Rank>;
  using ConstSubTensorType = ConstSubTensor_<T, Rank>;

  using IndexableType = Indexable<T, ConstSubTensorType, ColumnwiseLayout>;
  using TensorBaseType = TensorBase_<T, Rank, ConstSubTensorType>;

  ConstWeakStorage<T> storage_;

  using TensorBaseType::extent_;
  using TensorBaseType::size_;
  using TensorBaseType::stride_;

 public:
  ConstSubTensor_(const T* const d, const std::array<qtens_len_t, Rank>& extent,
                  const std::array<qtens_len_t, Rank>& stride, const qtens_len_t size) :
      TensorBaseType(extent, stride, size),
      storage_(d, std::get<Rank - 1>(stride) * std::get<Rank - 1>(extent))
  {}

  const T* data() const { return storage_.data(); }
  const T& data(const qtens_len_t& i) const { return storage_.data(i); }
  const T& data(const qtens_len_t& i, const qtens_len_t& j) const
  {
    return storage_.data(this->column_index(j) + i);
  }

  /// access operator is redefined here to hide the non-const versions in the base class
  template <typename... Args>
  const T& operator()(const Args&... args) const
  {
    return static_cast<const TensorBaseType*>(this)->operator()(args...);
  }

  /// at is redefined here to hide the non-const versions in the base class
  template <typename... Args>
  const T& at(const Args&... args) const
  {
    return this->operator()(args...);
  }

  qtens_len_t column_index(qtens_len_t i) const
  {
    // This function comes down to mapping i -> (0, p, ...)
    // and then using (0, p, ...) and known strides to compute the index
    // Yikes.

    // what would strides be if the columns were numbered contiguously?

    qtens_len_t out = 0;
    for (qtens_len_t j = Rank - 1; j > 0; --j) {
      qtens_len_t dense_stride = 1ul;
      for (qtens_len_t is = 1; is < j; ++is) {
        dense_stride *= extent_[is];
      }

      const qtens_len_t idiv = i / dense_stride;
      i = i % dense_stride;
      out += idiv * stride_[j];
    }
    return out;
  }

  qtens_len_t size() const { return size_; }

  WeakStorage<T>& storage() { return storage_; }
  const WeakStorage<T>& storage() const { return storage_; }

  TensorType clone() const { return (*this); }
  TensorType zeros_like() const { return TensorType(this->shape()); }

  using TensorBaseType::_norm_columnwise;
  double norm() const { return _norm_columnwise(); }

  template <int... ranks>
    requires(sizeof...(ranks) == Rank)
  TensorType reorder(const T alpha = 1.0, const T beta = 0.0) const
  {
    TensorType out(std::get<ranks>(this->shape())...);
    reorder_indices_strided<ranks...>(extent_, stride_, alpha, this->data(), out.stride(), beta,
                                      out.data());
    return std::move(out);
  }

  TensorType transpose() const
  {
    return this->transpose_impl(make_reversed_integer_sequence<int, Rank>{});
  }

  TensorType dagger() const
  {
    auto out = this->transpose();
    out = qleve::conj(out);
    return out;
  }

 private:
  template <int... ranks>
  TensorType transpose_impl(const std::integer_sequence<int, ranks...>&) const
  {
    return this->reorder<ranks...>();
  }
};

template <int Rank>
using ConstSubTensor = ConstSubTensor_<double, Rank>;
template <int Rank>
using ConstSubZTensor = ConstSubTensor_<dcomplex, Rank>;

// --------------------------------------------------------------------------
/// \brief StridedTensor_ is a class to address completely noncontiguous blocks of a Tensor_
///
/// StridedTensor_ is inherently view-like, but allows for non-contiguous data
///
/// \tparam T data type (e.g., double and dcomplex)
/// \tparam Rank rank of tensor (i.e., a Matrix is a rank 2 tensor)
// ----------------------------------------------------------------------------
template <typename T, int Rank>
class StridedTensor_ :
    public Indexable<T, StridedTensor_<T, Rank>, StridedLayout>,
    public TensorBase_<T, Rank, StridedTensor_<T, Rank>> {
 public:
  using DataType = T;
  using Layout = StridedLayout;

 protected:
  using TensorType = Tensor_<T, Rank>;
  using TensorViewType = TensorView_<T, Rank>;
  using SubTensorType = SubTensor_<T, Rank>;
  using StridedTensorType = StridedTensor_<T, Rank>;

  using IndexableType = Indexable<T, StridedTensorType, StridedLayout>;
  using TensorBaseType = TensorBase_<T, Rank, StridedTensorType>;

  WeakStorage<T> storage_;

  using TensorBaseType::extent_;
  using TensorBaseType::size_;
  using TensorBaseType::stride_;

 public:
  StridedTensor_(T* const d, const std::array<qtens_len_t, Rank>& extent,
                 const std::array<qtens_len_t, Rank>& stride, const qtens_len_t size) :
      TensorBaseType(extent, stride, size),
      storage_(d, std::get<Rank - 1>(stride) * std::get<Rank - 1>(extent))
  {}

  T* data() { return storage_.data(); }
  const T* data() const { return storage_.data(); }

  T& data(const qtens_len_t& i) { return storage_.data(i); }
  const T& data(const qtens_len_t& i) const { return storage_.data(i); }

  T& data(const qtens_len_t& i, const qtens_len_t& j)
  {
    return storage_.data(this->column_index(j) + i);
  }
  const T& data(const qtens_len_t& i, const qtens_len_t& j) const
  {
    return storage_.data(this->column_index(j) + i);
  }

  qtens_len_t column_index(qtens_len_t i) const
  {
    // This function comes down to mapping i -> (0, p, ...)
    // and then using (0, p, ...) and known strides to compute the index
    // Yikes.

    // what would strides be if the columns were numbered contiguously?

    qtens_len_t out = 0;
    for (qtens_len_t j = Rank - 1; j > 0; --j) {
      qtens_len_t dense_stride = 1ul;
      for (qtens_len_t is = 1; is < j; ++is) {
        dense_stride *= extent_[is];
      }

      const qtens_len_t idiv = i / dense_stride;
      i = i % dense_stride;
      out += idiv * stride_[j];
    }
    return out;
  }

  StridedTensor_& operator=(const StridedTensor_& x)
  {
    this->assign_from(x);
    return *this;
  }

  template <typename U, class L, class E>
  StridedTensor_& operator=(const Xpr<U, L, E>& x)
  {
    this->assign_from(x);
    return *this;
  }

  template <typename U, class A, class L>
  StridedTensor_& operator=(const Indexable<U, A, L>& x)
  {
    this->assign_from(x);
    return *this;
  }

  template <typename U>
  StridedTensor_& operator=(const U& x)
  {
    this->assign_from(x);
    return *this;
  }

  using TensorBaseType::operator();
  using TensorBaseType::at;

  qtens_len_t size() const { return size_; }

  WeakStorage<T>& storage() { return storage_; }
  const WeakStorage<T>& storage() const { return storage_; }

  using TensorBaseType::_norm_strided;
  double norm() const { return _norm_strided(); }

  TensorType clone() const { return (*this); }
  TensorType zeros_like() const { return TensorType(this->shape()); }

  template <int... ranks>
    requires(sizeof...(ranks) == Rank)
  TensorType reorder(const T alpha = 1.0, const T beta = 0.0) const
  {
    TensorType out(std::get<ranks>(this->shape())...);
    reorder_indices_strided<ranks...>(extent_, stride_, alpha, this->data(), out.stride(), beta,
                                      out.data());
    return std::move(out);
  }

  TensorType transpose() const
  {
    return this->transpose_impl(make_reversed_integer_sequence<int, Rank>{});
  }

  TensorType dagger() const
  {
    auto out = this->transpose();
    out = qleve::conj(out);
    return out;
  }

 private:
  template <int... ranks>
  TensorType transpose_impl(const std::integer_sequence<int, ranks...>&) const
  {
    return this->reorder<ranks...>();
  }
};

template <int Rank>
using StridedTensor = StridedTensor_<double, Rank>;
template <int Rank>
using ZStridedTensor = StridedTensor_<dcomplex, Rank>;

// --------------------------------------------------------------------------
/// \brief ConstStridedTensor_ is a class to address completely noncontiguous blocks of a
/// ConstTensorView_
///
/// ConstStridedTensor_ is inherently const view-like, but allows for non-contiguous data
///
/// \tparam T data type (e.g., double and dcomplex)
/// \tparam Rank rank of tensor (i.e., a Matrix is a rank 2 tensor)
// ----------------------------------------------------------------------------
template <typename T, int Rank>
class ConstStridedTensor_ :
    public Indexable<T, StridedTensor_<T, Rank>, StridedLayout>,
    public TensorBase_<T, Rank, ConstStridedTensor_<T, Rank>> {
 public:
  using DataType = T;
  using Layout = StridedLayout;

 protected:
  using TensorType = Tensor_<T, Rank>;
  using TensorViewType = TensorView_<T, Rank>;
  using SubTensorType = SubTensor_<T, Rank>;
  using StridedTensorType = StridedTensor_<T, Rank>;
  using ConstStridedTensorType = ConstStridedTensor_<T, Rank>;

  using IndexableType = Indexable<T, ConstStridedTensorType, StridedLayout>;
  using TensorBaseType = TensorBase_<T, Rank, ConstStridedTensorType>;

  ConstWeakStorage<T> storage_;

  using TensorBaseType::extent_;
  using TensorBaseType::size_;
  using TensorBaseType::stride_;

 public:
  ConstStridedTensor_(const T* const d, const std::array<qtens_len_t, Rank>& extent,
                      const std::array<qtens_len_t, Rank>& stride, const qtens_len_t size) :
      TensorBaseType(extent, stride, size),
      storage_(d, std::get<Rank - 1>(stride) * std::get<Rank - 1>(extent))
  {}

  const T* data() const { return storage_.data(); }

  /// probably not safe to use this function
  const T& data(const qtens_len_t& i) const { return storage_.data(i); }

  /// probably not safe to use this function
  const T& data(const qtens_len_t& i, const qtens_len_t& j) const
  {
    return storage_.data(this->column_index(j) + i);
  }

  qtens_len_t column_index(qtens_len_t i) const
  {
    // This function comes down to mapping i -> (0, p, ...)
    // and then using (0, p, ...) and known strides to compute the index
    // Yikes.

    // what would strides be if the columns were numbered contiguously?

    qtens_len_t out = 0;
    for (qtens_len_t j = Rank - 1; j > 0; --j) {
      qtens_len_t dense_stride = 1ul;
      for (qtens_len_t is = 1; is < j; ++is) {
        dense_stride *= extent_[is];
      }

      const qtens_len_t idiv = i / dense_stride;
      i = i % dense_stride;
      out += idiv * stride_[j];
    }
    return out;
  }

  /// access operator is redefined here to hide the non-const versions in the base class
  template <typename... Args>
  const T& operator()(const Args&... args) const
  {
    return static_cast<const TensorBaseType*>(this)->operator()(args...);
  }

  /// at is redefined here to hide the non-const versions in the base class
  template <typename... Args>
  const T& at(const Args&... args) const
  {
    return this->operator()(args...);
  }

  qtens_len_t size() const { return size_; }

  const ConstWeakStorage<T>& storage() const { return storage_; }

  using TensorBaseType::_norm_strided;
  double norm() const { return _norm_strided(); }

  TensorType clone() const { return (*this); }
  TensorType zeros_like() const { return TensorType(this->shape()); }

  template <int... ranks>
    requires(sizeof...(ranks) == Rank)
  TensorType reorder(const T alpha = 1.0, const T beta = 0.0) const
  {
    TensorType out(std::get<ranks>(this->shape())...);
    reorder_indices_strided<ranks...>(extent_, stride_, alpha, this->data(), out.stride(), beta,
                                      out.data());
    return std::move(out);
  }

  TensorType transpose() const
  {
    return this->transpose_impl(make_reversed_integer_sequence<int, Rank>{});
  }

  TensorType dagger() const
  {
    auto out = this->transpose();
    out = qleve::conj(out);
    return out;
  }

 private:
  template <int... ranks>
  TensorType transpose_impl(const std::integer_sequence<int, ranks...>&) const
  {
    return this->reorder<ranks...>();
  }
};

template <int Rank>
using ConstStridedTensor = ConstStridedTensor_<double, Rank>;
template <int Rank>
using ConstZStridedTensor = ConstStridedTensor_<dcomplex, Rank>;

} // namespace qleve

#endif // QLEVE_TENSOR_HPP
