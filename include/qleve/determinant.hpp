// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file include/qleve/determinant.hpp
///
/// Generic interface to compute a matrix determinant

#ifndef QLEVE_DETERMINANT_HPP
#define QLEVE_DETERMINANT_HPP

#include <tuple>

#include <qleve/blas_interface.hpp>
#include <qleve/pivoted_lu.hpp>
#include <qleve/tensor.hpp>
#include <qleve/tensor_traits.hpp>

namespace qleve::linalg
{

double determinant(real_matrixable auto&& A)
{
  const ptrdiff_t ndim = A.extent(0);
  if (ndim != A.extent(1)) {
    return 0.0;
  }

  auto tmp = A.clone();
  auto ipiv = linalg::pivoted_lu(tmp);

  double out = 1.0;
  for (ptrdiff_t i = 0; i < ndim; ++i) {
    out *= tmp(i, i);
  }

  ptrdiff_t npiv = 0;
  for (ptrdiff_t i = 0; i < ndim; ++i) {
    npiv += (ipiv[i] != i + 1);
  }
  if ((npiv % 2) == 1) {
    out *= -1;
  }

  return out;
}

} // namespace qleve::linalg

#endif // QLEVE_DETERMINANT_HPP
