// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file include/qleve/diagonalize_with_metric.hpp
///
/// Generic interface to Hermitian diagonalization of MatrixLike objects
/// including a metric, like an overlap matrix

#ifndef QLEVE_DIAGONALIZE_WITH_METRIC_HPP
#define QLEVE_DIAGONALIZE_WITH_METRIC_HPP

#include <vector>

#include <qleve/blas_interface.hpp>
#include <qleve/orthogonalize.hpp>
#include <qleve/pod_types.hpp>
#include <qleve/tensor.hpp>
#include <qleve/tensor_traits.hpp>

namespace qleve::linalg
{

void diagonalize_with_metric(real_matrixable auto&& A, const const_real_matrixable auto& B,
                             double* const eval, const double thresh = 1.0e-8)
{
  assert(A.extent(0) == A.extent(1) && B.extent(0) == B.extent(1) && A.extent(0) == B.extent(0));

  const ptrdiff_t n = A.extent(1);

  if (n == 0)
    return;

  auto U = orthogonalize_metric(B, thresh);
  auto Aorth = A.zeros_like();
  matrix_transform(1.0, A, U, 0.0, Aorth);

  diagonalize(Aorth, eval);
  gemm("n", "n", 1.0, U, Aorth, 0.0, A);
}

qleve::Tensor<1> diagonalize_with_metric(real_matrixable auto&& A,
                                         const const_real_matrixable auto& B,
                                         const double thresh = 1.0e-8)
{
  assert(A.extent(0) == A.extent(1) && B.extent(0) == B.extent(1) && A.extent(0) == B.extent(0));

  const ptrdiff_t n = A.extent(1);

  Tensor<1> eigenvalues(n);

  diagonalize_with_metric(A, B, eigenvalues.data(), thresh);
  return eigenvalues;
}

} // namespace qleve::linalg

#endif // QLEVE_DIAGONALIZE_WITH_METRIC_HPP
