#include "catch.hpp"

#include <qleve/array.hpp>

using namespace std;
using namespace qleve;

TEST_CASE("Array Tests", "[array]")
{

  Array arr(10);
  for (int i = 0; i < arr.size(); ++i) {
    arr(i) = i;
  }

  REQUIRE(arr.extent(0) == 10);
  REQUIRE(arr.size() == 10);

  const double refnorm = arr.norm();
  REQUIRE(refnorm == Approx(16.8819430161));

  SECTION("Can fill")
  {
    arr = 4.0;

    REQUIRE(arr(2) == 4.0);
    REQUIRE(arr.norm() == Approx(4.0 * std::sqrt(10)));
  }

  SECTION("Can copy")
  {
    Array a2(10);
    a2 = arr;

    REQUIRE(arr.norm() == Approx(a2.norm()));
  }

  SECTION("tensor = scalar * tensor")
  {
    Array a2(arr);

    arr = 1.1 * (a2 + 0.5);

    REQUIRE(arr(0) == Approx(1.1 * (a2(0) + 0.5)));
  }
}
