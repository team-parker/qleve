// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file include/qleve/indexable_traits.hpp
///
/// Type traits for indexables

#ifndef QLEVE_INDEXABLE_TRAITS_HPP
#define QLEVE_INDEXABLE_TRAITS_HPP

#include <cassert>
#include <type_traits>

#include <qleve/pod_types.hpp>
#include <qleve/trait_util.hpp>

namespace qleve
{

template <typename T>
struct is_std_array_of_ptrdiff_t : public std::false_type {};

template <size_t n>
struct is_std_array_of_ptrdiff_t<std::array<ptrdiff_t, n>> : public std::true_type {};

/// Requires that a shape is a std::array<qtens_len_t, N> for some N
template <class T>
concept shapeable = is_std_array_of_ptrdiff_t<T>::value && std::equality_comparable<T>;

/// Tests for a member function named shape with no other requirements.
template <class T>
struct has_shape {
 protected:
  template <class V>
  static std::true_type _implements_shape(decltype(&V::shape));
  template <class V>
  static std::false_type _implements_shape(...);

 public:
  static constexpr const bool value =
      std::is_same<std::true_type, decltype(_implements_shape<T>(0))>::value;
};

/// Concept for a type that has a member function named shape with no other
/// requirements.
template <class T>
concept shaped = requires(T v) {
  {
    v.shape()
  };
};

/// Tests for a member function of the type
/// \code
/// qtens_len_t size() const;
/// \endcode
template <class T, typename = void>
struct has_size : std::false_type {};

/// Tests for a member function of the type
/// \code
/// qtens_len_t size() const;
/// \endcode
template <class T>
struct has_size<T, std::void_t<decltype(std::declval<const T>().size())>> : std::true_type {};

template <class T>
concept sized = requires(T v) {
  {
    v.size()
  } -> std::same_as<qtens_len_t>;
};

/// Tests for a member function of the type
/// \code
/// qtens_len_t nrows() const;
/// \endcode
template <class T, typename = void>
struct has_nrows : std::false_type {};

/// Tests for a member function of the type
/// \code
/// qtens_len_t nrows() const;
/// \endcode
template <class T>
struct has_nrows<T, std::void_t<decltype(std::declval<const T>().nrows())>> : std::true_type {};

template <class T>
concept nrowed = requires(T v) {
  {
    v.nrows()
  } -> std::same_as<qtens_len_t>;
};

/// Tests for a member function of the type
/// \code
/// qtens_len_t ncolumns() const;
/// \endcode
template <class T, typename = void>
struct has_ncolumns : std::false_type {};

/// Tests for a member function of the type
/// \code
/// qtens_len_t ncolumns() const;
/// \endcode
template <class T>
struct has_ncolumns<T, std::void_t<decltype(std::declval<const T>().ncolumns())>> :
    std::true_type {};


template <class T>
concept ncolumned = requires(T v) {
  {
    v.ncolumns()
  } -> std::same_as<qtens_len_t>;
};

/// Tests for a member function of the type
/// \code
/// T* data();
/// \endcode
template <class T, typename DataType, typename = void>
struct has_nonconst_data_operator : std::false_type {};

/// Tests for a member function of the type
/// \code
/// T* data();
/// \endcode
template <class T, typename DataType>
struct has_nonconst_data_operator<
    T, DataType,
    typename std::enable_if<
        std::is_same<DataType*, decltype(std::declval<T>().data())>::value>::type> :
    std::true_type {};

template <class T>
concept nonconst_datable = requires(T v) {
  {
    v.data()
  } -> std::floating_point;
};

/// Tests for a member function of the type
/// \code
/// const T* data() const;
/// \endcode
template <class T, typename DataType, typename = void>
struct has_const_data_operator : std::false_type {};

/// Tests for a member function of the type
/// \code
/// const T* data() const;
/// \endcode
template <class T, typename DataType>
struct has_const_data_operator<
    T, DataType,
    typename std::enable_if<
        std::is_same<const DataType*, decltype(std::declval<const T>().data())>::value>::type> :
    std::true_type {};

template <class T>
concept const_datable = requires(const T v) {
  {
    v.data()
  } -> std::floating_point;
};

/// Tests for a member function of the type
/// \code
/// T& data(const qtens_len_t);
/// \endcode
template <class T, typename DataType, typename = void>
struct has_nonconst_access_operator : std::false_type {};

/// Tests for a member function of the type
/// \code
/// T& data(const qtens_len_t);
/// \endcode
template <class T, typename DataType>
struct has_nonconst_access_operator<
    T, DataType,
    typename std::enable_if<
        std::is_same<DataType&, decltype(std::declval<T>().data(qtens_len_t()))>::value>::type> :
    std::true_type {};

template <class T>
concept nonconst_accessible = requires(T v, qtens_len_t i) {
  {
    v.data(i)
  } -> std::floating_point;
};

/// Tests for a member function of the type
/// \code
/// const T& data(const qtens_len_t) const;
/// \endcode
template <class T, typename DataType, typename = void>
struct has_const_access_operator : std::false_type {};

/// Tests for a member function of the type
/// \code
/// const T& data(const qtens_len_t) const;
/// \endcode
template <class T, typename DataType>
struct has_const_access_operator<
    T, DataType,
    typename std::enable_if<std::is_same<const DataType&, decltype(std::declval<const T>().data(
                                                              qtens_len_t()))>::value>::type> :
    std::true_type {};

template <class T>
concept const_accessible = requires(const T v, qtens_len_t i) {
  {
    v.data(i)
  } -> std::floating_point;
};

/// Tests for a rectangular access member function of the type
/// \code
/// T& data(const qtens_len_t, const qtens_len_t);
/// \endcode
template <class T, typename DataType, typename = void>
struct has_nonconst_rect_access_operator : std::false_type {};

/// Tests for a rectangular access member function of the type
/// \code
/// T& data(const qtens_len_t, const qtens_len_t);
/// \endcode
template <class T, typename DataType>
struct has_nonconst_rect_access_operator<
    T, DataType,
    typename std::enable_if<std::is_same<
        DataType&, decltype(std::declval<T>().data(qtens_len_t(), qtens_len_t()))>::value>::type> :
    std::true_type {};

template <class T>
concept nonconst_rect_accessible = requires(T v, qtens_len_t i) {
  {
    v.data(i, i)
  } -> std::floating_point;
};

/// Tests for a rectangular access member function of the type
/// \code
/// const T& data(const qtens_len_t, const qtens_len_t) const;
/// \endcode
template <class T, typename DataType, typename = void>
struct has_const_rect_access_operator : std::false_type {};

/// Tests for a rectangular access member function of the type
/// \code
/// const T& data(const qtens_len_t, const qtens_len_t) const;
/// \endcode
template <class T, typename DataType>
struct has_const_rect_access_operator<
    T, DataType,
    typename std::enable_if<
        std::is_same<const DataType&, decltype(std::declval<const T>().data(
                                          qtens_len_t(), qtens_len_t()))>::value>::type> :
    std::true_type {};

template <class T>
concept const_rect_accessible = requires(const T v, qtens_len_t i) {
  {
    v.data(i, i)
  } -> std::floating_point;
};

/// Tests whether useable as a coefficient/scalar in indexable code, i.e.,
/// whether T is of type float, double, long double,
/// complex<float>, or complex<double>, or complex<long double>
template <typename T>
using is_floating_coefficient =
    is_any<std::remove_cv_t<T>, float, double, long double, std::complex<float>,
           std::complex<double>, std::complex<long double>>;

template <typename T>
inline constexpr bool is_floating_coefficient_v = is_floating_coefficient<T>::value;

template <typename T>
concept coeffable = is_floating_coefficient_v<T>;

/// Test whether class D satisfies requirements for Indexable with datatype T
template <class D, typename T>
using is_indexable =
    std::integral_constant<bool, has_size<D>::value && has_const_access_operator<D, T>::value
                                     && has_nonconst_access_operator<D, T>::value
                                     && has_const_rect_access_operator<D, T>::value
                                     && has_nonconst_rect_access_operator<D, T>::value
                                     && has_shape<D>::value && has_nrows<D>::value
                                     && has_ncolumns<D>::value>;

template <class T>
concept indexable =
    sized<T> && const_accessible<T> && nonconst_accessible<T> && const_rect_accessible<T>
    && nonconst_rect_accessible<T> && shaped<T> && nrowed<T> && ncolumned<T>;

/// Test whether class D satisfies requirements for const Indexable with datatype T
template <class D, typename T>
using is_const_indexable =
    std::integral_constant<bool, has_size<D>::value && has_const_access_operator<D, T>::value
                                     && has_const_rect_access_operator<D, T>::value
                                     && has_shape<D>::value && has_nrows<D>::value
                                     && has_ncolumns<D>::value>;

template <class T>
concept const_indexable = sized<T> && const_accessible<T> && const_rect_accessible<T> && shaped<T>
                          && nrowed<T> && ncolumned<T>;

} // namespace qleve

#endif // QLEVE_INDEXABLE_TRAITS_HPP
