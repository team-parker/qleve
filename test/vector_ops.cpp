#include "catch.hpp"

#include <qleve/array.hpp>
#include <qleve/excluded_product.hpp>

using namespace std;
using namespace qleve;

TEST_CASE("Vector Operation Tests", "[vector_ops]")
{

  Array arr(5);
  for (int i = 0; i < arr.size(); ++i) {
    arr(i) = i + 2;
  }

  REQUIRE(arr.extent(0) == 5);
  REQUIRE(arr.size() == 5);

  SECTION("One excluded product")
  {
    auto xp1 = excluded_product_1(arr);

    CHECK(xp1(0) == Approx(360.0));
    CHECK(xp1(1) == Approx(240.0));
    CHECK(xp1(2) == Approx(180.0));
    CHECK(xp1(3) == Approx(144.0));
    CHECK(xp1(4) == Approx(120.0));
  }

  SECTION("Two excluded product")
  {
    auto [xp1, xp2] = excluded_product_2(arr);

    CHECK(xp1(0) == Approx(360.0));
    CHECK(xp1(1) == Approx(240.0));
    CHECK(xp1(2) == Approx(180.0));
    CHECK(xp1(3) == Approx(144.0));
    CHECK(xp1(4) == Approx(120.0));

    CHECK(xp2(0, 0) == Approx(1.0));
    CHECK(xp2(1, 0) == Approx(120.0));
    CHECK(xp2(2, 0) == Approx(90.0));
    CHECK(xp2(3, 0) == Approx(72.0));
    CHECK(xp2(4, 0) == Approx(60.0));
    CHECK(xp2(0, 1) == Approx(120.0));
    CHECK(xp2(1, 1) == Approx(1.0));
    CHECK(xp2(2, 1) == Approx(60.0));
    CHECK(xp2(3, 1) == Approx(48.0));
    CHECK(xp2(4, 1) == Approx(40.0));
    CHECK(xp2(0, 2) == Approx(90.0));
    CHECK(xp2(1, 2) == Approx(60.0));
    CHECK(xp2(2, 2) == Approx(1.0));
    CHECK(xp2(3, 2) == Approx(36.0));
    CHECK(xp2(4, 2) == Approx(30.0));
    CHECK(xp2(0, 3) == Approx(72.0));
    CHECK(xp2(1, 3) == Approx(48.0));
    CHECK(xp2(2, 3) == Approx(36.0));
    CHECK(xp2(3, 3) == Approx(1.0));
    CHECK(xp2(4, 3) == Approx(24.0));
    CHECK(xp2(0, 4) == Approx(60.0));
    CHECK(xp2(1, 4) == Approx(40.0));
    CHECK(xp2(2, 4) == Approx(30.0));
    CHECK(xp2(3, 4) == Approx(24.0));
    CHECK(xp2(4, 4) == Approx(1.0));
  }

  SECTION("Three excluded product")
  {
    auto [xp1, xp2, xp3] = excluded_product_3(arr);

    CHECK(xp1(0) == Approx(360.0));
    CHECK(xp1(1) == Approx(240.0));
    CHECK(xp1(2) == Approx(180.0));
    CHECK(xp1(3) == Approx(144.0));
    CHECK(xp1(4) == Approx(120.0));

    CHECK(xp2(0, 0) == Approx(1.0));
    CHECK(xp2(1, 0) == Approx(120.0));
    CHECK(xp2(2, 0) == Approx(90.0));
    CHECK(xp2(3, 0) == Approx(72.0));
    CHECK(xp2(4, 0) == Approx(60.0));
    CHECK(xp2(0, 1) == Approx(120.0));
    CHECK(xp2(1, 1) == Approx(1.0));
    CHECK(xp2(2, 1) == Approx(60.0));
    CHECK(xp2(3, 1) == Approx(48.0));
    CHECK(xp2(4, 1) == Approx(40.0));
    CHECK(xp2(0, 2) == Approx(90.0));
    CHECK(xp2(1, 2) == Approx(60.0));
    CHECK(xp2(2, 2) == Approx(1.0));
    CHECK(xp2(3, 2) == Approx(36.0));
    CHECK(xp2(4, 2) == Approx(30.0));
    CHECK(xp2(0, 3) == Approx(72.0));
    CHECK(xp2(1, 3) == Approx(48.0));
    CHECK(xp2(2, 3) == Approx(36.0));
    CHECK(xp2(3, 3) == Approx(1.0));
    CHECK(xp2(4, 3) == Approx(24.0));
    CHECK(xp2(0, 4) == Approx(60.0));
    CHECK(xp2(1, 4) == Approx(40.0));
    CHECK(xp2(2, 4) == Approx(30.0));
    CHECK(xp2(3, 4) == Approx(24.0));
    CHECK(xp2(4, 4) == Approx(1.0));

    CHECK(xp3(0, 0, 0) == Approx(1.0));
    CHECK(xp3(1, 0, 0) == Approx(1.0));
    CHECK(xp3(2, 0, 0) == Approx(1.0));
    CHECK(xp3(3, 0, 0) == Approx(1.0));
    CHECK(xp3(4, 0, 0) == Approx(1.0));
    CHECK(xp3(0, 1, 0) == Approx(1.0));
    CHECK(xp3(1, 1, 0) == Approx(1.0));
    CHECK(xp3(2, 1, 0) == Approx(30.0));
    CHECK(xp3(3, 1, 0) == Approx(24.0));
    CHECK(xp3(4, 1, 0) == Approx(20.0));
    CHECK(xp3(0, 2, 0) == Approx(1.0));
    CHECK(xp3(1, 2, 0) == Approx(30.0));
    CHECK(xp3(2, 2, 0) == Approx(1.0));
    CHECK(xp3(3, 2, 0) == Approx(18.0));
    CHECK(xp3(4, 2, 0) == Approx(15.0));
    CHECK(xp3(0, 3, 0) == Approx(1.0));
    CHECK(xp3(1, 3, 0) == Approx(24.0));
    CHECK(xp3(2, 3, 0) == Approx(18.0));
    CHECK(xp3(3, 3, 0) == Approx(1.0));
    CHECK(xp3(4, 3, 0) == Approx(12.0));
    CHECK(xp3(0, 4, 0) == Approx(1.0));
    CHECK(xp3(1, 4, 0) == Approx(20.0));
    CHECK(xp3(2, 4, 0) == Approx(15.0));
    CHECK(xp3(3, 4, 0) == Approx(12.0));
    CHECK(xp3(4, 4, 0) == Approx(1.0));

    CHECK(xp3(0, 0, 1) == Approx(1.0));
    CHECK(xp3(1, 0, 1) == Approx(1.0));
    CHECK(xp3(2, 0, 1) == Approx(30.0));
    CHECK(xp3(3, 0, 1) == Approx(24.0));
    CHECK(xp3(4, 0, 1) == Approx(20.0));
    CHECK(xp3(0, 1, 1) == Approx(1.0));
    CHECK(xp3(1, 1, 1) == Approx(1.0));
    CHECK(xp3(2, 1, 1) == Approx(1.0));
    CHECK(xp3(3, 1, 1) == Approx(1.0));
    CHECK(xp3(4, 1, 1) == Approx(1.0));
    CHECK(xp3(0, 2, 1) == Approx(30.0));
    CHECK(xp3(1, 2, 1) == Approx(1.0));
    CHECK(xp3(2, 2, 1) == Approx(1.0));
    CHECK(xp3(3, 2, 1) == Approx(12.0));
    CHECK(xp3(4, 2, 1) == Approx(10.0));
    CHECK(xp3(0, 3, 1) == Approx(24.0));
    CHECK(xp3(1, 3, 1) == Approx(1.0));
    CHECK(xp3(2, 3, 1) == Approx(12.0));
    CHECK(xp3(3, 3, 1) == Approx(1.0));
    CHECK(xp3(4, 3, 1) == Approx(8.0));
    CHECK(xp3(0, 4, 1) == Approx(20.0));
    CHECK(xp3(1, 4, 1) == Approx(1.0));
    CHECK(xp3(2, 4, 1) == Approx(10.0));
    CHECK(xp3(3, 4, 1) == Approx(8.0));
    CHECK(xp3(4, 4, 1) == Approx(1.0));

    CHECK(xp3(0, 0, 2) == Approx(1.0));
    CHECK(xp3(1, 0, 2) == Approx(30.0));
    CHECK(xp3(2, 0, 2) == Approx(1.0));
    CHECK(xp3(3, 0, 2) == Approx(18.0));
    CHECK(xp3(4, 0, 2) == Approx(15.0));
    CHECK(xp3(0, 1, 2) == Approx(30.0));
    CHECK(xp3(1, 1, 2) == Approx(1.0));
    CHECK(xp3(2, 1, 2) == Approx(1.0));
    CHECK(xp3(3, 1, 2) == Approx(12.0));
    CHECK(xp3(4, 1, 2) == Approx(10.0));
    CHECK(xp3(0, 2, 2) == Approx(1.0));
    CHECK(xp3(1, 2, 2) == Approx(1.0));
    CHECK(xp3(2, 2, 2) == Approx(1.0));
    CHECK(xp3(3, 2, 2) == Approx(1.0));
    CHECK(xp3(4, 2, 2) == Approx(1.0));
    CHECK(xp3(0, 3, 2) == Approx(18.0));
    CHECK(xp3(1, 3, 2) == Approx(12.0));
    CHECK(xp3(2, 3, 2) == Approx(1.0));
    CHECK(xp3(3, 3, 2) == Approx(1.0));
    CHECK(xp3(4, 3, 2) == Approx(6.0));
    CHECK(xp3(0, 4, 2) == Approx(15.0));
    CHECK(xp3(1, 4, 2) == Approx(10.0));
    CHECK(xp3(2, 4, 2) == Approx(1.0));
    CHECK(xp3(3, 4, 2) == Approx(6.0));
    CHECK(xp3(4, 4, 2) == Approx(1.0));

    CHECK(xp3(0, 0, 3) == Approx(1.0));
    CHECK(xp3(1, 0, 3) == Approx(24.0));
    CHECK(xp3(2, 0, 3) == Approx(18.0));
    CHECK(xp3(3, 0, 3) == Approx(1.0));
    CHECK(xp3(4, 0, 3) == Approx(12.0));
    CHECK(xp3(0, 1, 3) == Approx(24.0));
    CHECK(xp3(1, 1, 3) == Approx(1.0));
    CHECK(xp3(2, 1, 3) == Approx(12.0));
    CHECK(xp3(3, 1, 3) == Approx(1.0));
    CHECK(xp3(4, 1, 3) == Approx(8.0));
    CHECK(xp3(0, 2, 3) == Approx(18.0));
    CHECK(xp3(1, 2, 3) == Approx(12.0));
    CHECK(xp3(2, 2, 3) == Approx(1.0));
    CHECK(xp3(3, 2, 3) == Approx(1.0));
    CHECK(xp3(4, 2, 3) == Approx(6.0));
    CHECK(xp3(0, 3, 3) == Approx(1.0));
    CHECK(xp3(1, 3, 3) == Approx(1.0));
    CHECK(xp3(2, 3, 3) == Approx(1.0));
    CHECK(xp3(3, 3, 3) == Approx(1.0));
    CHECK(xp3(4, 3, 3) == Approx(1.0));
    CHECK(xp3(0, 4, 3) == Approx(12.0));
    CHECK(xp3(1, 4, 3) == Approx(8.0));
    CHECK(xp3(2, 4, 3) == Approx(6.0));
    CHECK(xp3(3, 4, 3) == Approx(1.0));
    CHECK(xp3(4, 4, 3) == Approx(1.0));

    CHECK(xp3(0, 0, 4) == Approx(1.0));
    CHECK(xp3(1, 0, 4) == Approx(20.0));
    CHECK(xp3(2, 0, 4) == Approx(15.0));
    CHECK(xp3(3, 0, 4) == Approx(12.0));
    CHECK(xp3(4, 0, 4) == Approx(1.0));
    CHECK(xp3(0, 1, 4) == Approx(20.0));
    CHECK(xp3(1, 1, 4) == Approx(1.0));
    CHECK(xp3(2, 1, 4) == Approx(10.0));
    CHECK(xp3(3, 1, 4) == Approx(8.0));
    CHECK(xp3(4, 1, 4) == Approx(1.0));
    CHECK(xp3(0, 2, 4) == Approx(15.0));
    CHECK(xp3(1, 2, 4) == Approx(10.0));
    CHECK(xp3(2, 2, 4) == Approx(1.0));
    CHECK(xp3(3, 2, 4) == Approx(6.0));
    CHECK(xp3(4, 2, 4) == Approx(1.0));
    CHECK(xp3(0, 3, 4) == Approx(12.0));
    CHECK(xp3(1, 3, 4) == Approx(8.0));
    CHECK(xp3(2, 3, 4) == Approx(6.0));
    CHECK(xp3(3, 3, 4) == Approx(1.0));
    CHECK(xp3(4, 3, 4) == Approx(1.0));
    CHECK(xp3(0, 4, 4) == Approx(1.0));
    CHECK(xp3(1, 4, 4) == Approx(1.0));
    CHECK(xp3(2, 4, 4) == Approx(1.0));
    CHECK(xp3(3, 4, 4) == Approx(1.0));
    CHECK(xp3(4, 4, 4) == Approx(1.0));

  }
}
