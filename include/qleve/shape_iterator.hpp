// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file include/qleve/indexable.hpp
///
/// Indexable is the essential class implementing Expression Template
/// machinery, but it otherwise contains no class members.

#ifndef QLEVE_SHAPE_ITERATOR_HPP
#define QLEVE_SHAPE_ITERATOR_HPP

#include <cassert>
#include <iostream>
#include <memory>
#include <utility>

#include <qleve/detail.hpp>
#include <qleve/indexable_traits.hpp>
#include <qleve/layout.hpp>
#include <qleve/pod_types.hpp>
#include <qleve/vector_ops.hpp>

namespace qleve
{

template <int rank>
class shape_iterator_ {
 protected:
  std::array<ptrdiff_t, rank> shape_;
  std::array<ptrdiff_t, rank> i_;

 public:
  using shape_type = std::array<ptrdiff_t, rank>;

  shape_iterator_(const shape_type& shape) : shape_(shape), i_() {}

  shape_iterator_(const shape_type& shape, const shape_type& i) : shape_(shape), i_(i) {}

  shape_iterator_(const shape_iterator_& other) = default;

  shape_iterator_& operator=(const shape_iterator_& other) = default;

  shape_iterator_& operator++()
  {
    for (int j = 0; j < rank; ++j) {
      if (i_[j] < shape_[j] - 1) {
        ++i_[j];
        return *this;
      } else {
        i_[j] = 0;
      }
    }
    // should only get here when done iterating
    i_ = shape_;
    return *this;
  }

  shape_iterator_& operator--()
  {
    for (int j = 0; j < rank; ++j) {
      if (i_[j] > 0) {
        --i_[j];
        return *this;
      } else {
        i_[j] = shape_[j] - 1;
      }
    }
    return *this;
  }

  shape_iterator_& operator+=(ptrdiff_t n)
  {
    if (n < 0) {
      for (ptrdiff_t k = 0; k < -n; ++k) {
        --(*this);
      }
    } else {
      for (ptrdiff_t k = 0; k < n; ++k) {
        ++(*this);
      }
    }
    return *this;
  }

  shape_iterator_& operator-=(ptrdiff_t n)
  {
    if (n < 0) {
      for (ptrdiff_t k = 0; k < -n; ++k) {
        ++(*this);
      }
    } else {
      for (ptrdiff_t k = 0; k < n; ++k) {
        --(*this);
      }
    }
    return *this;
  }

  shape_iterator_ operator+(ptrdiff_t n) const
  {
    shape_iterator_ it(*this);
    it += n;
    return it;
  }

  shape_iterator_ operator-(ptrdiff_t n) const
  {
    shape_iterator_ it(*this);
    it -= n;
    return it;
  }

  bool operator<=>(const shape_iterator_& other) const { return i_ <=> other.i_; }

  bool operator==(const shape_iterator_& other) const { return i_ == other.i_; }

  std::array<ptrdiff_t, rank> operator*() const { return i_; }
};

template <int rank>
class shape_range_ {
 protected:
  std::array<ptrdiff_t, rank> shape_;

  using iterator = shape_iterator_<rank>;

 public:
  shape_range_(const std::array<ptrdiff_t, rank>& shape) : shape_(shape) {}

  shape_range_(const shape_range_& other) = default;

  iterator begin() const { return iterator(shape_); }

  iterator end() const { return iterator(shape_, shape_); }
};

} // namespace qleve

#endif // QLEVE_SHAPE_ITERATOR_HPP
