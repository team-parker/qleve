// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file include/qleve/storage.hpp
///
/// Storage classes for tensors

#ifndef QLEVE_STORAGE_HPP
#define QLEVE_STORAGE_HPP

#define USE_MKL_MALLOC 1

#include <memory>

#include <mkl.h>

#include <qleve/pod_types.hpp>

namespace qleve
{

constexpr int memory_alignment = 64;

/**
 * Allocated storage class, which is essentially a wrapper around a unique_ptr.
 * Optionally will keep track of total storage in the class when MEMORY_DEBUG is set.
 * No bounds checking as that should be done by the container.
 */
template <typename T>
class UniqueStorage {
#if USE_MKL_MALLOC
 private:
  struct mkl_deleter {
    void operator()(T* ptr) const { mkl_free(ptr); }
  };
#endif

 public:
  typedef T datatype;

 protected:
#if USE_MKL_MALLOC
  using deleter = mkl_deleter;
#else
  using deleter = std::default_delete<T[]>;
#endif
  std::unique_ptr<T[], deleter> data_;
  std::size_t size_;

#ifdef MEMORY_DEBUG
  static size_t total_allocated_;
#endif

 public:
  UniqueStorage(const size_t nwords) : size_(nwords)
  {
#if USE_MKL_MALLOC
    data_ = std::unique_ptr<T[], deleter>(
        reinterpret_cast<T*>(mkl_malloc(nwords * sizeof(T), memory_alignment)));
#else
    data_ = std::unique_ptr<T[], deleter>(new T[nwords]);
#endif

#ifdef MEMORY_DEBUG
    total_allocated_ += nwords;
    size_ = nwords;
#endif
  }

  UniqueStorage(UniqueStorage&& o) : data_(std::move(o.data_)), size_(o.size_)
  {
#ifdef MEMORY_DEBUG
    size_ = o.size_;
    o.size_ = T(0.0);
#endif
  }

  virtual ~UniqueStorage()
  {
#ifdef MEMORY_DEBUG
    if (data_)
      total_allocated_ -= size_;
#endif
  }

  UniqueStorage& operator=(const UniqueStorage& o) = delete;

  UniqueStorage& operator=(UniqueStorage&& o)
  {
    swap(*this, o);
    return *this;
  }

  T& data(const qtens_len_t& i)
  {
    assert(i >= 0 && i < size_);
    return data_[i];
  }
  const T& data(const qtens_len_t& i) const
  {
    assert(i >= 0 && i < size_);
    return data_[i];
  }

  explicit operator bool() const { return static_cast<bool>(data_); }

  T* data() { return data_.get(); }
  const T* data() const { return data_.get(); }

  T* release()
  {
#ifdef MEMORY_DEBUG
    total_allocated_ -= size_;
    size_ = 0;
#endif
    return data_.release();
  }
};

/**
 * Non-allocated storage class to be used or chunks of memory that are already allocated.
 * Has no capabilities to check validity of the underlying pointer, so should only be used
 * if it can be guaranteed that its memory will remain valid for its entire lifetime.
 */
template <typename T>
class WeakStorage {
 public:
  typedef T datatype;

 protected:
  T* const data_;
  size_t size_;

 public:
  WeakStorage(T* const data, const size_t size) : data_(data), size_(size) {}

  WeakStorage(UniqueStorage<T>& uniq, const size_t size) : WeakStorage(uniq.data(), size) {}

  WeakStorage(WeakStorage<T>& weak, const size_t size) : WeakStorage(weak.data(), size) {}

  virtual ~WeakStorage() = default;

  WeakStorage(const WeakStorage<T>& o) = default;
  WeakStorage& operator=(const WeakStorage<T>& o) = default;
  WeakStorage& operator=(WeakStorage<T>&& o) = default;

  T* data() { return data_; }
  const T* data() const { return data_; }

  T& data(const qtens_len_t& i)
  {
    assert(i >= 0 && i < size_);
    return data_[i];
  }
  const T& data(const qtens_len_t& i) const
  {
    assert(i >= 0 && i < size_);
    return data_[i];
  }
};

/**
 * Const version of WeakStorage, for when you really need to trust that the data won't be altered
 */
template <typename T>
class ConstWeakStorage {
 public:
  typedef T datatype;

 protected:
  const T* const data_;
  const size_t size_;

 public:
  ConstWeakStorage(const T* const data, const size_t size) : data_(data), size_(size) {}

  ConstWeakStorage(const UniqueStorage<T>& uniq, const size_t size) :
      ConstWeakStorage(uniq.data(), size)
  {}

  ConstWeakStorage(const WeakStorage<T>& weak, const size_t size) :
      ConstWeakStorage(weak.data(), size)
  {}

  ConstWeakStorage(const ConstWeakStorage<T>& weak, const size_t size) :
      ConstWeakStorage(weak.data(), size)
  {}

  virtual ~ConstWeakStorage() = default;

  ConstWeakStorage(const ConstWeakStorage&) = default;
  ConstWeakStorage& operator=(const ConstWeakStorage<T>&) = default;
  ConstWeakStorage& operator=(ConstWeakStorage<T>&&) = default;

  const T* data() const { return data_; }

  const T& data(const qtens_len_t& i) const
  {
    assert(i >= 0 && i < size_);
    return data_[i];
  }
};

} // namespace qleve

#endif // QLEVE_STORAGE_HPP
