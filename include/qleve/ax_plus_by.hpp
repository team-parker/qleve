// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file include/qleve/ax_plus_by.hpp
///
/// Generic implementation using the vector intrinsics library for
///   y <- alpha * x + beta * y
/// for alpha, beta constants and x, y vectors

#ifndef QLEVE_AX_PLUS_BY_HPP
#define QLEVE_AX_PLUS_BY_HPP

#include <array>
#include <cassert>
#include <type_traits>

#include <qleve/pod_types.hpp>

namespace qleve
{

// Y = alpha * X + beta * Y

template <typename A, typename X, typename B, typename Y>
  requires(std::is_same_v<X, Y> && std::is_convertible_v<A, X> && std::is_convertible_v<B, Y>)
void ax_plus_by(const size_t& n, const A& a, const X* const x, const size_t& incx, const B& b,
                Y* const y, const size_t& incy)
{
  assert(incx > 0 && incy > 0);

  for (size_t i = 0; i < n; ++i) {
    const size_t ix = i * incx;
    const size_t iy = i * incy;

    y[iy] = a * x[ix] + b * y[iy];
  }
}

template <typename A, typename X, typename B, typename Y>
void ax_plus_by(const size_t& n, const A& a, const X* const x, const B& b, Y* const y)
{
  ax_plus_by(n, a, x, 1ull, b, y, 1ull);
}

#ifdef HAVE_MKL
template <>
void ax_plus_by(const size_t& n, const double& a, const double* const x, const size_t& incx,
                const double& b, double* const y, const size_t& incy)
{
  daxpby_(n, a, x, incx, b, y, incy);
}

template <>
void ax_plus_by(const size_t& n, const dcomplex& a, const dcomplex* const x, const size_t& incx,
                const dcomplex& b, dcomplex* const y, const size_t& incy)
{
  zaxpby_(n, a, x, incx, b, y, incy);
}
#endif // HAVE_MKL

} // namespace qleve

#endif // QLEVE_AX_PLUS_BY_HPP
