// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file include/qleve/reorder_indices.hpp
///
/// Transpose-like functions

#ifndef QLEVE_REORDER_INDICES_HPP
#define QLEVE_REORDER_INDICES_HPP

#include <numeric>
#include <tuple>
#include <type_traits>

#include <qleve/pod_types.hpp>

namespace qleve
{

/// two-index reorder is basically just a transpose, but here for convenience
template <int I, int J, typename T1, typename T2, typename U1, typename U2,
          typename T = decltype(std::declval<T1>() * std::declval<T2>()
                                + std::declval<U1>() * std::declval<U2>())>
  requires std::is_convertible<U2, T>::value
void reorder_indices(const qtens_len_t np, const qtens_len_t nq, const T1 alpha, const T2* source,
                     const U1 beta, U2* target)
{
  static_assert((I == 0 || I == 1) && (J == 0 || J == 1) && I != J,
                "Reordered indices must be different and 0 or 1");

  auto src_extents = std::make_tuple(np, nq);
  auto src_strides = std::make_tuple(1, np);

  auto tar_extents = std::make_tuple(std::get<I>(src_extents), std::get<J>(src_extents));
  auto tar_strides = std::make_tuple(1, std::get<0>(tar_extents));

  for (qtens_len_t j = 0; j < std::get<1>(tar_extents); ++j) {
    for (qtens_len_t i = 0; i < std::get<0>(tar_extents); ++i) {
      const qtens_len_t it = std::get<0>(tar_strides) * i + std::get<1>(tar_strides) * j;
      const qtens_len_t is = std::get<I>(src_strides) * i + std::get<J>(src_strides) * j;
      target[it] = alpha * source[is] + beta * target[it];
    }
  }
}

/// three-index reorder
template <int I, int J, int K, typename T1, typename T2, typename U1, typename U2,
          typename T = decltype(std::declval<T1>() * std::declval<T2>()
                                + std::declval<U1>() * std::declval<U2>())>
  requires std::is_convertible<U2, T>::value
void reorder_indices(const qtens_len_t np, const qtens_len_t nq, const qtens_len_t nr,
                     const T1 alpha, const T2* source, const U1 beta, U2* target)
{
  static_assert((I >= 0 && I <= 2) && (J >= 0 && J <= 2) && (K >= 0 && K <= 2),
                "Reordered indices must be 0, 1, or 2");
  static_assert(!(I == J || J == K || I == K), "Reordered indices must all be different");

  auto src_extents = std::make_tuple(np, nq, nr);
  auto src_strides = std::make_tuple(1, np, np * nq);

  auto tar_extents =
      std::make_tuple(std::get<I>(src_extents), std::get<J>(src_extents), std::get<K>(src_extents));
  auto tar_strides = std::make_tuple(1, std::get<0>(tar_extents),
                                     std::get<0>(tar_extents) * std::get<1>(tar_extents));

  for (qtens_len_t k = 0; k < std::get<2>(tar_extents); ++k) {
    for (qtens_len_t j = 0; j < std::get<1>(tar_extents); ++j) {
      for (qtens_len_t i = 0; i < std::get<0>(tar_extents); ++i) {
        const qtens_len_t it = std::get<0>(tar_strides) * i + std::get<1>(tar_strides) * j
                               + std::get<2>(tar_strides) * k;
        const qtens_len_t is = std::get<I>(src_strides) * i + std::get<J>(src_strides) * j
                               + std::get<K>(src_strides) * k;
        target[it] = alpha * source[is] + beta * target[it];
      }
    }
  }
}

/// four-index reorder
template <int I, int J, int K, int L, typename T1, typename T2, typename U1, typename U2,
          typename T = decltype(std::declval<T1>() * std::declval<T2>()
                                + std::declval<U1>() * std::declval<U2>())>
  requires std::is_convertible<U2, T>::value
void reorder_indices(const qtens_len_t np, const qtens_len_t nq, const qtens_len_t nr,
                     const qtens_len_t ns, const T1 alpha, const T2* source, const U1 beta,
                     U2* target)
{
  static_assert(
      (I >= 0 && I <= 3) && (J >= 0 && J <= 3) && (K >= 0 && K <= 3) && (L >= 0 && L <= 3),
      "Reordered indices must be 0, 1, 2 or 3");
  static_assert(!(I == J || J == K || K == L || I == K || I == L || J == L),
                "Reordered indices must all be different");

  auto src_extents = std::make_tuple(np, nq, nr, ns);
  auto src_strides = std::make_tuple(1, np, np * nq, np * nq * nr);

  auto tar_extents = std::make_tuple(std::get<I>(src_extents), std::get<J>(src_extents),
                                     std::get<K>(src_extents), std::get<L>(src_extents));
  auto tar_strides = std::make_tuple(
      1, std::get<0>(tar_extents), std::get<0>(tar_extents) * std::get<1>(tar_extents),
      std::get<0>(tar_extents) * std::get<1>(tar_extents) * std::get<2>(tar_extents));

  for (qtens_len_t l = 0; l < std::get<3>(tar_extents); ++l) {
    for (qtens_len_t k = 0; k < std::get<2>(tar_extents); ++k) {
      for (qtens_len_t j = 0; j < std::get<1>(tar_extents); ++j) {
        for (qtens_len_t i = 0; i < std::get<0>(tar_extents); ++i) {
          const qtens_len_t it = std::get<0>(tar_strides) * i + std::get<1>(tar_strides) * j
                                 + std::get<2>(tar_strides) * k + std::get<3>(tar_strides) * l;
          const qtens_len_t is = std::get<I>(src_strides) * i + std::get<J>(src_strides) * j
                                 + std::get<K>(src_strides) * k + std::get<L>(src_strides) * l;
          target[it] = alpha * source[is] + beta * target[it];
        }
      }
    }
  }
}

template <int... ranks, typename T1, typename T2, typename U1, typename U2>
void reorder_indices_strided(std::array<qtens_len_t, sizeof...(ranks)> src_shape,
                             std::array<qtens_len_t, sizeof...(ranks)> src_strides, const T1 alpha,
                             const T2* source,
                             std::array<qtens_len_t, sizeof...(ranks)> tar_strides, const U1 beta,
                             U2* target)
{
  shape_range_<sizeof...(ranks)> src_range(src_shape);
  // i -> source index
  for (const auto& i : src_range) {
    const qtens_len_t ioff =
        std::inner_product(src_strides.begin(), src_strides.end(), i.begin(), 0);
    /// j -> target index
    std::array<qtens_len_t, sizeof...(ranks)> j = {std::get<ranks>(i)...};
    const qtens_len_t joff =
        std::inner_product(tar_strides.begin(), tar_strides.end(), j.begin(), 0);

    target[joff] = alpha * source[ioff] + beta * target[joff];
  }
}

template <int... ranks, typename T1, typename T2, typename U1, typename U2, std::size_t... iseq>
void reorder_unpack(std::array<qtens_len_t, sizeof...(ranks)> shape, const T1 alpha,
                    const T2* source, const U1 beta, U2* target, std::index_sequence<iseq...>)
{
  reorder_indices<ranks...>(std::get<iseq>(shape)..., alpha, source, beta, target);
}

/// general reorder ?
template <int... ranks, typename T1, typename T2, typename U1, typename U2>
void reorder_indices(std::array<qtens_len_t, sizeof...(ranks)> shape, const T1 alpha,
                     const T2* source, const U1 beta, U2* target)
{
  // static_assert(false, "you got here!");
  if constexpr (sizeof...(ranks) <= 4) {
    reorder_unpack<ranks...>(shape, alpha, source, beta, target,
                             std::make_index_sequence<sizeof...(ranks)>{});
  } else {
    // make a src_stride and tar_stride from the shape
    std::array<qtens_len_t, sizeof...(ranks)> tar_shape = {std::get<ranks>(shape)...};
    std::array<qtens_len_t, sizeof...(ranks)> src_stride;
    std::array<qtens_len_t, sizeof...(ranks)> tar_stride;
    src_stride[0] = 1;
    tar_stride[0] = 1;
    for (std::size_t i = 1; i < sizeof...(ranks); ++i) {
      src_stride[i] = src_stride[i - 1] * shape[i - 1];
      tar_stride[i] = tar_stride[i - 1] * tar_shape[i - 1];
    }
    reorder_indices_strided<ranks...>(shape, src_stride, alpha, source, tar_stride, beta, target);
  }
}

} // namespace qleve

#endif // QLEVE_REORDER_INDICES_HPP
