################################################################################
#
# \file      cmake/FindMKL.cmake
# \author    J. Bakosi (modified by S.M. Parker)
# \copyright 2012-2015, Jozsef Bakosi, 2016, Los Alamos National Security, LLC.
# \brief     Find the Math Kernel Library from Intel
# \date      Thu 26 Jan 2017 02:05:50 PM MST
#
################################################################################

# Find the Math Kernel Library from Intel
#
#  MKL_FOUND - System has MKL
#  MKL_INCLUDE_DIRS - MKL include files directories
#  MKL_LIBRARIES - The MKL libraries
#  MKL_INTERFACE_LIBRARY - MKL interface library
#  MKL_SEQUENTIAL_LAYER_LIBRARY - MKL sequential layer library
#  MKL_CORE_LIBRARY - MKL core library
#
#  The environment variables MKLROOT and INTEL are used to find the library.
#  Everything else is ignored. If MKL is found "-DMKL_ILP64" is added to
#  the compile definitions.
#
#  Exports the target intel::mkl which can be used as an interface
#
#  Example usage:
#
#  find_package(MKL REQUIRED)
#  if(target intel::mkl)
#    target_link_libraries(TARGET intel::mkl)
#  endif()

# If already in cache, be silent
if (MKL_INCLUDE_DIRS AND MKL_LIBRARIES AND MKL_INTERFACE_LIBRARY AND
    MKL_SEQUENTIAL_LAYER_LIBRARY AND MKL_CORE_LIBRARY)
  set (MKL_FIND_QUIETLY TRUE)
endif()

if(NOT BUILD_SHARED_LIBS)
  set(INT_LIB "libmkl_intel_ilp64.a")
  set(SEQ_LIB "libmkl_sequential.a")
  set(THR_LIB "libmkl_intel_thread.a")
  set(COR_LIB "libmkl_core.a")
else()
  set(INT_LIB "mkl_intel_ilp64")
  set(SEQ_LIB "mkl_sequential")
  set(THR_LIB "mkl_intel_thread")
  set(COR_LIB "mkl_core")
endif()

find_path(MKL_INCLUDE_DIR NAMES mkl.h HINTS $ENV{MKLROOT}/include)

find_library(MKL_INTERFACE_LIBRARY
             NAMES ${INT_LIB}
             PATHS $ENV{MKLROOT}/lib
                   $ENV{MKLROOT}/lib/intel64
                   $ENV{INTEL}/mkl/lib/intel64
             NO_DEFAULT_PATH)

find_library(MKL_SEQUENTIAL_LAYER_LIBRARY
             NAMES ${SEQ_LIB}
             PATHS $ENV{MKLROOT}/lib
                   $ENV{MKLROOT}/lib/intel64
                   $ENV{INTEL}/mkl/lib/intel64
             NO_DEFAULT_PATH)

find_library(MKL_CORE_LIBRARY
             NAMES ${COR_LIB}
             PATHS $ENV{MKLROOT}/lib
                   $ENV{MKLROOT}/lib/intel64
                   $ENV{INTEL}/mkl/lib/intel64
             NO_DEFAULT_PATH)

set(MKL_INCLUDE_DIRS ${MKL_INCLUDE_DIR})
set(MKL_LIBRARIES ${MKL_INTERFACE_LIBRARY} ${MKL_SEQUENTIAL_LAYER_LIBRARY} ${MKL_CORE_LIBRARY})

if (MKL_INCLUDE_DIR AND
    MKL_INTERFACE_LIBRARY AND
    MKL_SEQUENTIAL_LAYER_LIBRARY AND
    MKL_CORE_LIBRARY)

    add_library(intel::mkl::core STATIC IMPORTED)
    set_target_properties(intel::mkl::core PROPERTIES IMPORTED_LOCATION ${MKL_CORE_LIBRARY})

    add_library(intel::mkl::sequential_layer STATIC IMPORTED)
    set_target_properties(intel::mkl::sequential_layer PROPERTIES IMPORTED_LOCATION ${MKL_SEQUENTIAL_LAYER_LIBRARY})

    add_library(intel::mkl::interface STATIC IMPORTED)
    set_target_properties(intel::mkl::interface PROPERTIES IMPORTED_LOCATION ${MKL_INTERFACE_LIBRARY})

    add_library(mkl INTERFACE)
    if("${CMAKE_SYSTEM_NAME}" STREQUAL "Linux")
      target_link_libraries(mkl INTERFACE "-Wl,--start-group" intel::mkl::core intel::mkl::sequential_layer intel::mkl::interface "-Wl,--end-group -lpthread -lm -ldl")
    else()
      target_link_libraries(mkl INTERFACE intel::mkl::core intel::mkl::sequential_layer intel::mkl::interface)
    endif()
    set_target_properties(mkl PROPERTIES INTERFACE_COMPILE_DEFINITIONS MKL_ILP64)
    target_include_directories(mkl INTERFACE ${MKL_INCLUDE_DIRS})
    add_library(intel::mkl ALIAS mkl)
else()
  set(MKL_INCLUDE_DIRS "")
  set(MKL_LIBRARIES "")
  set(MKL_INTERFACE_LIBRARY "")
  set(MKL_SEQUENTIAL_LAYER_LIBRARY "")
  set(MKL_CORE_LIBRARY "")
endif()

# Handle the QUIETLY and REQUIRED arguments and set MKL_FOUND to TRUE if
include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(MKL DEFAULT_MSG MKL_LIBRARIES MKL_INCLUDE_DIRS MKL_INTERFACE_LIBRARY MKL_SEQUENTIAL_LAYER_LIBRARY MKL_CORE_LIBRARY)

mark_as_advanced(MKL_INCLUDE_DIRS MKL_LIBRARIES MKL_INTERFACE_LIBRARY MKL_SEQUENTIAL_LAYER_LIBRARY MKL_CORE_LIBRARY)
