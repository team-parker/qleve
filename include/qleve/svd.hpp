// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file include/qleve/svp.hpp
///
/// Generic interface to singular value decomposition for MatrixLike objects

#ifndef QLEVE_SVD_HPP
#define QLEVE_SVD_HPP

#include <tuple>

#include <qleve/blas_interface.hpp>
#include <qleve/pod_types.hpp>
#include <qleve/tensor.hpp>
#include <qleve/tensor_traits.hpp>

namespace qleve::linalg
{

std::tuple<Tensor<1>, Tensor<2>, Tensor<2>> svd(real_matrixable auto&& A)
{
  const auto [ndim, mdim] = A.shape();

  const ptrdiff_t min_nm = std::min(A.extent(0), A.extent(1));
  Tensor<1> s(min_nm);

  Tensor<2> u(ndim, ndim);
  Tensor<2> vt(mdim, mdim);

  ptrdiff_t lwork;
  {
    double lwork_double;
    // const_cast is ugly, but it's the fault of blas interface
    int info = dgesvd_("A", "A", ndim, mdim, A.data(), A.stride(1), s.data(), u.data(), ndim,
                       vt.data(), mdim, &lwork_double, -1);
    assert(info == 0);
    lwork = static_cast<ptrdiff_t>(lwork_double);
  }

  Tensor<1> work(lwork);

  int info = dgesvd_("A", "A", ndim, mdim, A.data(), A.stride(1), s.data(), u.data(), ndim,
                     vt.data(), mdim, work.data(), lwork);

  return {s, u, vt};
}

/// SVD algorithm using jacobi plane rotations (dgesvj from LAPACK).
///
/// Requires matrices to be tall/square (n >= m).
std::tuple<Tensor<1>, Tensor<2>, Tensor<2>> svd_jacobi(real_matrixable auto&& A)
{
  const auto [ndim, mdim] = A.shape();

  const ptrdiff_t min_nm = std::min(A.extent(0), A.extent(1));
  Tensor<1> s(min_nm);

  Tensor<2> u(ndim, ndim);
  Tensor<2> vt(mdim, mdim);

  char joba = 'G'; // general n x m matrix
  char jobu = 'U'; // compute U with default orthogonality threshold
  char jobv = 'V'; // compute V with default orthogonality threshold

  std::array<double, 6> stat;

  auto info = LAPACKE_dgesvj(LAPACK_COL_MAJOR, joba, jobu, jobv, ndim, mdim, A.data(), A.stride(1),
                             s.data(), mdim, vt.data(), vt.stride(1), stat.data());

  if (info != 0) {
    throw std::runtime_error("LAPACKE_dgesvj failed with error code " + std::to_string(info));
  }

  std::copy_n(A.data(), ndim * ndim, u.data());

  std::cout << "fancy SVD done\n";
  const auto scal = stat[0];
  std::cout << "scal = " << scal << '\n';
  std::cout << stat[1] << " singular values:\n";
  std::cout << "in " << stat[3] << " iterations\n";
  if (scal != 1.0) {
    s *= scal;
  }
  std::cout << "singular values:\n";
  for (const auto& val : s) {
    std::cout << val << '\n';
  }

  return {s, u, vt};
}

/// SVD algorithm using preconditioned jacobi (dgejsv from LAPACK),
/// which should give more accurate singular values/vectors.
std::tuple<Tensor<1>, Tensor<2>, Tensor<2>> svd_preconditioned_jacobi(real_matrixable auto&& A)
{
  const auto [ndim, mdim] = A.shape();

  const ptrdiff_t min_nm = std::min(A.extent(0), A.extent(1));
  Tensor<1> s(min_nm);

  Tensor<2> u(ndim, ndim);
  Tensor<2> vt(mdim, mdim);

  char joba = 'F'; // general n x m matrix
  char jobu = 'F'; // compute U with default orthogonality threshold
  char jobv = 'J'; // compute V with default orthogonality threshold
  char jobr = 'N'; // do not remove small columns
  char jobt = 'N'; // do not remove small rows
  char jobp = 'N'; // do not compute condition number

  std::array<double, 6> stat;
  std::array<lapack_int, 3> istat;

  auto info = LAPACKE_dgejsv(LAPACK_COL_MAJOR, joba, jobu, jobv, jobr, jobt, jobp, ndim, mdim,
                             A.data(), A.stride(1), s.data(), u.data(), u.stride(1), vt.data(),
                             vt.stride(1), stat.data(), istat.data());

  if (info != 0) {
    throw std::runtime_error("LAPACKE_dgesvj failed with error code " + std::to_string(info));
  }

  // std::cout << "fancy preconditioned SVD done\n";
  const auto scal = stat[1] / stat[0];
  // std::cout << "scal = " << scal << '\n';
  // std::cout << istat[1] << " singular values:\n";
  if (scal != 1.0) {
    s *= scal;
  }
  // std::cout << "singular values:\n";
  // for (const auto& val : s) {
  //   std::cout << val << '\n';
  // }

  vt = vt.transpose();
  return {s, u, vt};
}

} // namespace qleve::linalg

#endif // QLEVE_SVD_HPP
