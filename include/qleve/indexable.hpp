// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file include/qleve/indexable.hpp
///
/// Indexable is the essential class implementing Expression Template
/// machinery, but it otherwise contains no class members.

#ifndef QLEVE_INDEXABLE_HPP
#define QLEVE_INDEXABLE_HPP

#include <array>
#include <cassert>
#include <iostream>
#include <memory>
#include <utility>

#include <qleve/detail.hpp>
#include <qleve/indexable_traits.hpp>
#include <qleve/layout.hpp>
#include <qleve/pod_types.hpp>
#include <qleve/shape_iterator.hpp>
#include <qleve/vector_ops.hpp>

namespace qleve
{

template <typename T, class Derived, class Layout>
class Indexable;

/// Blank dummy container for Indexables
template <class Container>
struct ConstRef {
  const Container& c_;

  ConstRef(const Container& c) : c_(c) {}
  const auto& data(const qtens_len_t& i) const { return c_.data(i); }
  const auto& data(const qtens_len_t& i, const qtens_len_t& j) const { return c_.data(i, j); }
  template <shapeable Shape>
  const auto& data(const Shape& shape) const
  {
    return c_.data(shape);
  }

  template <shapeable Shape>
  bool match_shapes(const Shape& shape) const
  {
    return c_.match_shapes(shape);
  }

  const auto& ishape() const { return c_.ishape(); }
};
template <class T, class A, class L>
using CRI = ConstRef<Indexable<T, A, L>>;

/// Generalized Expression
template <typename T, class Layout, class Expression>
struct Xpr {
  Expression e_;

  Xpr(const Expression& e) : e_(e) {}
  const T data(const qtens_len_t& i) const { return e_.data(i); }
  const T data(const qtens_len_t& i, const qtens_len_t& j) const { return e_.data(i, j); }
  template <shapeable Shape>
  const T data(const Shape& i) const
  {
    return e_.data(i);
  }

  template <shapeable Shape>
  bool match_shapes(const Shape& shape) const
  {
    return e_.match_shapes(shape);
  }

  const auto& ishape() const { return e_.ishape(); }
};

/// Expression Template for func(Expression, Expression)
template <class A, class Op>
struct XprUnaryOp {
  using datatype = typename Op::datatype;

  A a_;

  XprUnaryOp(const A& a) : a_(a) {}
  datatype data(const qtens_len_t& i) const { return Op::apply(a_.data(i)); }
  datatype data(const qtens_len_t& i, const qtens_len_t& j) const
  {
    return Op::apply(a_.data(i, j));
  }
  template <shapeable Shape>
  datatype data(const Shape& i) const
  {
    return Op::apply(a_.data(i));
  }

  template <shapeable Shape>
  bool match_shapes(const Shape& shape) const
  {
    return a_.match_shapes(shape);
  }

  const auto& ishape() const { return a_.ishape(); }
};

/// Expression Template for func(Expression, Expression)
template <class A, class B, class Op>
struct XprBinaryOp {
  using datatype = typename Op::datatype;

  A a_;
  B b_;

  XprBinaryOp(const A& a, const B& b) : a_(a), b_(b) {}
  datatype data(const qtens_len_t& i) const { return Op::apply(a_.data(i), b_.data(i)); }
  datatype data(const qtens_len_t& i, const qtens_len_t& j) const
  {
    return Op::apply(a_.data(i, j), b_.data(i, j));
  }
  template <shapeable Shape>
  datatype data(const Shape& i) const
  {
    return Op::apply(a_.data(i), b_.data(i));
  }

  template <shapeable Shape>
  bool match_shapes(const Shape& shape) const
  {
    return a_.match_shapes(shape) && b_.match_shapes(shape);
  }

  const auto& ishape() const
  {
    assert(a_.ishape() == b_.ishape());
    return a_.ishape();
  }
};

/// Expression Template for Xpr . Scalar
template <class A, class scalar_type, class Op>
struct XprScalarOp {
  using datatype = typename Op::datatype;

  A a_;
  scalar_type scalar_;

  XprScalarOp(const A& a, const scalar_type& scalar) : a_(a), scalar_(scalar) {}
  datatype data(const qtens_len_t& i) const { return Op::apply(a_.data(i), scalar_); }
  datatype data(const qtens_len_t& i, const qtens_len_t& j) const
  {
    return Op::apply(a_.data(i, j), scalar_);
  }
  template <shapeable Shape>
  datatype data(const Shape& i) const
  {
    return Op::apply(a_.data(i), scalar_);
  }

  template <shapeable Shape>
  bool match_shapes(const Shape& shape) const
  {
    return a_.match_shapes(shape);
  }

  const auto& ishape() const { return a_.ishape(); }
};

/// Expression Template for Scalar . Xpr
template <typename scalar_type, class B, class Op>
struct ScalarXprOp {
  using datatype = typename Op::datatype;

  B b_;
  scalar_type scalar_;

  ScalarXprOp(const scalar_type& scalar, const B& b) : b_(b), scalar_(scalar) {}
  datatype data(const qtens_len_t& i) const { return Op::apply(scalar_, b_.data(i)); }
  datatype data(const qtens_len_t& i, const qtens_len_t& j) const
  {
    return Op::apply(scalar_, b_.data(i, j));
  }
  template <shapeable Shape>
  datatype data(const Shape& i) const
  {
    return Op::apply(scalar_, b_.data(i));
  }

  template <shapeable Shape>
  bool match_shapes(const Shape& shape) const
  {
    return b_.match_shapes(shape);
  }

  const auto& ishape() const { return b_.ishape(); }
};

/**********************************************************************************
  Unary operators
**********************************************************************************/

#define XM_REGISTER_UNARY_FROM_SCALAR(rf, func, op)                               \
  template <typename U>                                                           \
  struct op {                                                                     \
    using datatype = decltype(rf(std::declval<U>()));                             \
    inline static datatype apply(const U& a) { return rf(a); }                    \
  };                                                                              \
                                                                                  \
  /* func(Indexable) */                                                           \
  template <class A, coeffable U, class L, class T = typename op<U>::datatype>    \
  Xpr<T, L, XprUnaryOp<CRI<U, A, L>, op<U>>> func(const Indexable<U, A, L>& a)    \
  {                                                                               \
    typedef XprUnaryOp<CRI<U, A, L>, op<U>> ExprT;                                \
    return Xpr<T, L, ExprT>(ExprT(CRI<U, A, L>(a)));                              \
  }                                                                               \
                                                                                  \
  /* func(Xpr) */                                                                 \
  template <class A, coeffable U, class L, typename T = typename op<U>::datatype> \
  Xpr<T, L, XprUnaryOp<Xpr<U, L, A>, op<U>>> func(const Xpr<U, L, A>& a)          \
  {                                                                               \
    typedef XprUnaryOp<Xpr<U, L, A>, op<U>> ExprT;                                \
    return Xpr<T, L, ExprT>(ExprT(a));                                            \
  }

XM_REGISTER_UNARY_FROM_SCALAR(detail::conj, conj, OpConj)
XM_REGISTER_UNARY_FROM_SCALAR(detail::real, real, OpReal)
XM_REGISTER_UNARY_FROM_SCALAR(detail::imag, imag, OpImag)

XM_REGISTER_UNARY_FROM_SCALAR(std::abs, abs, OpAbs)
XM_REGISTER_UNARY_FROM_SCALAR(std::sqrt, sqrt, OpSqrt)
XM_REGISTER_UNARY_FROM_SCALAR(std::cbrt, cbrt, OpCbrt)
XM_REGISTER_UNARY_FROM_SCALAR(std::exp, exp, OpExp)
XM_REGISTER_UNARY_FROM_SCALAR(std::log, log, OpLog)
XM_REGISTER_UNARY_FROM_SCALAR(std::log2, log2, OpLog2)
XM_REGISTER_UNARY_FROM_SCALAR(std::cos, cos, OpCos)
XM_REGISTER_UNARY_FROM_SCALAR(std::sin, sin, OpSin)
XM_REGISTER_UNARY_FROM_SCALAR(std::tan, tan, OpTan)
XM_REGISTER_UNARY_FROM_SCALAR(std::acos, acos, OpAcos)
XM_REGISTER_UNARY_FROM_SCALAR(std::asin, asin, OpAsin)
XM_REGISTER_UNARY_FROM_SCALAR(std::atan, atan, OpAtan)
XM_REGISTER_UNARY_FROM_SCALAR(std::cosh, cosh, OpCosh)
XM_REGISTER_UNARY_FROM_SCALAR(std::sinh, sinh, OpSinh)
XM_REGISTER_UNARY_FROM_SCALAR(std::tanh, tanh, OpTanh)
XM_REGISTER_UNARY_FROM_SCALAR(std::acosh, acosh, OpAcosh)
XM_REGISTER_UNARY_FROM_SCALAR(std::asinh, asinh, OpAsinh)
XM_REGISTER_UNARY_FROM_SCALAR(std::atanh, atanh, OpAtanh)
XM_REGISTER_UNARY_FROM_SCALAR(std::erf, erf, OpErf)
XM_REGISTER_UNARY_FROM_SCALAR(std::erfc, erfc, OpErfc)
XM_REGISTER_UNARY_FROM_SCALAR(std::ceil, ceil, OpCeil)
XM_REGISTER_UNARY_FROM_SCALAR(std::floor, floor, OpFloor)

#undef XM_REGISTER_UNARY_FROM_SCALAR

/**********************************************************************************
  Binary operators
**********************************************************************************/

// Some dumb helper functions
template <typename U1, typename U2>
inline auto add(const U1 a, const U2 b)
{
  return a + b;
}
template <typename U1, typename U2>
inline auto sub(const U1 a, const U2 b)
{
  return a - b;
}
template <typename U1, typename U2>
inline auto mul(const U1 a, const U2 b)
{
  return a * b;
}
template <typename U1, typename U2>
inline auto div(const U1 a, const U2 b)
{
  return a / b;
}

#define XM_REGISTER_BINARY_FROM_SCALAR(rf, func, op)                                               \
  template <typename U1, typename U2>                                                              \
  struct op {                                                                                      \
    using datatype = decltype(rf(std::declval<U1>(), std::declval<U2>()));                         \
    inline static datatype apply(const U1& a, const U2& b) { return rf(a, b); }                    \
  };                                                                                               \
                                                                                                   \
  /* func(Indexable, Indexable) */                                                                 \
  template <class A, coeffable Ua, class La, class B, coeffable Ub, class Lb,                      \
            class T = typename op<Ua, Ub>::datatype,                                               \
            class L = typename compose_layout<La, Lb>::type>                                       \
  Xpr<T, L, XprBinaryOp<CRI<Ua, A, La>, CRI<Ub, B, Lb>, op<Ua, Ub>>> func(                         \
      const Indexable<Ua, A, La>& a, const Indexable<Ub, B, Lb>& b)                                \
  {                                                                                                \
    typedef XprBinaryOp<CRI<Ua, A, La>, CRI<Ub, B, Lb>, op<Ua, Ub>> ExprT;                         \
    return Xpr<T, L, ExprT>(ExprT(CRI<Ua, A, La>(a), CRI<Ub, B, Lb>(b)));                          \
  }                                                                                                \
                                                                                                   \
  /* func(Indexable, scalar) */                                                                    \
  template <class A, coeffable Ua, class La, coeffable Ub,                                         \
            typename T = typename op<Ua, Ub>::datatype>                                            \
  Xpr<T, La, XprScalarOp<CRI<Ua, A, La>, Ub, op<Ua, Ub>>> func(const Indexable<Ua, A, La>& a,      \
                                                               const Ub& b)                        \
  {                                                                                                \
    typedef XprScalarOp<CRI<Ua, A, La>, Ub, op<Ua, Ub>> ExprT;                                     \
    return Xpr<T, La, ExprT>(ExprT(CRI<Ua, A, La>(a), b));                                         \
  }                                                                                                \
  /* func(scalar, Indexable) */                                                                    \
  template <coeffable Ua, class B, coeffable Ub, class Lb,                                         \
            typename T = typename op<Ua, Ub>::datatype>                                            \
  Xpr<T, Lb, ScalarXprOp<Ua, CRI<Ub, B, Lb>, op<Ua, Ub>>> func(const Ua& a,                        \
                                                               const Indexable<Ub, B, Lb>& b)      \
  {                                                                                                \
    typedef ScalarXprOp<Ua, CRI<Ub, B, Lb>, op<Ua, Ub>> ExprT;                                     \
    return Xpr<T, Lb, ExprT>(ExprT(a, CRI<Ub, B, Lb>(b)));                                         \
  }                                                                                                \
                                                                                                   \
  /* func(Indexable, Xpr) */                                                                       \
  template <class A, coeffable Ua, class La, class B, coeffable Ub, class Lb,                      \
            typename T = typename op<Ua, Ub>::datatype,                                            \
            typename L = typename compose_layout<La, Lb>::type>                                    \
  Xpr<T, L, XprBinaryOp<CRI<Ua, A, La>, Xpr<Ub, Lb, B>, op<Ua, Ub>>> func(                         \
      const Indexable<Ua, A, La>& a, const Xpr<Ub, Lb, B>& b)                                      \
  {                                                                                                \
    typedef XprBinaryOp<CRI<Ua, A, La>, Xpr<Ub, Lb, B>, op<Ua, Ub>> ExprT;                         \
    return Xpr<T, L, ExprT>(ExprT(CRI<Ua, A, La>(a), b));                                          \
  }                                                                                                \
                                                                                                   \
  /* func(Xpr, Indexable) */                                                                       \
  template <class A, typename Ua, class La, class B, typename Ub, class Lb,                        \
            typename L = typename compose_layout<La, Lb>::type,                                    \
            typename T = typename op<Ua, Ub>::datatype>                                            \
  Xpr<T, L, XprBinaryOp<Xpr<Ua, La, A>, CRI<Ub, B, Lb>, op<Ua, Ub>>> func(                         \
      const Xpr<Ua, La, A>& a, const Indexable<Ub, B, Lb>& b)                                      \
  {                                                                                                \
    typedef XprBinaryOp<Xpr<Ua, La, A>, CRI<Ub, B, Lb>, op<Ua, Ub>> ExprT;                         \
    return Xpr<T, L, ExprT>(ExprT(a, CRI<Ub, B, Lb>(b)));                                          \
  }                                                                                                \
                                                                                                   \
  /* func(Xpr, scalar) */                                                                          \
  template <class A, coeffable Ua, class La, coeffable Ub,                                         \
            typename T = typename op<Ua, Ub>::datatype>                                            \
  Xpr<T, La, XprScalarOp<Xpr<Ua, La, A>, Ub, op<Ua, Ub>>> func(const Xpr<Ua, La, A>& a,            \
                                                               const Ub& b)                        \
  {                                                                                                \
    typedef XprScalarOp<Xpr<Ua, La, A>, Ub, op<Ua, Ub>> ExprT;                                     \
    return Xpr<T, La, ExprT>(ExprT(a, b));                                                         \
  }                                                                                                \
                                                                                                   \
  /* func(scalar, Xpr) */                                                                          \
  template <coeffable Ua, class B, coeffable Ub, class Lb,                                         \
            typename T = typename op<Ua, Ub>::datatype>                                            \
  Xpr<T, Lb, ScalarXprOp<Ua, Xpr<Ub, Lb, B>, op<Ua, Ub>>> func(const Ua& a,                        \
                                                               const Xpr<Ub, Lb, B>& b)            \
  {                                                                                                \
    typedef ScalarXprOp<Ua, Xpr<Ub, Lb, B>, op<Ua, Ub>> ExprT;                                     \
    return Xpr<T, Lb, ExprT>(ExprT(a, b));                                                         \
  }                                                                                                \
                                                                                                   \
  /* func(Xpr, Xpr) */                                                                             \
  template <class A, coeffable Ua, class La, class B, coeffable Ub, class Lb,                      \
            typename L = typename compose_layout<La, Lb>::type,                                    \
            typename T = typename op<Ua, Ub>::datatype>                                            \
  Xpr<T, L, XprBinaryOp<Xpr<Ua, La, A>, Xpr<Ub, Lb, B>, op<Ua, Ub>>> func(const Xpr<Ua, La, A>& a, \
                                                                          const Xpr<Ub, Lb, B>& b) \
  {                                                                                                \
    typedef XprBinaryOp<Xpr<Ua, La, A>, Xpr<Ub, Lb, B>, op<Ua, Ub>> ExprT;                         \
    return Xpr<T, L, ExprT>(ExprT(a, b));                                                          \
  }

XM_REGISTER_BINARY_FROM_SCALAR(qleve::add, operator+, OpAdd)
XM_REGISTER_BINARY_FROM_SCALAR(qleve::sub, operator-, OpSub)
XM_REGISTER_BINARY_FROM_SCALAR(qleve::mul, operator*, OpMul)
XM_REGISTER_BINARY_FROM_SCALAR(qleve::div, operator/, OpDiv)

XM_REGISTER_BINARY_FROM_SCALAR(std::pow, pow, OpPow)
XM_REGISTER_BINARY_FROM_SCALAR(std::atan2, atan2, OpAtan2)
XM_REGISTER_BINARY_FROM_SCALAR(std::copysign, copysign, OpCopysign)
XM_REGISTER_BINARY_FROM_SCALAR(std::hypot, hypot, OpHypot)
XM_REGISTER_BINARY_FROM_SCALAR(std::min, min, OpMin)
XM_REGISTER_BINARY_FROM_SCALAR(std::max, max, OpMax)

#undef XM_REGISTER_BINARY_FROM_SCALAR

/**
 * Indexable
 */
/// TODO only require ncolumns when mixing with Columnwise
template <typename T, class Derived, class Layout>
class Indexable {
 private:
  void check_indexable()
  {
    static_assert(has_size<Derived>::value, "Indexable must implement size()");
    static_assert(has_shape<Derived>::value, "Indexable must implement shape()");
    static_assert(has_nrows<Derived>::value, "Indexable must implement nrows()");
    static_assert(has_ncolumns<Derived>::value, "Indexable must implement ncolumns()");
    static_assert(has_nonconst_data_operator<Derived, T>::value, "Indexable must implement data()");
    static_assert(has_const_data_operator<Derived, T>::value,
                  "Indexable must implement data() const");
    static_assert(has_nonconst_access_operator<Derived, T>::value,
                  "Indexable must implement data(i)");
    static_assert(has_const_access_operator<Derived, T>::value,
                  "Indexable must implement data(i) const");
    static_assert(has_nonconst_rect_access_operator<Derived, T>::value,
                  "Indexable must implement data(i,j)");
    static_assert(has_const_rect_access_operator<Derived, T>::value,
                  "Indexable must implement data(i,j) const");
  }

  void check_const_indexable() const
  {
    static_assert(has_size<Derived>::value, "Indexable must implement size()");
    static_assert(has_shape<Derived>::value, "Indexable must implement shape()");
    static_assert(has_nrows<Derived>::value, "Indexable must implement nrows()");
    static_assert(has_ncolumns<Derived>::value, "Indexable must implement ncolumns()");
    static_assert(has_const_data_operator<Derived, T>::value,
                  "Indexable must implement data() const");
    static_assert(has_const_access_operator<Derived, T>::value,
                  "Indexable must implement data(i) const");
    static_assert(has_const_rect_access_operator<Derived, T>::value,
                  "Indexable must implement data(i,j) const");
  }

 public:
  typedef T datatype;

  T* data()
  {
    check_indexable();
    return static_cast<Derived*>(this)->data();
  }

  const T* data() const
  {
    check_const_indexable();
    return static_cast<const Derived*>(this)->data();
  }

  T& data(const qtens_len_t& i)
  {
    check_indexable();
    return static_cast<Derived*>(this)->data(i);
  }

  const T& data(const qtens_len_t& i) const
  {
    check_const_indexable();
    return static_cast<const Derived*>(this)->data(i);
  }

  T& data(const qtens_len_t& i, const qtens_len_t& j)
  {
    check_indexable();
    return static_cast<Derived*>(this)->data(i, j);
  }

  const T& data(const qtens_len_t& i, const qtens_len_t& j) const
  {
    check_const_indexable();
    return static_cast<const Derived*>(this)->data(i, j);
  }

  template <shapeable Shape>
  T& data(const Shape& i)
  {
    return static_cast<Derived*>(this)->operator()(i);
  }
  template <shapeable Shape>
  const T& data(const Shape& i) const
  {
    return static_cast<const Derived*>(this)->operator()(i);
  }

  const auto& ishape() const
  {
    check_const_indexable();
    return static_cast<const Derived*>(this)->shape();
  }

  auto isize() const
  {
    check_const_indexable();
    return static_cast<const Derived*>(this)->size();
  }

  auto irows() const
  {
    check_const_indexable();
    return static_cast<const Derived*>(this)->nrows();
  }

  auto icols() const
  {
    check_const_indexable();
    return static_cast<const Derived*>(this)->ncolumns();
  }

  template <shapeable Shape>
  bool match_shapes(const Shape& shape) const
  {
    check_const_indexable();
    return shape == ishape();
  }

  template <typename U, class L, class E>
  bool have_matching_shapes(const Xpr<U, L, E>& x) const
  {
    check_const_indexable();
    return x.match_shapes(ishape());
  }

  template <typename U, class A, class L>
  bool have_matching_shapes(const Indexable<U, A, L>& x) const
  {
    check_const_indexable();
    return x.match_shapes(ishape());
  }

  template <typename U, class L, class E>
    requires std::is_convertible_v<U, datatype>
  void assign_from(const Xpr<U, L, E>& x)
  {
    check_indexable();
    assert(have_matching_shapes(x));
    using composed_layout = compose_layout_t<Layout, L>;
    if constexpr (std::is_same_v<composed_layout, ContiguousLayout>) {
      const qtens_len_t size = isize();
      for (qtens_len_t i = 0; i < size; ++i)
        data(i) = x.data(i);
    } else if constexpr (std::is_same_v<composed_layout, ColumnwiseLayout>) {
      const qtens_len_t ncols = this->icols();
      const qtens_len_t nrows = this->irows();
      for (qtens_len_t j = 0; j < ncols; ++j) {
        for (qtens_len_t i = 0; i < nrows; ++i) {
          data(i, j) = x.data(i, j);
        }
      }
    } else if constexpr (std::is_same_v<composed_layout, StridedLayout>) {
      constexpr int rank = std::tuple_size<std::remove_cvref_t<decltype(ishape())>>{};
      for (const auto sr = shape_range_<rank>(ishape()); const auto& i : sr) {
        data(i) = x.data(i);
      }
    }
  }

  template <typename U, class E, class L>
    requires std::is_convertible_v<U, T>
  void assign_from(const Indexable<U, E, L>& x)
  {
    check_indexable();
    assert(have_matching_shapes(x));

    using composed_layout = compose_layout_t<Layout, L>;
    if constexpr (std::is_same_v<composed_layout, ContiguousLayout>) {
      std::copy_n(x.data(), isize(), data());
    } else if constexpr (std::is_same_v<composed_layout, ColumnwiseLayout>) {
      const qtens_len_t ncols = this->icols();
      const qtens_len_t nrows = this->irows();
      for (qtens_len_t j = 0; j < ncols; ++j) {
        std::copy_n(&x.data(0, j), nrows, &data(0, j));
      }
    } else if constexpr (std::is_same_v<composed_layout, StridedLayout>) {
      constexpr int rank = std::tuple_size<std::remove_cvref_t<decltype(ishape())>>{};
      for (const auto sr = shape_range_<rank>(ishape()); const auto& i : sr) {
        data(i) = x.data(i);
      }
    }
  }

  template <typename U>
    requires std::is_convertible_v<U, T>
  void assign_from(const U& x)
  {
    check_indexable();

    if constexpr (std::is_same_v<Layout, ContiguousLayout>) {
      std::fill_n(data(), isize(), x);
    } else if constexpr (std::is_same_v<Layout, ColumnwiseLayout>) {
      const qtens_len_t ncols = this->icols();
      const qtens_len_t nrows = this->irows();
      for (qtens_len_t j = 0; j < ncols; ++j) {
        std::fill_n(&data(0, j), nrows, x);
      }
    } else if constexpr (std::is_same_v<Layout, StridedLayout>) {
      constexpr int rank = std::tuple_size<std::remove_cvref_t<decltype(ishape())>>{};
      for (const auto sr = shape_range_<rank>(ishape()); const auto& i : sr) {
        data(i) = x;
      }
    }
  }

  /// Specialize for *this = Indexable * scalar -> ax_plus_y()
  template <class A, typename U>
    requires std::is_convertible_v<U, T>
  void assign_from(
      const Xpr<T, ContiguousLayout, XprScalarOp<CRI<T, A, ContiguousLayout>, U, OpMul<T, U>>>& xpr)
  {
    check_indexable();
    assert(have_matching_shapes(xpr));

    this->assign_from(0.0);
    if constexpr (std::is_same_v<Layout, ContiguousLayout>) {
      qleve::ax_plus_y(isize(), xpr.e_.scalar_, xpr.e_.a_.c_.data(), data());
    } else if constexpr (std::is_same_v<Layout, ColumnwiseLayout>) {
      const qtens_len_t ncols = this->icols();
      const qtens_len_t nrows = this->irows();
      for (qtens_len_t j = 0; j < ncols; ++j) {
        qleve::ax_plus_y(nrows, xpr.e_.scalar_, &xpr.e_.a_.c_.data(0, j), &data(0, j));
      }
    } else if constexpr (std::is_same_v<Layout, StridedLayout>) {
      constexpr int rank = std::tuple_size<std::remove_cvref_t<decltype(ishape())>>{};
      for (const auto sr = shape_range_<rank>(ishape()); const auto& i : sr) {
        data(i) += xpr.e_.scalar_ * xpr.e_.a_.c_.data(i);
      }
    }
  }

  /// Specialize for *this = scalar * Indexable -> ax_plus_y()
  template <typename U, class B>
    requires std::is_convertible_v<U, T>
  void assign_from(
      const Xpr<T, ContiguousLayout, ScalarXprOp<U, CRI<T, B, ContiguousLayout>, OpMul<U, T>>>& xpr)
  {
    check_indexable();
    assert(have_matching_shapes(xpr));

    this->assign_from(0.0);
    if constexpr (std::is_same_v<Layout, ContiguousLayout>) {
      qleve::ax_plus_y(isize(), xpr.e_.scalar_, xpr.e_.b_.c_.data(), data());
    } else if constexpr (std::is_same_v<Layout, ColumnwiseLayout>) {
      const qtens_len_t ncols = this->icols();
      const qtens_len_t nrows = this->irows();
      for (qtens_len_t j = 0; j < ncols; ++j) {
        qleve::ax_plus_y(nrows, xpr.e_.scalar_, &xpr.e_.b_.c_.data(0, j), &data(0, j));
      }
    } else if constexpr (std::is_same_v<Layout, StridedLayout>) {
      constexpr int rank = std::tuple_size<std::remove_cvref_t<decltype(ishape())>>{};
      for (const auto sr = shape_range_<rank>(ishape()); const auto& i : sr) {
        data(i) += xpr.e_.scalar_ * xpr.e_.b_.c_.data(i);
      }
    }
  }

  /// Specialize for *this = Indexable / scalar -> ax_plus_y()
  template <class A, typename U>
    requires std::is_convertible_v<U, T>
  void assign_from(
      const Xpr<T, ContiguousLayout, XprScalarOp<CRI<T, A, ContiguousLayout>, U, OpDiv<T, U>>>& xpr)
  {
    check_indexable();
    assert(have_matching_shapes(xpr));

    this->assign_from(0.0);
    if constexpr (std::is_same_v<Layout, ContiguousLayout>) {
      qleve::ax_plus_y(isize(), 1.0 / xpr.e_.scalar_, xpr.e_.a_.c_.data(), data());
    } else if constexpr (std::is_same_v<Layout, ColumnwiseLayout>) {
      const qtens_len_t ncols = this->icols();
      const qtens_len_t nrows = this->irows();
      for (qtens_len_t j = 0; j < ncols; ++j) {
        qleve::ax_plus_y(nrows, 1.0 / xpr.e_.scalar_, &xpr.e_.a_.c_.data(0, j), &data(0, j));
      }
    } else if constexpr (std::is_same_v<Layout, StridedLayout>) {
      constexpr int rank = std::tuple_size<std::remove_cvref_t<decltype(ishape())>>{};
      for (const auto sr = shape_range_<rank>(ishape()); const auto& i : sr) {
        data(i) += xpr.e_.a_.c_.data(i) / xpr.e_.scalar_;
      }
    }
  }

  /* Indexable += scalar */
  template <class U>
    requires std::is_convertible_v<U, T>
  Derived& operator+=(const U& x)
  {
    check_indexable();

    if constexpr (std::is_same_v<Layout, ContiguousLayout>) {
      const auto size = isize();
      for (qtens_len_t i = 0; i < size; ++i)
        data(i) += x;
    } else if constexpr (std::is_same_v<Layout, ColumnwiseLayout>) {
      const qtens_len_t ncols = this->icols();
      const qtens_len_t nrows = this->irows();
      for (qtens_len_t j = 0; j < ncols; ++j) {
        for (qtens_len_t i = 0; i < nrows; ++i) {
          data(i, j) += x;
        }
      }
    } else if constexpr (std::is_same_v<Layout, StridedLayout>) {
      constexpr int rank = std::tuple_size<std::remove_cvref_t<decltype(ishape())>>{};
      for (const auto sr = shape_range_<rank>(ishape()); const auto& i : sr) {
        data(i) += x;
      }
    }

    return *static_cast<Derived*>(this);
  }

  /* Indexable -= scalar */
  template <class U>
    requires std::is_convertible_v<U, T>
  Derived& operator-=(const U& x)
  {
    check_indexable();

    if constexpr (std::is_same_v<Layout, ContiguousLayout>) {
      const auto size = isize();
      for (qtens_len_t i = 0; i < size; ++i)
        data(i) -= x;
    } else if constexpr (std::is_same_v<Layout, ColumnwiseLayout>) {
      const qtens_len_t ncols = this->icols();
      const qtens_len_t nrows = this->irows();
      for (qtens_len_t j = 0; j < ncols; ++j) {
        for (qtens_len_t i = 0; i < nrows; ++i) {
          data(i, j) -= x;
        }
      }
    } else if constexpr (std::is_same_v<Layout, StridedLayout>) {
      constexpr int rank = std::tuple_size<std::remove_cvref_t<decltype(ishape())>>{};
      for (const auto sr = shape_range_<rank>(ishape()); const auto& i : sr) {
        data(i) -= x;
      }
    }

    return *static_cast<Derived*>(this);
  }

  /* Indexable *= scalar */
  template <class U>
    requires std::is_convertible_v<U, T>
  Derived& operator*=(const U& x)
  {
    check_indexable();
    if constexpr (std::is_same_v<Layout, ContiguousLayout>) {
      qleve::scale(isize(), x, data());
    } else if constexpr (std::is_same_v<Layout, ColumnwiseLayout>) {
      const qtens_len_t ncols = this->icols();
      const qtens_len_t nrows = this->irows();
      for (qtens_len_t j = 0; j < ncols; ++j) {
        qleve::scale(nrows, x, &data(0, j));
      }
    } else if constexpr (std::is_same_v<Layout, StridedLayout>) {
      constexpr int rank = std::tuple_size<std::remove_cvref_t<decltype(ishape())>>{};
      for (const auto sr = shape_range_<rank>(ishape()); const auto& i : sr) {
        data(i) *= x;
      }
    }
    return *static_cast<Derived*>(this);
  }

  /* Indexable /= scalar */
  template <class U>
    requires std::is_convertible_v<U, T>
  Derived& operator/=(const U& x)
  {
    check_indexable();
    if constexpr (std::is_same_v<Layout, ContiguousLayout>) {
      qleve::scale(isize(), 1.0 / x, data());
    } else if constexpr (std::is_same_v<Layout, ColumnwiseLayout>) {
      const qtens_len_t ncols = this->icols();
      const qtens_len_t nrows = this->irows();
      for (qtens_len_t j = 0; j < ncols; ++j) {
        qleve::scale(nrows, 1.0 / x, &data(0, j));
      }
    } else if constexpr (std::is_same_v<Layout, StridedLayout>) {
      constexpr int rank = std::tuple_size<std::remove_cvref_t<decltype(ishape())>>{};
      for (const auto sr = shape_range_<rank>(ishape()); const auto& i : sr) {
        data(i) /= x;
      }
    }
    return *static_cast<Derived*>(this);
  }

#define XM_REGISTER_OPEQUALS(opname, op)                                               \
  /* Indexable op= Indexable */                                                        \
  template <class U, class A, class L>                                                 \
    requires std::is_convertible_v<U, T>                                               \
  Derived& opname(const Indexable<U, A, L>& x)                                         \
  {                                                                                    \
    check_indexable();                                                                 \
    assert(have_matching_shapes(x));                                                   \
    using composed_layout = compose_layout_t<L, Layout>;                               \
    if constexpr (std::is_same_v<composed_layout, ContiguousLayout>) {                 \
      const auto size = isize();                                                       \
      for (qtens_len_t i = 0; i < size; ++i)                                           \
        data(i) op x.data(i);                                                          \
    } else if constexpr (std::is_same_v<composed_layout, ColumnwiseLayout>) {          \
      const qtens_len_t ncols = this->icols();                                         \
      const qtens_len_t nrows = this->irows();                                         \
      for (qtens_len_t j = 0; j < ncols; ++j) {                                        \
        for (qtens_len_t i = 0; i < nrows; ++i) {                                      \
          data(i, j) op x.data(i, j);                                                  \
        }                                                                              \
      }                                                                                \
    } else if constexpr (std::is_same_v<composed_layout, StridedLayout>) {             \
      constexpr int rank = std::tuple_size<std::remove_cvref_t<decltype(ishape())>>{}; \
      for (const auto sr = shape_range_<rank>(ishape()); const auto& i : sr) {         \
        data(i) op x.data(i);                                                          \
      }                                                                                \
    }                                                                                  \
    return *static_cast<Derived*>(this);                                               \
  }                                                                                    \
                                                                                       \
  /* Indexable op=  Xpr */                                                             \
  template <class U, class L, class E>                                                 \
    requires std::is_convertible_v<U, T>                                               \
  Derived& opname(const Xpr<U, L, E>& x)                                               \
  {                                                                                    \
    check_indexable();                                                                 \
    assert(have_matching_shapes(x));                                                   \
    using composed_layout = compose_layout_t<L, Layout>;                               \
    if constexpr (std::is_same_v<composed_layout, ContiguousLayout>) {                 \
      const auto size = isize();                                                       \
      for (qtens_len_t i = 0; i < size; ++i)                                           \
        data(i) op x.data(i);                                                          \
    } else if constexpr (std::is_same_v<composed_layout, ColumnwiseLayout>) {          \
      const qtens_len_t ncols = this->icols();                                         \
      const qtens_len_t nrows = this->irows();                                         \
      for (qtens_len_t j = 0; j < ncols; ++j) {                                        \
        for (qtens_len_t i = 0; i < nrows; ++i) {                                      \
          data(i, j) op x.data(i, j);                                                  \
        }                                                                              \
      }                                                                                \
    } else if constexpr (std::is_same_v<composed_layout, StridedLayout>) {             \
      constexpr int rank = std::tuple_size<std::remove_cvref_t<decltype(ishape())>>{}; \
      for (const auto sr = shape_range_<rank>(ishape()); const auto& i : sr) {         \
        data(i) op x.data(i);                                                          \
      }                                                                                \
    }                                                                                  \
    return *static_cast<Derived*>(this);                                               \
  }

  XM_REGISTER_OPEQUALS(operator+=, +=)
  XM_REGISTER_OPEQUALS(operator-=, -=)
  XM_REGISTER_OPEQUALS(operator*=, *=)
  XM_REGISTER_OPEQUALS(operator/=, /=)

#undef XM_REGISTER_OPEQUALS

  /// Specialize *this += Indexable -> ax_plus_y
  template <class A>
  Derived& operator+=(const Indexable<T, A, ContiguousLayout>& x)
  {
    check_indexable();
    if constexpr (std::is_same_v<Layout, ContiguousLayout>) {
      qleve::ax_plus_y(isize(), 1.0, x.data(), data());
    } else if constexpr (std::is_same_v<Layout, ColumnwiseLayout>) {
      const qtens_len_t ncols = this->icols();
      const qtens_len_t nrows = this->irows();
      for (qtens_len_t j = 0; j < ncols; ++j) {
        qleve::ax_plus_y(nrows, 1.0, &x.data(0, j), &data(0, j));
      }
    } else if constexpr (std::is_same_v<Layout, StridedLayout>) {
      constexpr int rank = std::tuple_size<std::remove_cvref_t<decltype(ishape())>>{};
      for (const auto sr = shape_range_<rank>(ishape()); const auto& i : sr) {
        data(i) += x.data(i);
      }
    }
    return *static_cast<Derived*>(this);
  }

  /// Specialize *this -= Indexable -> ax_plus_y
  template <class A>
  Derived& operator-=(const Indexable<T, A, ContiguousLayout>& x)
  {
    check_indexable();
    if constexpr (std::is_same_v<Layout, ContiguousLayout>) {
      qleve::ax_plus_y(isize(), -1.0, x.data(), data());
    } else if constexpr (std::is_same_v<Layout, ColumnwiseLayout>) {
      const qtens_len_t ncols = this->icols();
      const qtens_len_t nrows = this->irows();
      for (qtens_len_t j = 0; j < ncols; ++j) {
        qleve::ax_plus_y(nrows, -1.0, &x.data(0, j), &data(0, j));
      }
    } else if constexpr (std::is_same_v<Layout, StridedLayout>) {
      constexpr int rank = std::tuple_size<std::remove_cvref_t<decltype(ishape())>>{};
      for (const auto sr = shape_range_<rank>(ishape()); const auto& i : sr) {
        data(i) -= x.data(i);
      }
    }
    return *static_cast<Derived*>(this);
  }

  /// Specialize *this += scalar * Indexable -> ax_plus_y
  template <typename U, class B>
    requires std::is_convertible_v<U, T>
  Derived& operator+=(
      const Xpr<T, ContiguousLayout, ScalarXprOp<U, CRI<T, B, ContiguousLayout>, OpMul<U, T>>>& xpr)
  {
    check_indexable();
    if constexpr (std::is_same_v<Layout, ContiguousLayout>) {
      qleve::ax_plus_y(isize(), xpr.e_.scalar_, xpr.e_.b_.c_.data(), data());
    } else if constexpr (std::is_same_v<Layout, ColumnwiseLayout>) {
      const qtens_len_t ncols = this->icols();
      const qtens_len_t nrows = this->irows();
      for (qtens_len_t j = 0; j < ncols; ++j) {
        qleve::ax_plus_y(nrows, xpr.e_.scalar_, &xpr.e_.b_.c_.data(0, j), &data(0, j));
      }
    } else if constexpr (std::is_same_v<Layout, StridedLayout>) {
      constexpr int rank = std::tuple_size<std::remove_cvref_t<decltype(ishape())>>{};
      for (const auto sr = shape_range_<rank>(ishape()); const auto& i : sr) {
        data(i) += xpr.e_.scalar_ * xpr.e_.b_.c_.data(i);
      }
    }
    return *static_cast<Derived*>(this);
  }

  /// Specialize *this -= scalar * Indexable -> ax_plus_y
  template <typename U, class B>
    requires std::is_convertible_v<U, T>
  Derived& operator-=(
      const Xpr<T, ContiguousLayout, ScalarXprOp<U, CRI<T, B, ContiguousLayout>, OpMul<U, T>>>& xpr)
  {
    check_indexable();
    if constexpr (std::is_same_v<Layout, ContiguousLayout>) {
      qleve::ax_plus_y(isize(), -xpr.e_.scalar_, xpr.e_.b_.c_.data(), data());
    } else if constexpr (std::is_same_v<Layout, ColumnwiseLayout>) {
      const qtens_len_t ncols = this->icols();
      const qtens_len_t nrows = this->irows();
      for (qtens_len_t j = 0; j < ncols; ++j) {
        qleve::ax_plus_y(nrows, -xpr.e_.scalar_, &xpr.e_.b_.c_.data(0, j), &data(0, j));
      }
    } else if constexpr (std::is_same_v<Layout, StridedLayout>) {
      constexpr int rank = std::tuple_size<std::remove_cvref_t<decltype(ishape())>>{};
      for (const auto sr = shape_range_<rank>(ishape()); const auto& i : sr) {
        data(i) -= xpr.e_.scalar_ * xpr.e_.b_.c_.data(i);
      }
    }
    return *static_cast<Derived*>(this);
  }

  /// Specialize *this += Indexable * scalar -> ax_plus_y
  template <class A, typename U>
    requires std::is_convertible_v<U, T>
  Derived& operator+=(
      const Xpr<T, ContiguousLayout, XprScalarOp<CRI<T, A, ContiguousLayout>, U, OpMul<T, U>>>& xpr)
  {
    check_indexable();
    if constexpr (std::is_same_v<Layout, ContiguousLayout>) {
      qleve::ax_plus_y(isize(), xpr.e_.scalar_, xpr.e_.a_.c_.data(), data());
    } else if constexpr (std::is_same_v<Layout, ColumnwiseLayout>) {
      const qtens_len_t ncols = this->icols();
      const qtens_len_t nrows = this->irows();
      for (qtens_len_t j = 0; j < ncols; ++j) {
        qleve::ax_plus_y(nrows, xpr.e_.scalar_, &xpr.e_.a_.c_.data(0, j), &data(0, j));
      }
    } else if constexpr (std::is_same_v<Layout, StridedLayout>) {
      constexpr int rank = std::tuple_size<std::remove_cvref_t<decltype(ishape())>>{};
      for (const auto sr = shape_range_<rank>(ishape()); const auto& i : sr) {
        data(i) += xpr.e_.scalar_ * xpr.e_.a_.c_.data(i);
      }
    }
    return *static_cast<Derived*>(this);
  }

  /// Specialize *this -= Indexable * scalar -> ax_plus_y
  template <class A, typename U>
    requires std::is_convertible_v<U, T>
  Derived& operator-=(
      const Xpr<T, ContiguousLayout, XprScalarOp<CRI<T, A, ContiguousLayout>, U, OpMul<T, U>>>& xpr)
  {
    check_indexable();
    if constexpr (std::is_same_v<Layout, ContiguousLayout>) {
      qleve::ax_plus_y(isize(), -xpr.e_.scalar_, xpr.e_.a_.c_.data(), data());
    } else if constexpr (std::is_same_v<Layout, ColumnwiseLayout>) {
      const qtens_len_t ncols = this->icols();
      const qtens_len_t nrows = this->irows();
      for (qtens_len_t j = 0; j < ncols; ++j) {
        qleve::ax_plus_y(nrows, -xpr.e_.scalar_, &xpr.e_.a_.c_.data(0, j), &data(0, j));
      }
    } else if constexpr (std::is_same_v<Layout, StridedLayout>) {
      constexpr int rank = std::tuple_size<std::remove_cvref_t<decltype(ishape())>>{};
      for (const auto sr = shape_range_<rank>(ishape()); const auto& i : sr) {
        data(i) -= xpr.e_.scalar_ * xpr.e_.a_.c_.data(i);
      }
    }
    return *static_cast<Derived*>(this);
  }
};

} // namespace qleve

#endif // QLEVE_INDEXABLE_HPP
