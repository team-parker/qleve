// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file include/qleve/array.hpp
///
/// Array class for storage of 1D data along with associated views.

#ifndef QLEVE_ARRAY_HPP
#define QLEVE_ARRAY_HPP

#include <algorithm>
#include <utility>

#include <qleve/tensor.hpp>

namespace qleve
{

using Array = Tensor_<double, 1>;
using ArrayView = TensorView_<double, 1>;
using ConstArrayView = ConstTensorView_<double, 1>;

using ZArray = Tensor_<dcomplex, 1>;
using ZArrayView = TensorView_<dcomplex, 1>;
using ConstZArrayView = ConstTensorView_<dcomplex, 1>;

} // namespace qleve


#endif // QLEVE_ARRAY_HPP
