// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file include/qleve/tensor_indices.hpp
///
/// Representations for tensor indices that can allow for type checking

#ifndef QLEVE_TENSOR_INDICES_HPP
#define QLEVE_TENSOR_INDICES_HPP

#include <algorithm>
#include <cassert>
#include <cmath>
#include <cstddef>

#include <qleve/indexable_traits.hpp>
#include <qleve/tensor_traits.hpp>

namespace qleve
{

/// Dummy function to force a compiler error
void index_error(const char* msg);

/// A simplified string view that is constexpr friendly.
template <typename Char>
class basic_string_view {
 private:
  const Char* data_;
  size_t size_;

 public:
  using value_type = Char;
  using iterator = const Char*;

  constexpr basic_string_view() noexcept : data_(nullptr), size_(0) {}

  /** Constructs a string reference object from a C string and a size. */
  constexpr basic_string_view(const Char* s, size_t count) noexcept : data_(s), size_(count) {}

  constexpr basic_string_view(std::nullptr_t) = delete;

  ///  Constructs a string reference object from a C string.
  constexpr basic_string_view(const Char* s) : data_(s), size_(std::char_traits<Char>::length(s)) {}

  /**
    Constructs a string reference from a ``std::basic_string`` or a
    ``std::basic_string_view`` object.
  */
  template <typename S>
  constexpr basic_string_view(const S& s) noexcept : data_(s.data()), size_(s.size())
  {}

  size_t size() const { return size_; }

  const Char* c_str() const { return data_; }
};

/// Helper type to initialize a runtime index from a string literal.
///
/// This extra type is needed so that indices default to compile time checks.
template <typename Char>
struct runtime_index {
  basic_string_view<Char> str_;
};

/// Helper function to initialize a runtime index from a string literal.
inline auto runtime(const char* s) -> runtime_index<char> { return {s}; }

/// Base class for indices that can be used to index a tensor.
template <typename Char, shaped T>
class basic_indices {
 private:
  basic_string_view<Char> str_;

 public:
  /// Constructs a new index from a string at compile time.
  template <typename S>
  consteval inline basic_indices(const S& s) : str_(s)
  {
    if (std::char_traits<Char>::length(s) != tensor_rank<T>::value) {
      // forces a compile time error by calling a non constexpr function
      index_error("Number of indices must match rank of tensor");
    }
  }

  basic_indices(const runtime_index<Char>& s) : str_(s.str_)
  {
    assert(std::char_traits<Char>::length(str_.c_str()) == tensor_rank<T>::value);
  }

  operator basic_string_view<Char>() const { return str_; }

  const Char* c_str() const { return str_.c_str(); }
};

/// Class that stores the indices of a tensor and checks that they correspond to the tensor rank.
///
/// Use this class in templated functions that accept a tensor and its indices. For example:
///
/// \code
/// template <tensorable T>
/// void foo(const T& t, indices<T> i);
/// \endcode
///
/// Using it in this way will automatically verify that the number of indices match the rank
/// of the tensor. If called as ``foo(t, "ij")`` the check will happen at compile time. To force a
/// runtime check, use the ``runtime`` function:
///
/// \code
/// foo(t, runtime("ij"));
/// \endcode
template <typename T>
using indices = basic_indices<char, std::type_identity_t<T>>;

} // namespace qleve

#endif // QLEVE_TENSOR_INDICES_HPP
