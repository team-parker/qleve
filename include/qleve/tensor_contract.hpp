// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file include/qleve/tensor_contract.hpp
///
/// Interface for tensor contractions

#ifndef QLEVE_TENSOR_CONTRACT_HPP
#define QLEVE_TENSOR_CONTRACT_HPP

#include <algorithm>
#include <complex>
#include <utility>

#include <qleve/pod_types.hpp>
#include <qleve/tblis_interface.hpp>
#include <qleve/tensor_checks.hpp>
#include <qleve/tensor_indices.hpp>
#include <qleve/tensor_traits.hpp>

namespace qleve
{

template <const_strided_tensorable TensorA, const_strided_tensorable TensorB,
          strided_tensorable TensorC, typename T, typename U>
  requires are_convertible_scalars<TensorC, T, U>::value
void contract(const T& alpha, const TensorA& A, indices<TensorA> indA, const TensorB& B,
              indices<TensorB> indB, const U& beta, TensorC&& C, indices<TensorC> indC,
              const bool& single = false)
{
  const char* labelsA = indA.c_str();
  const char* labelsB = indB.c_str();
  const char* labelsC = indC.c_str();

  assert(is_contractable(A.shape(), labelsA, B.shape(), labelsB, C.shape(), labelsC));

  tblis_const_tensor_wrap<TensorA> tensorA(false, alpha, A);
  tblis_const_tensor_wrap<TensorB> tensorB(false, 1.0, B);
  tblis_tensor_wrap<TensorC> tensorC(false, beta, C);

  const tblis_comm* comm = single ? tblis_single : nullptr;

  tblis::tblis_tensor_mult(comm, nullptr, tensorA.tblis_tensor(), labelsA, tensorB.tblis_tensor(),
                           labelsB, tensorC.tblis_tensor(), labelsC);
}

template <const_strided_tensorable TensorA, const_strided_tensorable TensorB,
          strided_tensorable TensorC, typename T, typename U>
  requires are_convertible_scalars<TensorC, T, U>::value
void contract(const bool& conjA, const bool& conjB, const T& alpha, const TensorA& A,
              indices<TensorA> indA, const TensorB& B, indices<TensorB> indB, const U& beta,
              TensorC&& C, indices<TensorC> indC, const bool& single = false)
{
  const char* labelsA = indA.c_str();
  const char* labelsB = indB.c_str();
  const char* labelsC = indC.c_str();

  assert(is_contractable(A.shape(), labelsA, B.shape(), labelsB, C.shape(), labelsC));

  tblis_const_tensor_wrap<TensorA> tensorA(conjA, alpha, A);
  tblis_const_tensor_wrap<TensorB> tensorB(conjB, 1.0, B);
  tblis_tensor_wrap<TensorC> tensorC(false, beta, C);

  const tblis_comm* comm = single ? tblis_single : nullptr;

  tblis::tblis_tensor_mult(comm, nullptr, tensorA.tblis_tensor(), labelsA, tensorB.tblis_tensor(),
                           labelsB, tensorC.tblis_tensor(), labelsC);
}

} // namespace qleve

#endif // QLEVE_TENSOR_CONTRACT_HPP
