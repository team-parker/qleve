// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file include/qleve/blas_interface.hpp
///
/// Provides simplified interface to BLAS routines

#ifndef QLEVE_BLAS_INTERFACE_HPP
#define QLEVE_BLAS_INTERFACE_HPP

#include <complex>
#include <cstdint>

#include <qleve/pod_types.hpp>

#define MKL_Complex16 qleve::dcomplex
#include <mkl.h>
using blasint_t = MKL_INT;

namespace qleve
{

namespace
{

inline void dgemm_(const char* transa, const char* transb, const blasint_t m, const blasint_t n,
                   const blasint_t k, const double alpha, const double* a, const blasint_t lda,
                   const double* b, const blasint_t ldb, const double beta, double* c,
                   const blasint_t ldc) noexcept
{
  dgemm(transa, transb, &m, &n, &k, &alpha, a, &lda, b, &ldb, &beta, c, &ldc);
}

inline void dgemv_(const char* a, const blasint_t b, const blasint_t c, const double d,
                   const double* e, const blasint_t f, const double* g, const blasint_t h,
                   const double i, double* j, const blasint_t k) noexcept
{
  dgemv(a, &b, &c, &d, e, &f, g, &h, &i, j, &k);
}

inline void daxpy_(const blasint_t a, const double b, const double* c, const blasint_t d, double* e,
                   const blasint_t f) noexcept
{
  daxpy(&a, &b, c, &d, e, &f);
}

inline void dscal_(const blasint_t a, const double b, double* c, const blasint_t d) noexcept
{
  dscal(&a, &b, c, &d);
}

inline double dasum_(const blasint_t n, const double* x, const blasint_t incx) noexcept
{
  return dasum(&n, x, &incx);
}

inline double ddot_(const blasint_t a, const double* b, const blasint_t c, const double* d,
                    const blasint_t e) noexcept
{
  return ddot(&a, b, &c, d, &e);
}

inline blasint_t dsyev_(const char* a, const char* b, const blasint_t c, double* d,
                        const blasint_t e, double* f, double* g, const blasint_t h) noexcept
{
  blasint_t i;
  dsyev(a, b, &c, d, &e, f, g, &h, &i);
  return i;
}

inline blasint_t dpotrf_(const char* uplo, const blasint_t n, double* a,
                         const blasint_t lda) noexcept
{
  blasint_t i;
  dpotrf(uplo, &n, a, &lda, &i);
  return i;
}

inline void dtrsm_(const char* side, const char* uplo, const char* transa, const char* diag,
                   const blasint_t m, const blasint_t n, const double alpha, const double* a,
                   const blasint_t lda, double* b, const blasint_t ldb) noexcept
{
  dtrsm(side, uplo, transa, diag, &m, &n, &alpha, a, &lda, b, &ldb);
}

inline blasint_t dsysv_(const char* uplo, const blasint_t n, const blasint_t nrhs, double* a,
                        const blasint_t lda, blasint_t* ipiv, double* b, const blasint_t ldb,
                        double* work, const blasint_t lwork) noexcept
// TODO double check ipiv
{
  blasint_t info;
  dsysv(uplo, &n, &nrhs, a, &lda, ipiv, b, &ldb, work, &lwork, &info);
  return info;
}

inline void dgesv_(const blasint_t n, const blasint_t nrhs, double* a, const blasint_t lda,
                   blasint_t* ipiv, double* b, const blasint_t ldb, blasint_t& info) noexcept
// TODO double check ipiv
{
  dgesv(&n, &nrhs, a, &lda, ipiv, b, &ldb, &info);
}

inline void dger_(const blasint_t a, const blasint_t b, const double c, const double* d,
                  const blasint_t e, const double* f, const blasint_t g, double* h,
                  const blasint_t i) noexcept
{
  dger(&a, &b, &c, d, &e, f, &g, h, &i);
}

inline void drot_(const blasint_t a, double* b, const blasint_t c, double* d,
                  const blasint_t e, const double f, const double g) noexcept
{
  drot(&a, b, &c, d, &e, &f, &g);
}

inline blasint_t dgesvd_(const char* a, const char* b, const blasint_t c, const blasint_t d,
                         double* e, const blasint_t f, double* g, double* h, const blasint_t i,
                         double* j, const blasint_t k, double* l, const blasint_t m) noexcept
{
  blasint_t info;
  dgesvd(a, b, &c, &d, e, &f, g, h, &i, j, &k, l, &m, &info);
  return info;
}

inline blasint_t dgetrf_(const blasint_t m, const blasint_t n, double* a, const blasint_t lda,
                         blasint_t* ipiv) noexcept
{
  blasint_t out;
  dgetrf(&m, &n, a, &lda, ipiv, &out);
  return out;
}

inline void zgesvd_(const char* a, const char* b, const blasint_t c, const blasint_t d,
                    qleve::dcomplex* e, const blasint_t f, double* g, qleve::dcomplex* h,
                    const blasint_t i, qleve::dcomplex* j, const blasint_t k, qleve::dcomplex* l,
                    const blasint_t m, double* n, blasint_t& o) noexcept
{
  zgesvd(a, b, &c, &d, e, &f, g, h, &i, j, &k, l, &m, n, &o);
}

inline void zgemv_(const char* a, const blasint_t b, const blasint_t c, const qleve::dcomplex d,
                   const qleve::dcomplex* e, const blasint_t f, const qleve::dcomplex* g,
                   const blasint_t h, const qleve::dcomplex i, qleve::dcomplex* j,
                   const blasint_t k) noexcept
{
  zgemv(a, &b, &c, &d, e, &f, g, &h, &i, j, &k);
}

inline void zgemm_(const char* tra, const char* trb, const blasint_t n, const blasint_t m,
                   const blasint_t k, const qleve::dcomplex alpha, const qleve::dcomplex* A,
                   const blasint_t lda, const qleve::dcomplex* B, const blasint_t ldb,
                   const qleve::dcomplex beta, qleve::dcomplex* C, const blasint_t ldc) noexcept
{
  zgemm(tra, trb, &n, &m, &k, &alpha, A, &lda, B, &ldb, &beta, C, &ldc);
}

#ifdef HAVE_MKL
inline void zgemm3m_(const char* transa, const char* transb, const blasint_t m, const blasint_t n,
                     const blasint_t k, const qleve::dcomplex alpha, const qleve::dcomplex* a,
                     const blasint_t lda, const qleve::dcomplex* b, const blasint_t ldb,
                     const qleve::dcomplex beta, qleve::dcomplex* c, const blasint_t ldc) noexcept
{
  zgemm3m(transa, transb, &m, &n, &k, &alpha, a, &lda, b, &ldb, &beta, c, &ldc);
}
#endif

inline void zaxpy_(const blasint_t a, const qleve::dcomplex b, const qleve::dcomplex* c,
                   const blasint_t d, qleve::dcomplex* e, const blasint_t f) noexcept
{
  zaxpy(&a, &b, c, &d, e, &f);
}

inline void zscal_(const blasint_t a, const qleve::dcomplex b, qleve::dcomplex* c,
                   const blasint_t d) noexcept
{
  zscal(&a, &b, c, &d);
}

#ifndef ZDOT_RETURN
qleve::dcomplex zdotc_(const blasint_t b, const qleve::dcomplex* c, const blasint_t d,
                       const qleve::dcomplex* e, const blasint_t f) noexcept
{
  qleve::dcomplex a;
  zdotc(&a, &b, c, &d, e, &f);
  return a;
}
#else
qleve::dcomplex zdotc_(const blasint_t a, const qleve::dcomplex* b, const blasint_t c,
                       const qleve::dcomplex* d, const blasint_t e) noexcept
{
  return zdotc(&a, b, &c, d, &e);
}
#endif

inline void zgesv_(const blasint_t n, const blasint_t nrhs, qleve::dcomplex* a, const blasint_t lda,
                   blasint_t* ipiv, qleve::dcomplex* b, const blasint_t ldb,
                   blasint_t& info) noexcept
// TODO double check ipiv
{
  zgesv(&n, &nrhs, a, &lda, ipiv, b, &ldb, &info);
}

inline void zheev_(const char* a, const char* b, const blasint_t c, qleve::dcomplex* d,
                   const blasint_t e, double* f, qleve::dcomplex* g, const blasint_t h, double* i,
                   blasint_t& j) noexcept
{
  zheev(a, b, &c, d, &e, f, g, &h, i, &j);
}

inline void zgeev_(const char* a, const char* b, const blasint_t c, qleve::dcomplex* d,
                   const blasint_t e, qleve::dcomplex* f, qleve::dcomplex* g, const blasint_t h,
                   qleve::dcomplex* i, const blasint_t j, qleve::dcomplex* k, const blasint_t l,
                   double* m, blasint_t& n) noexcept
{
  zgeev(a, b, &c, d, &e, f, g, &h, i, &j, k, &l, m, &n);
}

inline void zhbev_(const char* a, const char* b, const blasint_t c, const blasint_t d,
                   qleve::dcomplex* e, const blasint_t f, double* g, qleve::dcomplex* h,
                   const blasint_t i, qleve::dcomplex* j, double* k, blasint_t& l) noexcept
{
  zhbev(a, b, &c, &d, e, &f, g, h, &i, j, k, &l);
}

inline void zrot_(const blasint_t a, qleve::dcomplex* b, const blasint_t c, qleve::dcomplex* d,
                  const blasint_t e, const double f, const qleve::dcomplex g) noexcept
{
  zrot(&a, b, &c, d, &e, &f, &g);
}

inline void zgerc_(const blasint_t a, const blasint_t b, const qleve::dcomplex c,
                   const qleve::dcomplex* d, const blasint_t e, const qleve::dcomplex* f,
                   const blasint_t g, qleve::dcomplex* h, const blasint_t i) noexcept
{
  zgerc(&a, &b, &c, d, &e, f, &g, h, &i);
}

inline void zgeru_(const blasint_t a, const blasint_t b, const qleve::dcomplex c,
                   const qleve::dcomplex* d, const blasint_t e, const qleve::dcomplex* f,
                   const blasint_t g, qleve::dcomplex* h, const blasint_t i) noexcept
{
  zgeru(&a, &b, &c, d, &e, f, &g, h, &i);
}

} // namespace
} // namespace qleve


#endif // QLEVE_BLAS_INTERFACE_HPP
