#include "catch.hpp"
#include "util.hpp"

#include <qleve/matrix_multiply.hpp>
#include <qleve/matrix_phase_adjust.hpp>
#include <qleve/tensor.hpp>

using namespace std;
using namespace qleve;

using Catch::Matchers::WithinAbs;

TEST_CASE("Complex Matrix Linear Algebra", "[zmatrix]")
{
  ZTensor<2> A(2, 2);

  A(0, 0) = 1.0;
  A(1, 0) = 2.0;
  A(0, 1) = complex<double>(1.0, 2.0);
  A(1, 1) = complex<double>(0.0, 1.0);

  REQUIRE(A.size() == 4);
  REQUIRE(A.shape()[0] == 2);
  REQUIRE(A.shape()[1] == 2);

  SECTION("view functions as alias")
  {
    ZTensorView<2> aview(A);
    aview *= complex<double>(0.0, 2.0);

    CHECK(A(0, 0) == aview(0, 0));
    CHECK(A(1, 0) == aview(1, 0));
    CHECK(A(0, 1) == aview(0, 1));
    CHECK(A(1, 1) == aview(1, 1));
  }

  SECTION("matrix += scalar * matrix -> zaxpy")
  {
    A += complex<double>(1.0, 0.2) * A;

    REQUIRE(A.norm() == Approx(6.666333325));
  }

  SECTION("can create matrix from template expression")
  {
    ZTensor<2> B(2, 2);
    B(0, 0) = 10.0;
    B(0, 1) = 12.0;
    B(1, 0) = 14.0;
    B(1, 1) = 16.0;

    ZTensorView<2> aview(A);

    ZTensor<2> C = 2.0 * aview - B / 2.0;

    CHECK(C(0, 0).real() == Approx(-3.0));
    CHECK(C(0, 0).imag() == Approx(0.0));
  }

  SECTION("can create zmatrix from real matrix")
  {
    Tensor<2> realA = qleve::real(A);

    ZTensor<2> B = realA;

    REQUIRE(realA.norm() == Approx(B.norm()));
  }

  SECTION("matrix multiply")
  {
    ZTensor<2> B(A);
    ZTensorView<2> aview(A);

    qleve::gemm("C", "N", 1.0, aview, A, 0.0, B);

    REQUIRE(B.norm() == Approx(9.7467943448));
  }

  SECTION("dagger symmetrizes")
  {
    ZTensor<2> symmA = A + A.dagger();

    ZTensor<2> tmp = symmA - symmA.dagger();

    REQUIRE(tmp.norm() == Approx(0.0).margin(1e-15));
  }

  SECTION("phase adjust complex matrix")
  {
    phase_adjust(A);

    CHECK_THAT(A(0, 0).real(), WithinAbs(1.0, 1e-15));
    CHECK_THAT(A(0, 0).imag(), WithinAbs(0.0, 1e-15));

    CHECK_THAT(A(1, 0).real(), WithinAbs(2.0, 1e-15));
    CHECK_THAT(A(1, 0).imag(), WithinAbs(0.0, 1e-15));

    CHECK_THAT(A(0, 1).real(), WithinAbs(std::sqrt(5), 1e-15));
    CHECK_THAT(A(0, 1).imag(), WithinAbs(0.0, 1e-15));

    const auto z = dcomplex(0.0, 1.0) * dcomplex(1.0, -2.0) / std::abs(dcomplex(1.0, -2.0));
    CHECK_THAT(A(1, 1).real(), WithinAbs(z.real(), 1e-15));
    CHECK_THAT(A(1, 1).imag(), WithinAbs(z.imag(), 1e-15));
  }
}
