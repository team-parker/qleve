#include <numeric>

#include "catch.hpp"

#include <qleve/checks.hpp>
#include <qleve/cholesky.hpp>
#include <qleve/determinant.hpp>
#include <qleve/diagonalize.hpp>
#include <qleve/diagonalize_with_metric.hpp>
#include <qleve/matrix_transform.hpp>
#include <qleve/rotate.hpp>
#include <qleve/solve_herm_lse.hpp>
#include <qleve/svd.hpp>
#include <qleve/tensor.hpp>
#include <qleve/trace.hpp>
#include <qleve/unitary_exp.hpp>

using namespace std;
using namespace qleve;

TEST_CASE("Linear Algebra Tests", "[linear algebra]")
{
  const double pi = atan2(0, -1);

  const size_t n = 3;
  std::vector<double> x(n);
  std::iota(x.data(), x.data() + x.size(), 1);

  std::vector<double> y(n);
  std::iota(y.data(), y.data() + y.size(), 9);

  Tensor<2> A(2, 2);
  A(0, 0) = 0.0;
  A(0, 1) = A(1, 0) = 0.5;
  A(1, 1) = 4.0;

  Tensor<2> Aref(A);

  REQUIRE(A.size() == 4);
  REQUIRE(A.shape()[0] == 2);
  REQUIRE(A.shape()[1] == 2);

  CHECK(linalg::is_hermitian(A));
  CHECK(!linalg::is_identity(A));
  CHECK(!linalg::is_idempotent(A));

  qleve::Tensor<2> C(4, 4);

  std::default_random_engine generator;
  std::uniform_real_distribution<double> distribution(-1.0, 1.0);
  for (size_t i = 0; i < C.size(); ++i) {
    C.data(i) = distribution(generator);
  }

  SECTION("Can rotate")
  {
    rotate(x.size(), x.data(), y.data(), 0.5 * pi);

    CHECK(x[0] == Approx(9));
    CHECK(x[1] == Approx(10));
    CHECK(x[2] == Approx(11));

    CHECK(y[0] == Approx(-1));
    CHECK(y[1] == Approx(-2));
    CHECK(y[2] == Approx(-3));
  }

  SECTION("Exponential rotation matrix: Identity")
  {
    Tensor<2> kappa(2, 2);

    auto U1 = linalg::unitary_exp(kappa);
    CHECK(U1.extent(0) == 4);
    CHECK(U1.extent(1) == 4);
    CHECK(U1(0, 0) == Approx(1.0));
    CHECK(U1(1, 1) == Approx(1.0));
    CHECK(U1(2, 2) == Approx(1.0));
    CHECK(U1(3, 3) == Approx(1.0));
    CHECK(U1(1, 0) == Approx(0.0).margin(1e-8));
    CHECK(U1(2, 1) == Approx(0.0).margin(1e-8));
  }

  SECTION("Exponential rotation matrix: kappa")
  {
    Tensor<2> kappa(2, 2);
    kappa(0, 0) = 0.1;
    kappa(1, 0) = -0.2;
    kappa(0, 1) = 0.3;
    kappa(1, 1) = -0.4;
    ConstTensorView<2> ck(kappa);
    auto U2 = linalg::unitary_exp(ck);

    CHECK(U2.extent(0) == 4);
    CHECK(U2.extent(1) == 4);
    CHECK(U2(0, 0) == Approx(0.97560231));
    CHECK(U2(1, 1) == Approx(0.87807755));
    CHECK(U2(2, 2) == Approx(0.95122112));
    CHECK(U2(3, 3) == Approx(0.90245874));

    CHECK(U2(1, 0) == Approx(-0.05363862));
    CHECK(U2(0, 2) == Approx(-0.09376066));
    CHECK(U2(1, 3) == Approx(0.37996818));
  }

  SECTION("trace") { REQUIRE(linalg::trace(A) == A(0, 0) + A(1, 1)); }

  SECTION("orthogonalization Tensor<2> based on provided metric")
  {
    qleve::Tensor<2> S(4, 4);
    qleve::gemm("t", "n", 1.0, C, C, 0.0, S);

    auto X = linalg::orthogonalize_metric(S);
    auto orthoC = C.zeros_like();
    qleve::gemm("n", "n", 1.0, C, X, 0.0, orthoC);

    auto orthoS = S.zeros_like();
    qleve::gemm("t", "n", 1.0, orthoC, orthoC, 0.0, orthoS);

    CHECK(orthoS(0, 0) == Approx(1.0).epsilon(1e-8));
    CHECK(orthoS(1, 1) == Approx(1.0).epsilon(1e-8));
    CHECK(orthoS(2, 2) == Approx(1.0).epsilon(1e-8));
    CHECK(orthoS(3, 3) == Approx(1.0).epsilon(1e-8));

    CHECK(orthoS(0, 1) == Approx(0.0).margin(1e-8));

    CHECK(linalg::is_identity(orthoS));
  }

  SECTION("orthogonalization TensorView<2> based on provided metric")
  {
    qleve::Tensor<2> S(4, 4);
    qleve::gemm("t", "n", 1.0, C, C, 0.0, S);

    auto Sview = S.slice(0, 4);
    auto X = linalg::orthogonalize_metric(Sview);
    auto orthoC = C.zeros_like();
    qleve::gemm("n", "n", 1.0, C, X, 0.0, orthoC);

    auto orthoS = S.zeros_like();
    qleve::gemm("t", "n", 1.0, orthoC, orthoC, 0.0, orthoS);

    CHECK(orthoS(0, 0) == Approx(1.0).epsilon(1e-8));
    CHECK(orthoS(0, 1) == Approx(0.0).margin(1e-8));
  }

  SECTION("Orthogonalize vectors")
  {
    ptrdiff_t northo = linalg::orthogonalize_vectors(C);
    auto orthoC = C.slice(0, northo);

    qleve::Tensor<2> S = orthoC.zeros_like();
    qleve::gemm("t", "n", 1.0, orthoC, orthoC, 0.0, S);

    CHECK(S(0, 0) == Approx(1.0).epsilon(1e-8));
    CHECK(S(0, 1) == Approx(0.0).margin(1e-8));
  }

  SECTION("diagonalize returns eigenvalues and eigenvectors")
  {
    CHECK(!linalg::is_diagonal(A));

    Tensor<2> U(A);
    auto eigenvalues = linalg::diagonalize(U);

    CHECK(eigenvalues(0) == Approx(-0.0615528128));
    CHECK(eigenvalues(1) == Approx(4.0615528128));

    matrix_transform(1.0, A, U, 0.0, A);
    CHECK(linalg::is_diagonal(A));
  }

  SECTION("diagonalize blocks makes the diagonal blocks diagonal")
  {
    Tensor<2> AA(2 * A.extent(0), 2 * A.extent(1));

    AA.subtensor({0, 0}, {2, 2}) = A;
    AA.subtensor({2, 2}, {4, 4}) = A;
    AA.subtensor({0, 2}, {2, 4}) = 1.0;
    AA.subtensor({2, 0}, {4, 2}) = 1.0;

    CHECK(!linalg::is_diagonal(AA));

    Tensor<2> U(AA);
    auto eigenvalues = linalg::diagonalize_blocks(U, {2, 0, 2});

    CHECK(eigenvalues(0) == Approx(-0.0615528128));
    CHECK(eigenvalues(1) == Approx(4.0615528128));
    CHECK(eigenvalues(2) == Approx(-0.0615528128));
    CHECK(eigenvalues(3) == Approx(4.0615528128));

    matrix_transform(1.0, AA, U, 0.0, AA);
    CHECK(linalg::is_diagonal(AA.subtensor({0, 0}, {2, 2})));
    CHECK(linalg::is_diagonal(AA.subtensor({2, 2}, {4, 4})));
  }

  SECTION("can take SVD")
  {
    auto [sigma, U, Vt] = linalg::svd(A);

    CHECK(sigma(0) == Approx(4.06155281));
    CHECK(sigma(1) == Approx(0.06155281));
  }

  SECTION("can take jacobi SVD")
  {
    auto [sigma, U, Vt] = linalg::svd_jacobi(A);

    CHECK(sigma(0) == Approx(4.06155281));
    CHECK(sigma(1) == Approx(0.06155281));
  }

  SECTION("can take preconditioned jacobi SVD")
  {
    auto [sigma, U, Vt] = linalg::svd_preconditioned_jacobi(A);

    CHECK(sigma(0) == Approx(4.06155281));
    CHECK(sigma(1) == Approx(0.06155281));
  }

  SECTION("can compute determinant")
  {
    const double det = linalg::determinant(A);

    CHECK(det == Approx(-0.25));
  }

  SECTION("can compute cholesky decomposition")
  {
    auto A2 = A.clone();
    A2(0, 0) += 1.0;

    linalg::cholesky(A2);

    CHECK(A2(0, 0) == Approx(1.0));
    CHECK(A2(1, 0) == Approx(0.5));
    CHECK(A2(1, 1) == Approx(1.93649167));

    Tensor<2> eye(2, 2);
    eye(0, 0) = 1.0;
    eye(1, 1) = 1.0;

    CHECK(linalg::is_identity(eye));
    CHECK(linalg::is_idempotent(eye));

    linalg::cholesky_solve("l", "l", "n", "n", A2, eye);

    CHECK(eye(0, 0) == Approx(1.0));
    CHECK(eye(1, 0) == Approx(-0.25819889));
    CHECK(eye(1, 1) == Approx(0.51639778));
  }

  SECTION("Solve Linear System of Equations")
  {
    Tensor<2> b(2, 1);
    b(0, 0) = 0.2;
    b(1, 0) = 1.0;

    auto x = linalg::solve_herm_lse(A, b);

    CHECK(x(0, 0) == Approx(-1.2));
    CHECK(x(1, 0) == Approx(0.4));
  }
}
