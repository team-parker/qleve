// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file include/qleve/checks.hpp
///
/// Routines to verify properties of matrices

#ifndef QLEVE_TENSOR_CHECKS_HPP
#define QLEVE_TENSOR_CHECKS_HPP

#include <map>
#include <set>

namespace qleve
{

template <typename ShapeA, typename ShapeB, typename ShapeC>
bool is_contractable(const ShapeA& shapeA, const std::string& idxA, const ShapeB& shapeB,
                     const std::string& idxB, const ShapeC& shapeC, const std::string& idxC)
{
  if ((shapeA.size() != idxA.size()) || (shapeB.size() != idxB.size())
      || (shapeC.size() != idxC.size())) {
    return false;
  }

  using idxmap = std::multimap<char, ptrdiff_t>;

  idxmap labels;
  const int sizeA = shapeA.size();
  const int sizeB = shapeB.size();
  const int sizeC = shapeC.size();
  for (int i = 0; i < sizeA; ++i) {
    labels.emplace(idxA[i], shapeA[i]);
  }
  for (int i = 0; i < sizeB; ++i) {
    labels.emplace(idxB[i], shapeB[i]);
  }
  for (int i = 0; i < sizeC; ++i) {
    labels.emplace(idxC[i], shapeC[i]);
  }

  std::set<char> all_idx;
  for (const auto& [key, val] : labels) {
    all_idx.insert(key);
  }

  for (const char& lab : all_idx) {
    if (labels.count(lab) > 1) { // check that all elements are equal
      auto rng = labels.equal_range(lab);
      auto n = *rng.first;
      bool allequal = std::all_of(rng.first, rng.second, [&n](auto x) { return x == n; });
      if (!allequal) {
        return false;
      }
    }
  }

  // if none of these checks crash out, then return true
  return true;
}

} // namespace qleve

#endif // QLEVE_TENSOR_CHECKS_HPP
