// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file include/qleve/tensor_base.hpp
///
/// TensorBase_ class provides shape, stride, and indexing type routines
/// for tensor types

#ifndef QLEVE_TENSOR_BASE_HPP
#define QLEVE_TENSOR_BASE_HPP

#include <array>
#include <iomanip>
#include <numeric>
#ifdef __cpp_lib_span
#include <span>
#endif // __cpp_lib_span
#include <utility>

#include <qleve/detail.hpp>
#include <qleve/indexable.hpp>
#include <qleve/pod_types.hpp>
#include <qleve/span_getter.hpp>
#include <qleve/storage.hpp>
#include <qleve/tensor_traits.hpp>
#include <qleve/vector_ops.hpp>

namespace qleve
{

/// Tensor base class for all indexing functions
template <typename T, int Rank, class Derived>
class TensorBase_ {
 protected:
  const std::array<qtens_len_t, Rank> extent_;

  const std::array<qtens_len_t, Rank> stride_;

  const qtens_len_t size_;

  // compute size helper for a variadic template
  template <typename... Args>
    requires(are_size_worthy_v<Args...>)
  inline qtens_len_t compute_size_(const Args&... args) const
  {
    static_assert(sizeof...(args) == Rank, "Tensor rank and extent don't match");
    return (args * ...);
  }

  inline qtens_len_t compute_size_(const std::array<qtens_len_t, Rank>& extent) const
  {
    return compute_size_(extent, std::make_index_sequence<Rank>());
  }

  template <size_t... I>
  inline qtens_len_t compute_size_(const std::array<qtens_len_t, Rank>& extent,
                                   std::index_sequence<I...>) const
  {
    return (extent[I] * ...);
  }

  inline qtens_len_t compute_index_(const std::array<qtens_len_t, Rank>& indices) const
  {
    return compute_index_(indices, std::make_index_sequence<Rank>());
  }

  template <size_t... I>
  inline qtens_len_t compute_index_(const std::array<qtens_len_t, Rank>& indices,
                                    std::index_sequence<I...>) const
  {
    return compute_index_(std::get<I>(indices)...);
  }

  template <typename... Args>
    requires(are_size_worthy_v<Args...>)
  inline qtens_len_t compute_index_(const Args&... args) const
  {
    static_assert(sizeof...(args) == Rank, "Tensor rank and length of index list don't match.");
    return compute_index_<0>(args...);
  }

  std::array<qtens_len_t, Rank> compute_stride_(const std::array<qtens_len_t, Rank>& ext) const
  {
    // according to gcc.godbolt.org, at least for small Ranks, this compiles into the equivalent
    // of doing {{ 1, ext[0], ... }}
    std::array<qtens_len_t, Rank> out;

    for (qtens_len_t i = 0; i < Rank; ++i) {
      out[i] = 1ul;
      for (qtens_len_t j = 0; j < i; ++j) {
        out[i] *= ext[j];
      }
    }

    return out;
  }

  template <typename... Args>
    requires(are_size_worthy_v<Args...>)
  inline std::array<qtens_len_t, Rank> make_extent_(const Args&... args) const
  {
    static_assert(sizeof...(args) == Rank, "Tensor rank and extent don't match");
    std::array<qtens_len_t, Rank> out;
    fill_extent_<0>(out, args...);
    return out;
  }

  template <typename... Args>
    requires(are_size_worthy_v<Args...>)
  inline bool check_bounds_(const Args&... args) const
  {
    return check_bounds_<0>(args...);
  }

  inline bool check_bounds_(const std::array<qtens_len_t, Rank>& i) const
  {
    return check_bounds_(i, std::make_index_sequence<Rank>());
  }

  template <size_t... I>
  inline bool check_bounds_(const std::array<qtens_len_t, Rank>& i, std::index_sequence<I...>) const
  {
    return check_bounds_(std::get<I>(i)...);
  }

  T* data_impl() { return static_cast<Derived*>(this)->data(); }
  const T* data_impl() const { return static_cast<const Derived*>(this)->data(); }

  T& data_impl(const qtens_len_t& i) { return static_cast<Derived*>(this)->data(i); }
  const T& data_impl(const qtens_len_t& i) const
  {
    return static_cast<const Derived*>(this)->data(i);
  }

  qtens_len_t column_index_impl(const qtens_len_t& i) const
  {
    return static_cast<const Derived*>(this)->column_index(i);
  }

 public:
  class iterator {
    friend class const_iterator;

   public:
    using iterator_category = std::forward_iterator_tag;
    using value_type = T;
    using difference_type = ptrdiff_t;

   private:
    double* ptr_;
    const std::array<qtens_len_t, Rank>& extent_;
    const std::array<qtens_len_t, Rank>& stride_;
    std::array<qtens_len_t, Rank> index_;

   public:
    iterator() : ptr_(nullptr), extent_{}, stride_{}, index_{} {}
    iterator(double* ptr, const std::array<qtens_len_t, Rank>& extents,
             const std::array<qtens_len_t, Rank>& stride,
             const std::array<qtens_len_t, Rank>& index = std::array<qtens_len_t, Rank>{}) :
        ptr_(ptr), extent_(extents), stride_(stride), index_(index)
    {}
    iterator(const iterator& other) :
        ptr_(other.ptr_), extent_(other.extent_), stride_(other.stride_), index_(other.index_)
    {}
    iterator(iterator&& other) :
        ptr_(other.ptr_), extent_(other.extent_), stride_(other.stride_), index_(other.index_)
    {}

    iterator& operator=(const iterator& other)
    {
      index_ = other.index_;
      return *this;
    }
    iterator& operator=(iterator&& other)
    {
      index_ = other.index_;
      return *this;
    }

    iterator& operator++()
    {
      index_[0]++;
      for (qtens_len_t i = 0; i < Rank - 1; ++i) {
        if (index_[i] >= extent_[i]) {
          index_[i] = 0;
          index_[i + 1]++;
        }
      }
      return *this;
    }

    iterator operator++(int)
    {
      iterator tmp = *this;
      ++(*this);
      return tmp;
    }

    bool operator==(const iterator& other) const
    {
      return (this->ptr_ == other.ptr_) && (this->index_ == other.index_);
    }

    bool operator!=(const iterator& other) const
    {
      return !((this->ptr_ == other.ptr_) && (this->index_ == other.index_));
    }

    T& operator*()
    {
      ptrdiff_t offset = 0;
      for (qtens_len_t i = 0; i < Rank; ++i) {
        offset += index_[i] * stride_[i];
      }
      return *(ptr_ + offset);
    }

    T& operator*() const
    {
      ptrdiff_t offset = 0;
      for (qtens_len_t i = 0; i < Rank; ++i) {
        offset += index_[i] * stride_[i];
      }
      return *(ptr_ + offset);
    }
  };
  static_assert(std::forward_iterator<iterator>, "iterator must be an input iterator");

  iterator begin() { return iterator(data_impl(), extent_, stride_); }
  iterator end()
  {
    std::array<qtens_len_t, Rank> last{};
    last[Rank - 1] = extent_[Rank - 1];
    return iterator(data_impl(), extent_, stride_, last);
  }

  class const_iterator {
   public:
    using iterator_category = std::forward_iterator_tag;
    using value_type = T;
    using difference_type = ptrdiff_t;

   private:
    const double* ptr_;
    const std::array<qtens_len_t, Rank>& extent_;
    const std::array<qtens_len_t, Rank>& stride_;
    std::array<qtens_len_t, Rank> index_;

   public:
    const_iterator() : ptr_(nullptr), extent_{}, stride_{}, index_{} {}
    const_iterator(const double* ptr, const std::array<qtens_len_t, Rank>& extents,
                   const std::array<qtens_len_t, Rank>& stride,
                   const std::array<qtens_len_t, Rank>& index = std::array<qtens_len_t, Rank>{}) :
        ptr_(ptr), extent_(extents), stride_(stride), index_(index)
    {}
    const_iterator(const const_iterator& other) :
        ptr_(other.ptr_), extent_(other.extent_), stride_(other.stride_), index_(other.index_)
    {}
    const_iterator(const_iterator&& other) :
        ptr_(other.ptr_), extent_(other.extent_), stride_(other.stride_), index_(other.index_)
    {}

    const_iterator& operator=(const const_iterator& other)
    {
      index_ = other.index_;
      return *this;
    }
    const_iterator& operator=(const_iterator&& other)
    {
      index_ = other.index_;
      return *this;
    }

    const_iterator& operator++()
    {
      index_[0]++;
      for (qtens_len_t i = 0; i < Rank - 1; ++i) {
        if (index_[i] >= extent_[i]) {
          index_[i] = 0;
          index_[i + 1]++;
        }
      }
      return *this;
    }

    const_iterator operator++(int)
    {
      const_iterator tmp = *this;
      ++(*this);
      return tmp;
    }

    bool operator==(const const_iterator& other) const
    {
      return (this->ptr_ == other.ptr_) && (this->index_ == other.index_);
    }

    bool operator!=(const const_iterator& other) const
    {
      return !((this->ptr_ == other.ptr_) && (this->index_ == other.index_));
    }

    const T& operator*()
    {
      ptrdiff_t offset = 0;
      for (qtens_len_t i = 0; i < Rank; ++i) {
        offset += index_[i] * stride_[i];
      }
      return *(ptr_ + offset);
    }

    const T& operator*() const
    {
      ptrdiff_t offset = 0;
      for (qtens_len_t i = 0; i < Rank; ++i) {
        offset += index_[i] * stride_[i];
      }
      return *(ptr_ + offset);
    }
  };
  static_assert(std::forward_iterator<const_iterator>, "const_iterator must be an input iterator");

  const_iterator begin() const { return const_iterator(data_impl(), extent_, stride_); }
  const_iterator cbegin() const { return const_iterator(data_impl(), extent_, stride_); }
  const_iterator end() const
  {
    std::array<qtens_len_t, Rank> last{};
    last[Rank - 1] = extent_[Rank - 1];
    return const_iterator(data_impl(), extent_, stride_, last);
  }
  const_iterator cend() const
  {
    std::array<qtens_len_t, Rank> last{};
    last[Rank - 1] = extent_[Rank - 1];
    return const_iterator(data_impl(), extent_, stride_, last);
  }

  template <typename... Args>
    requires(are_size_worthy_v<Args...>)
  TensorBase_(const Args&... args) :
      extent_(make_extent_(args...)),
      size_(compute_size_(args...)),
      stride_(compute_stride_(extent_))
  {
    static_assert(sizeof...(args) == Rank, "Tensor rank and constructor arg list don't match.");
  }

  TensorBase_(const std::array<qtens_len_t, Rank>& extent) :
      extent_(extent), stride_(compute_stride_(extent)), size_(compute_size_(extent))
  {}

  TensorBase_(const std::array<qtens_len_t, Rank>& extent,
              const std::array<qtens_len_t, Rank>& stride, const qtens_len_t& size) :
      extent_(extent), size_(size), stride_(stride)
  {}

  template <typename... Args>
    requires(are_size_worthy_v<Args...>)
  T& operator()(const Args&... args)
  {
    static_assert(sizeof...(args) == Rank);
    return data_impl(this->compute_index_(args...));
  }

  template <typename... Args>
    requires(are_size_worthy_v<Args...>)
  const T& operator()(const Args&... args) const
  {
    static_assert(sizeof...(args) == Rank);
    return data_impl(this->compute_index_(args...));
  }

  T& operator()(const std::array<qtens_len_t, Rank>& i)
  {
    return data_impl(this->compute_index_(i));
  }

  const T& operator()(const std::array<qtens_len_t, Rank>& i) const
  {
    return data_impl(this->compute_index_(i));
  }

  template <typename... Args>
    requires(are_size_worthy_v<Args...>)
  T& at(const Args&... args)
  {
    static_assert(sizeof...(args) == Rank);
    assert(check_bounds_(args...));
    return operator()(args...);
  }

  template <typename... Args>
    requires(are_size_worthy_v<Args...>)
  const T& at(const Args&... args) const
  {
    static_assert(sizeof...(args) == Rank);
    assert(check_bounds_(args...));
    return operator()(args...);
  }

  T& at(const std::array<qtens_len_t, Rank>& i)
  {
    assert(check_bounds_(i));
    return operator()(i);
  }
  const T& at(const std::array<qtens_len_t, Rank>& i) const
  {
    assert(check_bounds_(i));
    return operator()(i);
  }

  const std::array<qtens_len_t, Rank>& shape() const { return extent_; }
  qtens_len_t extent(const int i) const { return extent_[i]; }

  qtens_len_t size() const { return size_; }

  qtens_len_t ncolumns() const
  {
    // hoping that this compiles down to something simple
    qtens_len_t out = 1;
    for (qtens_len_t i = 1; i < Rank; ++i) {
      out *= extent_[i];
    }
    return out;
  }

#ifdef __cpp_lib_span
  template <size_t N, typename... Args>
  std::span<T, N> get_column(const Args&... args)
  {
    static_assert(sizeof...(args) + 1 == Rank, "get_column expects N-1 arguments");
    assert(this->extent(0) == N);
    T* start = static_cast<Derived*>(this)->data() + this->compute_index_(0, args...);
    return std::span<T, N>(start, N);
  }

  template <size_t N, typename... Args>
  const std::span<const T, N> get_const_column(const Args&... args)
  {
    static_assert(sizeof...(args) + 1 == Rank, "get_const_column expects N-1 arguments");
    assert(this->extent(0) == N);
    const T* const start =
        static_cast<const Derived*>(this)->data() + this->compute_index_(0, args...);
    return std::span<const T, N>(start, N);
  }
#endif // __cpp_lib_span

  qtens_len_t nrows() const { return extent_[0]; }

  const std::array<qtens_len_t, Rank>& stride() const { return stride_; }
  qtens_len_t stride(const int i) const { return stride_[i]; }

  static constexpr int rank() { return Rank; }

  template <typename Output>
  void print_tensor(Output& ccout, const std::string& name, const qtens_len_t n,
                    const bool compact = false) const
  {
    const int w = compact ? 10 : 16;
    const int p = compact ? 1 : 8;
    ccout << " --- " << name << ": [";
    bool first_element = true;
    for (auto& s : extent_) {
      if (!first_element) {
        ccout << ", ";
      }
      ccout << s;
      first_element = false;
    }
    ccout << "] ---" << std::endl;
    if constexpr (Rank == 1) { // flat printing for 1D tensor
      for (qtens_len_t i = 0; i < this->extent(0); ++i) {
        ccout << std::setw(w) << std::setprecision(p) << this->at(i);
      }
      ccout << std::endl;
    } else if constexpr (Rank == 2) { // special printing pattern for 2D tensor
      for (qtens_len_t i = 0; i < this->extent(0); ++i) {
        for (qtens_len_t j = 0; j < this->extent(1); ++j) {
          ccout << std::setw(w) << std::setprecision(p) << this->at(i, j);
        }
        ccout << std::endl;
      }
    } else {
      for (qtens_len_t i = 0; i < size(); ++i) {
        if (!compact) {
          ccout << std::setw(w) << std::setprecision(p);
        }
        ccout << data_impl(i) << std::endl;
      }
    }
  }

  virtual void print(std::string name, const qtens_len_t n = 0, const bool compact = false) const
  {
    print_tensor(std::cout, name, n, compact);
  }

 protected:
  /// compute the norm assuming a contiguous layout
  double _norm_contiguous() const
  {
    return std::sqrt(detail::real(qleve::dot_product(size(), data_impl(), data_impl())));
  }

  /// compute the norm assuming a columnwise layout
  double _norm_columnwise() const
  {
    double sum = 0.0;
    const auto ncol = this->ncolumns();
    const auto n = this->extent(0);
    for (qtens_len_t i = 0; i < ncol; ++i) {
      const T* d = data_impl() + column_index_impl(i);
      sum += detail::real(qleve::dot_product(n, d, d));
    }
    return std::sqrt(sum);
  }

  /// compute the norm assuming nothing about the layout
  double _norm_strided() const
  {
    double sum = 0.0;
    shape_range_<Rank> sr(extent_);
    for (auto i : sr) {
      sum += qleve::detail::real(qleve::detail::conj(at(i)) * at(i));
    }
    return std::sqrt(sum);
  }

 private:
  template <int pos, typename... Tail>
  inline void fill_extent_(std::array<qtens_len_t, Rank>& extent, const qtens_len_t& head,
                           const Tail&... tail) const
  {
    extent[pos] = head;
    fill_extent_<pos + 1>(extent, tail...);
  }

  template <int pos>
  inline void fill_extent_(std::array<qtens_len_t, Rank>& extent, const qtens_len_t& tail) const
  {
    extent[pos] = tail;
    static_assert(pos + 1 == Rank, "Improper number of arguments in constructor");
  }

  template <int pos, typename... Tail>
  inline qtens_len_t compute_index_(const qtens_len_t& head, const Tail&... tail) const
  {
    return head * stride_[pos] + compute_index_<pos + 1>(tail...);
  }

  template <int pos>
  inline qtens_len_t compute_index_(const qtens_len_t& tail) const
  {
    return tail * stride_[pos];
  }

  template <int pos, typename... Tail>
  inline bool check_bounds_(const qtens_len_t& head, const Tail&... tail) const
  {
    return (head >= 0 && head < std::get<pos>(extent_)) && check_bounds_<pos + 1>(tail...);
  }

  template <int pos>
  inline bool check_bounds_(const qtens_len_t& tail) const
  {
    return (tail >= 0 && tail < std::get<pos>(extent_));
  }
};

} // namespace qleve

#endif // QLEVE_TENSOR_BASE_HPP
