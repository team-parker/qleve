#include <complex>

#include "catch.hpp"

#include <qleve/tensor_traits.hpp>

using namespace std;
using namespace qleve;

TEST_CASE("Tensor Traits", "[traits]")
{

  SECTION("has_shape")
  {
    struct with_shape {
      int shape() { return 0; }
    };
    with_shape sh;
    auto shape = sh.shape();
    INFO("dummy shape returns " << shape);
    REQUIRE(has_shape<with_shape>::value);

    struct no_shape {};
    REQUIRE_FALSE(has_shape<no_shape>::value);
  }

  SECTION("has_size")
  {
    struct with_size {
      qtens_len_t size() const { return 0; }
    };
    with_size ss;
    auto size = ss.size();
    INFO("dummy size returns " << size);
    REQUIRE(has_size<with_size>::value);

    struct no_size {};
    REQUIRE_FALSE(has_size<no_size>::value);
  }

  SECTION("has_nonconst_data_operator")
  {
    struct with_data {
      double* data() { return nullptr; }
    };
    with_data wd;
    auto val = wd.data();
    INFO("dummy data returns " << val);
    REQUIRE(has_nonconst_data_operator<with_data, double>::value);
    REQUIRE_FALSE(has_nonconst_data_operator<with_data, float>::value);

    struct no_data {};
    REQUIRE_FALSE(has_nonconst_data_operator<no_data, double>::value);
  }

  SECTION("has_const_data_operator")
  {
    struct with_data {
      const double* data() const { return nullptr; }
    };
    const with_data wd;
    auto data = wd.data();
    INFO("dummy data returns " << data);
    REQUIRE(has_const_data_operator<with_data, double>::value);
    REQUIRE_FALSE(has_const_data_operator<with_data, float>::value);

    struct no_data {};
    REQUIRE_FALSE(has_const_data_operator<no_data, double>::value);
  }

  SECTION("has_nonconst_access_operator")
  {
    struct with_access {
      double a;
      with_access(double A) : a(A) {}
      double& data(qtens_len_t) { return a; }
    };
    with_access wa(1.0);
    auto val = wa.data(0);
    INFO("dummy data returns " << val);
    REQUIRE(has_nonconst_access_operator<with_access, double>::value);
    REQUIRE_FALSE(has_nonconst_access_operator<with_access, float>::value);

    struct no_access {};
    REQUIRE_FALSE(has_nonconst_access_operator<no_access, double>::value);
  }

  SECTION("has_const_access_operator")
  {
    struct with_access {
      const double a;
      with_access(double A) : a(A) {}
      const double& data(qtens_len_t) const { return a; }
    };
    const with_access wa(1.0);
    auto val = wa.data(0);
    INFO("dummy data returns " << val);
    REQUIRE(has_const_access_operator<with_access, double>::value);
    REQUIRE_FALSE(has_const_access_operator<with_access, float>::value);

    struct no_access {};
    REQUIRE_FALSE(has_const_access_operator<no_access, double>::value);
  }

  SECTION("is_floating_coefficient")
  {
    REQUIRE(is_floating_coefficient<float>::value);
    REQUIRE(is_floating_coefficient<double>::value);
    REQUIRE(is_floating_coefficient<long double>::value);

    REQUIRE(is_floating_coefficient<std::complex<float>>::value);
    REQUIRE(is_floating_coefficient<std::complex<double>>::value);
    REQUIRE(is_floating_coefficient<std::complex<long double>>::value);

    REQUIRE_FALSE(is_floating_coefficient<int>::value);
    REQUIRE_FALSE(is_floating_coefficient<qtens_len_t>::value);
    REQUIRE_FALSE(is_floating_coefficient<bool>::value);
  }
}
