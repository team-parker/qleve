// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file include/qleve/trait_util.hpp
///
/// A brief description of the contents of this file

#ifndef QLEVE_TRAIT_UTIL_HPP
#define QLEVE_TRAIT_UTIL_HPP

#include <type_traits>

namespace qleve
{
template <bool... Bs>
using bool_sequence = std::integer_sequence<bool, Bs...>;

template <bool... Bs>
using bool_and = std::is_same<bool_sequence<Bs...>, bool_sequence<(Bs || true)...>>;

template <bool... Bs>
using bool_or = std::integral_constant<bool, !bool_and<!Bs...>::value>;

template <typename T, typename... Ts>
using is_any = bool_or<std::is_same<T, Ts>::value...>;

template <typename T, T... Is>
constexpr auto integer_sequence_reverse(std::integer_sequence<T, Is...> const&)
    -> decltype(std::integer_sequence<T, sizeof...(Is) - 1 - Is...>{});

template <typename T, int N>
using make_reversed_integer_sequence =
    decltype(integer_sequence_reverse(std::make_integer_sequence<T, N>{}));

} // namespace qleve

#endif // QLEVE_TRAIT_UTIL_HPP
