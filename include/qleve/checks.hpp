// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file include/qleve/checks.hpp
///
/// Routines to verify properties of matrices

#ifndef QLEVE_CHECKS_HPP
#define QLEVE_CHECKS_HPP

#include <tuple>

#include <qleve/blas_interface.hpp>
#include <qleve/matrix_multiply.hpp>
#include <qleve/pod_types.hpp>
#include <qleve/tensor_traits.hpp>

namespace qleve::linalg
{

double diagonality(const const_matrixable auto& A)
{
  double out = 0.0;
  const auto [ndim, mdim] = A.shape();
  for (qtens_len_t j = 0; j < mdim; ++j) {
    for (qtens_len_t i = 0; i < ndim; ++i) {
      if (i == j)
        continue;
      out += std::abs(A(i, j)) * std::abs(A(i, j));
    }
  }
  out = std::sqrt(out);
  return out;
}


bool is_diagonal(const const_matrixable auto& A, const double thresh = 1.0e-12)
{
  return diagonality(A) < thresh;
}


double hermiticity(const const_matrixable auto& A)
{
  double out = 0.0;

  const auto [n, m] = A.shape();
  assert(n == m);

  for (ptrdiff_t j = 0; j < m; ++j) {
    for (ptrdiff_t i = 0; i < n; ++i) {
      const auto tmp = A(i, j) - std::conj(A(j, i));
      out += std::abs(tmp) * std::abs(tmp);
    }
  }

  return std::sqrt(out);
}


bool is_hermitian(const const_matrixable auto& A, const double thr = 1.0e-8)
{
  return hermiticity(A) < thr;
}


double idempotency(const const_matrixable auto& A)
{
  const auto [n, m] = A.shape();
  assert(n == m);

  auto tmp = qleve::gemm("n", "n", 1.0, A, A);
  tmp -= A;
  return tmp.norm();
}


double idempotency_with_metric(const const_matrixable auto& A, const const_matrixable auto& S)
{
  const auto [n, m] = A.shape();
  assert(n == m && S.extent(0) == n && S.extent(1) == m);

  auto AS = qleve::gemm("n", "n", 1.0, A, S);
  auto tmp = qleve::gemm("n", "n", 1.0, AS, A);
  tmp -= A;
  return tmp.norm();
}


bool is_idempotent(const const_matrixable auto& A, const double thr = 1.0e-8)
{
  return idempotency(A) < thr;
}


bool is_idempotent_with_metric(const const_matrixable auto& A, const const_matrixable auto& S,
                               const double thr = 1.0e-8)
{
  return idempotency_with_metric(A, S) < thr;
}


double ident(const const_matrixable auto& A)
{
  double out = 0.0;

  const auto [n, m] = A.shape();
  assert(n == m);

  for (ptrdiff_t j = 0; j < m; ++j) {
    for (ptrdiff_t i = 0; i < j; ++i) {
      const auto tmp = A(i, j);
      out += std::abs(tmp) * std::abs(tmp);
    }
    const auto tmp = A(j, j) - 1.0;
    out += std::abs(tmp) * std::abs(tmp);
  }

  return std::sqrt(out);
}


bool is_identity(const const_matrixable auto& A, const double thr = 1.0e-8)
{
  return ident(A) < thr;
}

} // namespace qleve::linalg

#endif // QLEVE_CHECKS_HPP
