// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file include/qleve/matrix_transform.hpp
///
/// Generic interface to common transformation routines:
///   C <- alpha * B^\dagger A B + beta * C
/// for alpha, beta constants and A, B matrices

#ifndef QLEVE_MATRIX_TRANSFORM_HPP
#define QLEVE_MATRIX_TRANSFORM_HPP

#include <algorithm>
#include <utility>

#include <qleve/blas_interface.hpp>
#include <qleve/matrix_multiply.hpp>
#include <qleve/tensor.hpp>

namespace qleve
{

/// C <- alpha * B^T A B + beta * C
///
/// optionally use provided temporary storage

template <const_matrixable MatTypeA, const_matrixable MatTypeB, matrixable MatTypeC, typename T,
          typename U>
  requires(have_same_datatype_v<MatTypeA, MatTypeB, MatTypeC>
           && are_convertible_scalars_v<MatTypeA, T, U>)
void matrix_transform(const T alpha, const MatTypeA& A, const MatTypeB& B, const U beta,
                      MatTypeC&& C, T* scratch = nullptr)
{
  assert(A.extent(0) == A.extent(1));
  assert(A.extent(1) == B.extent(0));
  assert(A.extent(0) == B.extent(0));

  std::unique_ptr<T[]> storage;
  if (scratch == nullptr) {
    storage = std::unique_ptr<T[]>(new T[A.extent(0) * B.extent(1)]);
    scratch = storage.get();
  }

  using MatViewType = TensorView_<T, 2>;
  MatViewType scratchview(scratch, A.extent(0), B.extent(1));

  gemm("n", "n", 1.0, A, B, 0.0, scratchview);
  gemm("t", "n", alpha, B, scratchview, beta, C);
}

template <const_matrixable MatTypeA, const_matrixable MatTypeB, matrixable MatTypeC, typename T,
          typename U>
  requires(have_same_datatype_v<MatTypeA, MatTypeB, MatTypeC>
           && are_convertible_scalars_v<MatTypeA, T, U>)
void matrix_transform(const char* tr, const T alpha, const MatTypeA& A, const MatTypeB& B,
                      const U beta, MatTypeC&& C, T* scratch = nullptr)
{
  assert(A.extent(0) == A.extent(1));

  const auto [trans, c] = matrix_trans_conj(tr);

  const ptrdiff_t innerdim = A.extent(1);
  const ptrdiff_t outerdim = trans ? B.extent(0) : B.extent(1);

  assert(innerdim == (trans ? B.extent(1) : B.extent(0)));

  std::unique_ptr<T[]> storage;
  if (scratch == nullptr) {
    storage = std::unique_ptr<T[]>(new T[innerdim * outerdim]);
    scratch = storage.get();
  }

  using MatViewType = TensorView_<T, 2>;
  MatViewType scratchview(scratch, innerdim, outerdim);

  const char* trl = trans ? "n" : "t";

  gemm("n", tr, 1.0, A, B, 0.0, scratchview);
  gemm(trl, "n", alpha, B, scratchview, beta, C);
}

/// C <- alpha * L^T A R + beta * C
template <const_matrixable MatTypeL, const_matrixable MatTypeA, const_matrixable MatTypeR,
          matrixable MatTypeC, typename T, typename U>
  requires(have_same_datatype_v<MatTypeL, MatTypeA, MatTypeR, MatTypeC>
           && are_convertible_scalars_v<MatTypeA, T, U>)
void matrix_transform(const T alpha, const MatTypeL& left, const MatTypeA& A, const MatTypeR& right,
                      const U beta, MatTypeC&& C, T* scratch = nullptr)
{
  assert(left.extent(1) == C.extent(0));
  assert(right.extent(1) == C.extent(1));
  assert(left.extent(0) == A.extent(0));
  assert(A.extent(1) == right.extent(0));

  // if L and R are different sizes, contract the smaller one first
  const bool contract_left = (right.extent(1) > left.extent(1));

  // dimensions of the intermediate
  const ptrdiff_t ndim = contract_left ? left.extent(1) : A.extent(0);
  const ptrdiff_t mdim = contract_left ? A.extent(1) : right.extent(1);

  using MatType = Tensor_<T, 2>;
  std::unique_ptr<MatType> storage;
  if (scratch == nullptr) {
    storage = std::make_unique<MatType>(ndim, mdim);
    scratch = storage->data();
  }

  using MatViewType = TensorView_<T, 2>;
  MatViewType scratchview(scratch, ndim, mdim);

  if (contract_left) {
    qleve::gemm("t", "n", 1.0, left, A, 0.0, scratchview);
    qleve::gemm("n", "n", alpha, scratchview, right, beta, C);
  } else {
    qleve::gemm("n", "n", 1.0, A, right, 0.0, scratchview);
    qleve::gemm("t", "n", alpha, left, scratchview, beta, C);
  }
}

/// C <- alpha * L^op A R^op + beta * C
///
/// most general version of matrix_transform where L and R can take any operation
/// and don't need to be the same shape
template <const_matrixable MatTypeL, const_matrixable MatTypeA, const_matrixable MatTypeR,
          matrixable MatTypeC, typename T, typename U>
  requires(have_same_datatype_v<MatTypeL, MatTypeA, MatTypeR, MatTypeC>
           && are_convertible_scalars_v<MatTypeA, T, U>)
void matrix_transform(const char* trL, const char* trR, const T alpha, const MatTypeL& left,
                      const MatTypeA& A, const MatTypeR& right, const U beta, MatTypeC&& C,
                      T* scratch = nullptr)
{
  const auto [transL, cL] = matrix_trans_conj(trL);
  const auto [transR, cR] = matrix_trans_conj(trR);

  // dimensions of the output matrix
  const ptrdiff_t idim = transL ? left.extent(1) : left.extent(0);
  const ptrdiff_t jdim = transR ? right.extent(0) : right.extent(1);

  assert(idim == C.extent(0));
  assert(jdim == C.extent(1));

  // dimensions of the contracted indices
  const ptrdiff_t innerdimL = transL ? left.extent(0) : left.extent(1);
  const ptrdiff_t innerdimR = transR ? right.extent(1) : right.extent(0);

  assert(innerdimL == A.extent(0));
  assert(innerdimR == A.extent(1));

  // if L and R are different sizes, contract the smaller one first
  const bool contract_left = (idim > jdim);

  // dimensions of the intermediate
  const ptrdiff_t ndim = contract_left ? idim : innerdimL;
  const ptrdiff_t mdim = contract_left ? innerdimR : jdim;

  using MatType = Tensor_<T, 2>;
  std::unique_ptr<MatType> storage;
  if (scratch == nullptr) {
    storage = std::make_unique<MatType>(ndim, mdim);
    scratch = storage->data();
  }

  using MatViewType = TensorView_<T, 2>;
  MatViewType scratchview(scratch, ndim, mdim);

  if (contract_left) {
    qleve::gemm(trL, "n", 1.0, left, A, 0.0, scratchview);
    qleve::gemm("n", trR, alpha, scratchview, right, beta, C);
  } else {
    qleve::gemm("n", trR, 1.0, A, right, 0.0, scratchview);
    qleve::gemm(trL, "n", alpha, left, scratchview, beta, C);
  }
}

/// C^k <- alpha * op(L^k) A^k op(R^k) + beta * C^k
///
/// most general version of a stacked matrix_transform where L, A, and R are 3 index tensors L and R
/// can take any operation and don't need to be the same shape
template <const_tensor3able T3L, const_tensor3able T3A, const_tensor3able T3R, tensor3able T3C,
          typename T, typename U>
  requires(have_same_datatype_v<T3L, T3A, T3R, T3C> && are_convertible_scalars_v<T3A, T, U>)
void matrix_transform(const char* trL, const char* trR, const T alpha, const T3L& left,
                      const T3A& A, const T3R& right, const U beta, T3C&& C, T* scratch = nullptr)
{
  const auto [transL, cL] = matrix_trans_conj(trL);
  const auto [transR, cR] = matrix_trans_conj(trR);

  const ptrdiff_t kdim = A.extent(2);
  assert(kdim == left.extent(2) && kdim == right.extent(2) && kdim == C.extent(2));

  // dimensions of the output matrix
  const ptrdiff_t idim = transL ? left.extent(1) : left.extent(0);
  const ptrdiff_t jdim = transR ? right.extent(0) : right.extent(1);

  assert(idim == C.extent(0));
  assert(jdim == C.extent(1));

  // dimensions of the contracted indices
  const ptrdiff_t innerdimL = transL ? left.extent(0) : left.extent(1);
  const ptrdiff_t innerdimR = transR ? right.extent(1) : right.extent(0);

  assert(innerdimL == A.extent(0));
  assert(innerdimR == A.extent(1));

  // if L and R are different sizes, contract the smaller one first
  const bool contract_left = (idim > jdim);

  // dimensions of the intermediate
  const ptrdiff_t ndim = contract_left ? idim : innerdimL;
  const ptrdiff_t mdim = contract_left ? innerdimR : jdim;

  using MatType = Tensor_<T, 2>;
  std::unique_ptr<MatType> storage;
  if (scratch == nullptr) {
    storage = std::make_unique<MatType>(ndim, mdim);
    scratch = storage->data();
  }

  using MatViewType = TensorView_<T, 2>;
  MatViewType scratchview(scratch, ndim, mdim);

  for (ptrdiff_t k = 0; k < kdim; ++k) {
    if (contract_left) {
      qleve::gemm(trL, "n", 1.0, left.const_pluck(k), A.const_pluck(k), 0.0, scratchview);
      qleve::gemm("n", trR, alpha, scratchview, right.const_pluck(k), beta, C.pluck(k));
    } else {
      qleve::gemm("n", trR, 1.0, A.const_pluck(k), right.const_pluck(k), 0.0, scratchview);
      qleve::gemm(trL, "n", alpha, left.const_pluck(k), scratchview, beta, C.pluck(k));
    }
  }
}

} // namespace qleve

#endif // QLEVE_MATRIX_TRANSFORM_HPP
