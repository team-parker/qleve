// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file include/qleve/tensor_dot.hpp
///
/// Interface for tensor dot products

#ifndef QLEVE_TENSOR_DOT_HPP
#define QLEVE_TENSOR_DOT_HPP

#include <algorithm>
#include <complex>
#include <utility>

#include <qleve/pod_types.hpp>
#include <qleve/tblis_interface.hpp>
#include <qleve/tensor_indices.hpp>
#include <qleve/tensor_traits.hpp>

namespace qleve
{

template <const_strided_tensorable TensorA, const_strided_tensorable TensorB>
auto dot_product(const bool conjA, const bool conjB, const TensorA& A, indices<TensorA> indA,
                 const TensorB& B, indices<TensorB> indB, const bool& single = false)
{
  using dot_t = decltype(tensor_datatype<TensorA>() * tensor_datatype<TensorB>());

  const char* labelsA = indA.c_str();
  const char* labelsB = indB.c_str();

  tblis_const_tensor_wrap<TensorA> tensorA(conjA, 1.0, A);
  tblis_const_tensor_wrap<TensorB> tensorB(conjB, 1.0, B);

  const tblis_comm* comm = single ? tblis_single : nullptr;

  tblis::tblis_scalar result;

  tblis::tblis_tensor_dot(comm, nullptr, tensorA.tblis_tensor(), labelsA, tensorB.tblis_tensor(),
                          labelsB, &result);

  return result.get<dot_t>();
}

template <const_strided_tensorable TensorA, const_strided_tensorable TensorB>
auto dot_product(const TensorA& A, indices<TensorA> indA, const TensorB& B, indices<TensorB> indB,
                 const bool& single = false)
{
  return dot_product(false, false, A, indA, B, indB, single);
}

template <const_strided_tensorable TensorA, const_strided_tensorable TensorB>
auto dot_product(const TensorA& A, const TensorB& B, const bool& single = false)
{
  assert(A.shape() == B.shape());
  std::vector<char> abc(A.rank());
  std::iota(abc.begin(), abc.end(), 'a');
  std::string labels(abc.begin(), abc.begin() + abc.size());
  return dot_product(false, false, A, runtime(labels.c_str()), B, runtime(labels.c_str()), single);
}

} // namespace qleve

#endif // QLEVE_TENSOR_DOT_HPP
