/// \file test/util.hpp
//
//  This file is part of qleve.
//
//  Foobar is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  qleve is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with qleve.  If not, see <http://www.gnu.org/licenses/>.

#ifndef QLEVE_TEST_UTIL_HPP
#define QLEVE_TEST_UTIL_HPP

#include <cmath>
#include <complex>

#include <qleve/indexable.hpp>

namespace qleve
{

template <class A, class D, typename T>
bool indexable_equals(const Indexable<T, A, D>& indexable, const std::vector<T>& ref_result,
                      const double tol = 1.0e-8)
{
  if (indexable.isize() != ref_result.size())
    return false;

  for (size_t i = 0; i < indexable.isize(); ++i) {
    if (auto error = std::abs(indexable.data(i) - ref_result.at(i)); error > tol) {
      return false;
    }
  }

  return true;
}

} // namespace qleve

#endif // QLEVE_TEST_UTIL_HPP
