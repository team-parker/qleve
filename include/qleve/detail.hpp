// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file include/qleve/detail.hpp
///
/// Special functions to simplify detailed handling of complex vs real types

#ifndef QLEVE_DETAIL_HPP
#define QLEVE_DETAIL_HPP

#include <complex>

#include <qleve/pod_types.hpp>

namespace qleve
{
namespace detail
{
namespace
{
template <typename T>
T conj(const T& a)
{
  assert(false);
  return a;
}

template <>
double conj(const double& a)
{
  return a;
}

template <>
dcomplex conj(const dcomplex& a)
{
  return std::conj(a);
}

template <typename T>
double real(const T& a)
{
  assert(false);
  return a;
}

template <>
double real(const double& a)
{
  return a;
}

template <>
double real(const dcomplex& a)
{
  return a.real();
}

template <typename T>
double imag(const T& a)
{
  assert(false);
  return a;
}

template <>
double imag(const double& a)
{
  return 0.0;
}

template <>
double imag(const dcomplex& a)
{
  return a.imag();
}
} // namespace
} // namespace detail
} // namespace qleve

#endif // QLEVE_DETAIL_HPP
