################################################################################
#
# \file      cmake/FindTBLIS.cmake
# \author    Shane M. Parker
# \brief     Find the TBLIS library
#
################################################################################

# Find the TBLIS Library from Intel. If it is not found, use external project to add it
#
#  TBLIS_FOUND - System has TBLIS
#  TBLIS_INCLUDE_DIRS - TBLIS include files directories
#  TBLIS_LIBRARIES - The TBLIS libraries
#
#  The environment variable TBLISROOT is used as a hint.
#
#  Example usage:
#
#  find_package(TBLIS REQUIRED)
#  if(TARGET tblis::tblis)
#    target_link_libraries(TARGET tblis::tblis)
#  endif()

# If already in cache, be silent
if (TBLIS_FOUND AND TBLIS_INCLUDE_DIRS AND TBLIS_LIBRARIES)
  set (TBLIS_FIND_QUIETLY TRUE)
endif()

find_path(TBLIS_INCLUDE_DIR
  NAMES tblis/tblis.h
  PATHS $ENV{TBLISROOT}/include
        ${PROJECT_BINARY_DIR}/deps)

find_library(TCI_LIBRARY
  NAMES libtci${CMAKE_STATIC_LIBRARY_SUFFIX}
  PATHS $ENV{TBLISROOT}/lib
        ${PROJECT_BINARY_DIR}/deps)

find_library(TBLIS_LIBRARY
  NAMES libtblis${CMAKE_STATIC_LIBRARY_SUFFIX}
  PATHS $ENV{TBLISROOT}/lib
        ${PROJECT_BINARY_DIR}/deps)

if(NOT (TBLIS_INCLUDE_DIR AND TCI_LIBRARY AND TBLIS_LIBRARY) )
  message("Installed TBLIS not found. Downloading from github.com/devinamatthews/tblis")
  include(ExternalProject)
  ExternalProject_Add(tblis-download
    GIT_REPOSITORY https://github.com/devinamatthews/tblis
    GIT_TAG master
    PREFIX ${PROJECT_BINARY_DIR}/deps
    INSTALL_DIR ${PROJECT_BINARY_DIR}/deps/tblis
    #INSTALL_COMMAND "make install"
    CONFIGURE_COMMAND <SOURCE_DIR>/configure --prefix=<INSTALL_DIR> --libdir=<INSTALL_DIR>/lib --enable-thread-model=tbb --without-hwloc
    "CC=${CMAKE_C_COMPILER} -fopenmp" "CXX=${CMAKE_CXX_COMPILER} -fopenmp"
    UPDATE_COMMAND ""
  )

  ExternalProject_Get_Property(tblis-download INSTALL_DIR)
  set(TBLIS_DIR ${INSTALL_DIR})

  set(TBLIS_INCLUDE_DIR ${TBLIS_DIR}/include)
  set(TCI_LIBRARY ${TBLIS_DIR}/lib/libtci${CMAKE_STATIC_LIBRARY_SUFFIX})
  set(TBLIS_LIBRARY ${TBLIS_DIR}/lib/libtblis${CMAKE_STATIC_LIBRARY_SUFFIX})
endif()

set(TBLIS_LIBRARIES ${TCI_LIBRARY} ${TBLIS_LIBRARY})
add_library(tci STATIC IMPORTED)
set_target_properties(tci PROPERTIES IMPORTED_LOCATION ${TCI_LIBRARY})
add_library(tblis::tci ALIAS tci)

add_library(tblis STATIC IMPORTED)
set_target_properties(tblis PROPERTIES IMPORTED_LOCATION ${TBLIS_LIBRARY})
add_library(tblis::tblis ALIAS tblis)

# Some architectures want an explicit link to atomic. This is kind of ugly, but what can you do?
if ("${CMAKE_CXX_COMPILER_ID}" MATCHES "Clang")
  set(LATOMIC "")
elseif ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU")
  set(LATOMIC atomic)
elseif ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "Intel")
  set(LATOMIC "")
elseif ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "MSVC")
  set(LATOMIC "")
endif()

set(TBLIS_INCLUDE_DIRS ${TBLIS_INCLUDE_DIR})
target_link_libraries(tblis INTERFACE tci dl pthread OpenMP::OpenMP_CXX ${OpenMP_CXX_FLAGS} ${LATOMIC})
target_include_directories(tblis INTERFACE
  $<BUILD_INTERFACE:${TBLIS_INCLUDE_DIR}>
  $<INSTALL_INTERFACE:include>)

# Handle the QUIETLY and REQUIRED arguments and set MKL_FOUND to TRUE if
# all listed variables are TRUE.
include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(TBLIS DEFAULT_MSG TBLIS_LIBRARIES TBLIS_INCLUDE_DIRS)
mark_as_advanced(MKL_INCLUDE_DIRS MKL_LIBRARIES)
