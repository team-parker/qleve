// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file include/qleve/matrix_diag_multiply.hpp
///
/// Interface to matrix multiplies that also scale by a diagonal matrix.

#ifndef QLEVE_WEIGHTED_MATRIX_MULTIPLY_HPP
#define QLEVE_WEIGHTED_MATRIX_MULTIPLY_HPP

#include <algorithm>
#include <complex>
#include <utility>

#include <qleve/pod_types.hpp>
#include <qleve/tblis_interface.hpp>
#include <qleve/tensor_traits.hpp>

namespace qleve
{

namespace
{

template <const_matrixable MatTypeA, const_arrayable ArrayType, const_matrixable MatTypeB,
          matrixable MatTypeC, typename T, typename U>
  requires(have_same_datatype<MatTypeA, MatTypeB, MatTypeC, ArrayType>::value
           && are_convertible_scalars<MatTypeA, T, U>::value)
void weighted_gemm(const char* tra, const char* trb, const T& alpha, const MatTypeA& A,
                   const ArrayType& D, const MatTypeB& B, const U& beta, MatTypeC&& C,
                   const bool single = false)
{
  const bool transA = (*tra == 't' || *tra == 'T');
  const bool transB = (*trb == 't' || *trb == 'T');

  const bool conjA = (*tra == 'c' || *tra == 'C');
  const bool conjB = (*trb == 'c' || *trb == 'C');

  tblis_const_matrix_wrap<MatTypeA> matA(transA, conjA, alpha, A);
  tblis_const_matrix_wrap<MatTypeB> matB(transB, conjB, 1.0, B);
  tblis_const_vector_wrap<ArrayType> diag(false, 1.0, D);
  tblis_matrix_wrap<MatTypeC> matC(false, false, beta, C);

  const qtens_len_t ndim = transA ? A.extent(1) : A.extent(0);
  const qtens_len_t mdim = transB ? B.extent(0) : B.extent(1);
  const qtens_len_t kdim = transA ? A.extent(0) : A.extent(1);

  assert(ndim == C.extent(0));
  assert(mdim == C.extent(1));
  assert(kdim == (transB ? B.extent(1) : B.extent(0)));

  const tblis_comm* comm = single ? tblis_single : nullptr;

  tblis_matrix_mult_diag(comm, nullptr, matA.tblis_matrix(), diag.tblis_vector(),
                         matB.tblis_matrix(), matC.tblis_matrix());
}

} // namespace

} // namespace qleve

#endif // QLEVE_WEIGHTED_MATRIX_MULTIPLY_HPP
