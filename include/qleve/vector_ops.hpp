// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file include/qleve/vector_ops.hpp
///
/// Generalized interfaces for BLAS-like operations

#ifndef QLEVE_VECTOR_OPS_HPP
#define QLEVE_VECTOR_OPS_HPP

#include <complex>

#include <qleve/blas_interface.hpp>
#include <qleve/pod_types.hpp>

namespace qleve
{

/******************************************************************/
/*                        Sum functions                           */
/******************************************************************/
namespace
{
double asum(const qtens_len_t& n, const double* x, const qtens_len_t& incx)
{
  return dasum_(n, x, incx);
}
} // namespace

// short circuit for unit stride
template <typename T>
double asum(const qtens_len_t& n, const T* x)
{
  return asum(n, x, 1);
}

/******************************************************************/
/*                      Scale functions                           */
/******************************************************************/
namespace
{
void scale(const qtens_len_t& n, const double alpha, double* x, const qtens_len_t incx)
{
  dscal_(n, alpha, x, incx);
}
void scale(const qtens_len_t& n, const dcomplex alpha, dcomplex* x, const qtens_len_t incx)
{
  zscal_(n, alpha, x, incx);
}
} // namespace

// short circuit for unit stride
template <typename ScalarType, typename T>
  requires(std::is_convertible_v<ScalarType, T>)
void scale(const qtens_len_t& n, const ScalarType alpha, T* x)
{
  scale(n, alpha, x, 1);
}

/******************************************************************/
/*                       Y = a X + Y                              */
/******************************************************************/
namespace
{
void ax_plus_y(const qtens_len_t& n, const double a, const double* x, const qtens_len_t incx,
               double* y, const qtens_len_t incy)
{
  daxpy_(n, a, x, incx, y, incy);
}

void ax_plus_y(const qtens_len_t& n, const dcomplex a, const dcomplex* x, const qtens_len_t incx,
               dcomplex* y, const qtens_len_t incy)
{
  zaxpy_(n, a, x, incx, y, incy);
}
} // namespace

// short circuit for unit stride
template <typename ScalarType, typename T>
  requires(std::is_convertible_v<ScalarType, T>)
void ax_plus_y(const qtens_len_t& n, const ScalarType a, const T* x, T* y)
{
  ax_plus_y(n, a, x, 1, y, 1);
}

/******************************************************************/
/*                       inner products                           */
/******************************************************************/
namespace
{
double dot_product(const qtens_len_t n, const double* x, const qtens_len_t incx, const double* y,
                   const qtens_len_t incy)
{
  return ddot_(n, x, incx, y, incy);
}
dcomplex dot_product(const qtens_len_t n, const dcomplex* x, const qtens_len_t incx,
                     const dcomplex* y, const qtens_len_t incy)
{
  return zdotc_(n, x, incx, y, incy);
}

} // namespace

template <typename T>
  requires(is_floating_coefficient_v<T>)
T dot_product(const qtens_len_t n, const T* x, const T* y)
{
  return dot_product(n, x, 1, y, 1);
}

} // namespace qleve

#endif // QLEVE_VECTOR_OPS_HPP
