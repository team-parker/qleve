// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file include/qleve/layout.hpp
///
/// Type traits for layout in indexables

#ifndef QLEVE_LAYOUT_HPP
#define QLEVE_LAYOUT_HPP

#include <cassert>
#include <type_traits>

namespace qleve
{

/// Trait for contiguous layout, i.e., all elements are in one contiguous block in memory
struct ContiguousLayout {};

/// Trait for columnwise layout, i.e., columns are stored contiguously (stride 1)
/// but columns are non-contiguous
struct ColumnwiseLayout {};

/// Trait for strided layout, i.e., elements are separated by non-1 stride
struct StridedLayout {};

/// Trait for no layout, i.e., no layout is defined
struct NoLayout {};

/// Compose layouts table
///
///   T + U -> TU
///   C = contiguous, CW = columnwise, S = strided
///
///   |    | C  | CW | S |
///   |----|----|----|---|
///   | C  | C  | CW | S |
///   | CW | CW | CW | S |
///   | S  | S  | S  | S |

template <class T, class U>
struct compose_layout {};

template <class T, class U>
using compose_layout_t = typename compose_layout<T, U>::type;

/// Compose layouts implementation:
///   contiguous + contiguous -> contiguous
///   columnwise + columnwise -> columnwise
///   strided + strided -> strided
template <class T, class U>
  requires std::is_same_v<T, U>
struct compose_layout<T, U> {
  using type = T;
};

/// Compose layouts implementation:
///   columnwise + contiguous -> columnwise
template <>
struct compose_layout<ColumnwiseLayout, ContiguousLayout> {
  using type = ColumnwiseLayout;
};

/// Compose layouts implementation:
///   contiguous + columnwise -> columnwise
template <>
struct compose_layout<ContiguousLayout, ColumnwiseLayout> {
  using type = ColumnwiseLayout;
};

/// Compose layouts implementation:
///   strided + contiguous -> strided
template <>
struct compose_layout<StridedLayout, ContiguousLayout> {
  using type = StridedLayout;
};

/// Compose layouts implementation:
///   contiguous + strided -> strided
template <>
struct compose_layout<ContiguousLayout, StridedLayout> {
  using type = StridedLayout;
};

/// Compose layouts implementation:
///   strided + columnwise -> strided
template <>
struct compose_layout<StridedLayout, ColumnwiseLayout> {
  using type = StridedLayout;
};

/// Compose layouts implementation:
///   columnwise + strided -> strided
template <>
struct compose_layout<ColumnwiseLayout, StridedLayout> {
  using type = StridedLayout;
};

/// check whether a class has a Layout typedef
template <class T, typename = void>
struct has_layout : std::false_type {};

template <class T>
struct has_layout<T, std::void_t<typename std::remove_reference_t<T>::Layout>> : std::true_type {};

template <class T>
concept laidout = has_layout<T>::value;

template <class T, typename U = void>
struct layout {
  using type = NoLayout;
};

/// specialization for if T has a Layout typedef
template <class T>
  requires has_layout<T>::value
struct layout<T> {
  using type = typename std::remove_reference_t<T>::Layout;
};

template <class T>
using layout_t = typename layout<T>::type;

} // namespace qleve

#endif // QLEVE_LAYOUT_HPP
