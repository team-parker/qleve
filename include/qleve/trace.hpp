// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file include/qleve/trace.hpp
///
/// Generic interface to matrix trace

#ifndef QLEVE_TRACE_HPP
#define QLEVE_TRACE_HPP

#include <tuple>

#include <qleve/blas_interface.hpp>
#include <qleve/pod_types.hpp>
#include <qleve/tensor_traits.hpp>

namespace qleve::linalg
{

auto trace(const const_matrixable auto& A)
{
  const auto [ndim, mdim] = A.shape();
  const ptrdiff_t ij = std::min(ndim, mdim);

  using T = decltype(A.data(0));
  auto out = T{0.0};
  for (ptrdiff_t i = 0; i < ij; ++i) {
    out += A(i, i);
  }
  return out;
}

} // namespace qleve::linalg

#endif // QLEVE_TRACE_HPP
