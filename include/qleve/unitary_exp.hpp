// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file include/qleve/unitary_exp.hpp
///
/// Generic interface to generate unitary matrices from exponentials

#ifndef QLEVE_UNITARY_EXP_HPP
#define QLEVE_UNITARY_EXP_HPP

#include <vector>

#include <qleve/matrix_multiply.hpp>
#include <qleve/svd.hpp>
#include <qleve/tensor.hpp>
#include <qleve/tensor_traits.hpp>
#include <qleve/weighted_matrix_multiply.hpp>

namespace qleve::linalg
{

/// Generates a unitary transformation by exponeniating an anti-Hermitian
/// matrix X which is in turn parametrized by a rectangular off-diagonal
/// matrix kappa.
///
///     ( 0      - kappa^T )
/// X = (                  )
///     ( kappa          0 )
///
qleve::Tensor<2> unitary_exp(const const_real_matrixable auto& A)
{
  const ptrdiff_t nvir = A.extent(0);
  const ptrdiff_t nocc = A.extent(1);

  const ptrdiff_t norb = nvir + nocc;

  const ptrdiff_t k = std::min(nocc, nvir);

  qleve::Tensor<2> out(norb, norb);

  qleve::Tensor<2> acopy(A);

  auto [s, ufull, vtfull] = svd(acopy);

  auto u = ufull.slice(0, k);
  auto vt = vtfull.subtensor({0, 0}, {k, vtfull.extent(1)});

  Tensor<1> costh(cos(s.slice(0, k)));
  Tensor<1> sinth(sin(s.slice(0, k)));

  weighted_gemm("t", "n", 1.0, vt, costh, vt, 0.0, out.subtensor({0, 0}, {nocc, nocc}));
  weighted_gemm("n", "t", 1.0, u, costh, u, 0.0, out.subtensor({nocc, nocc}, {norb, norb}));

  if (nocc > nvir) {
    auto vt_extra = vtfull.subtensor({k, 0}, {nocc, vtfull.extent(1)});
    gemm("t", "n", 1.0, vt_extra, vt_extra, 1.0, out.subtensor({0, 0}, {nocc, nocc}));
  } else if (nvir > nocc) {
    auto u_extra = ufull.slice(k, nvir);
    gemm("n", "t", 1.0, u_extra, u_extra, 1.0, out.subtensor({nocc, nocc}, {norb, norb}));
  }

  weighted_gemm("n", "n", 1.0, u, sinth, vt, 0.0, out.subtensor({nocc, 0}, {norb, nocc}));
  weighted_gemm("t", "t", -1.0, vt, sinth, u, 0.0, out.subtensor({0, nocc}, {nocc, norb}));

  return out;
}

} // namespace qleve::linalg

#endif // QLEVE_UNITARY_EXP_HPP
