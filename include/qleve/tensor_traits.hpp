// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file include/qleve/tensor_traits.hpp
///
/// Type traits for tensors

#ifndef QLEVE_TENSOR_TRAITS_HPP
#define QLEVE_TENSOR_TRAITS_HPP

#include <array>
#include <cassert>
#include <type_traits>

#include <qleve/indexable_traits.hpp>
#include <qleve/layout.hpp>
#include <qleve/pod_types.hpp>
#include <qleve/trait_util.hpp>

namespace qleve
{

// Layout traits

/// Checks if a tensor has a contiguous layout
template <typename T>
using is_contiguous = std::integral_constant<bool, std::is_same_v<layout_t<T>, ContiguousLayout>>;

template <typename T>
inline constexpr bool is_contiguous_v = is_contiguous<T>::value;

/// Checks if tensors have a contiguous layout
template <typename... Ts>
using are_contiguous = bool_and<is_contiguous<Ts>::value...>;

template <typename... Ts>
inline constexpr bool are_contiguous_v = are_contiguous<Ts...>::value;

/// Checks if a tensor has a columnwise layout
template <typename T>
using is_columnwise = std::integral_constant<bool, std::is_same_v<layout_t<T>, ColumnwiseLayout>>;

template <typename T>
inline constexpr bool is_columnwise_v = is_columnwise<T>::value;

/// Checks if tensors have a columnwise layout
template <typename... Ts>
using are_columnwise = bool_and<is_columnwise<Ts>::value...>;

template <typename... Ts>
inline constexpr bool are_columnwise_v = are_columnwise<Ts...>::value;

/// Checks if a tensor has a strided layout
template <typename T>
using is_strided = std::integral_constant<bool, std::is_same_v<layout_t<T>, StridedLayout>>;

template <typename T>
inline constexpr bool is_strided_v = is_strided<T>::value;

/// Checks if tensors have a strided layout
template <typename... Ts>
using are_strided = bool_and<is_strided<Ts>::value...>;

template <typename... Ts>
inline constexpr bool are_strided_v = are_strided<Ts...>::value;

/// Check to see if a tensor is either contiguous or columnwise,
/// since a lot of libraries, like BLAS, only work with one of these two structures
template <typename T>
using is_at_least_columnwise =
    std::integral_constant<bool, is_contiguous<T>::value || is_columnwise<T>::value>;

template <typename T>
inline constexpr bool is_at_least_columnwise_v = is_at_least_columnwise<T>::value;

template <typename... Ts>
using are_at_least_columnwise = bool_and<is_at_least_columnwise<Ts>::value...>;

template <typename... Ts>
inline constexpr bool are_at_least_columnwise_v = are_at_least_columnwise<Ts...>::value;

// Requirements for tensors
template <typename T>
using tensor_rank = std::tuple_size<std::decay_t<decltype(std::declval<T>().shape())>>;

template <typename T>
using tensor_datatype = typename std::remove_reference_t<T>::DataType;

template <typename T, typename U>
using has_type = std::is_same<tensor_datatype<T>, U>;

template <typename T>
using is_size_worthy = std::is_convertible<T, qtens_len_t>;

template <typename T>
inline constexpr bool is_size_worthy_v = is_size_worthy<T>::value;

template <typename... Ts>
using are_size_worthy = bool_and<is_size_worthy<Ts>::value...>;

template <typename... Ts>
inline constexpr bool are_size_worthy_v = are_size_worthy<Ts...>::value;

template <typename T, typename... Ts>
using are_convertible_scalars = bool_and<std::is_convertible<Ts, tensor_datatype<T>>::value...>;

template <typename T, typename... Ts>
inline constexpr bool are_convertible_scalars_v = are_convertible_scalars<T, Ts...>::value;

template <typename T, typename... Ts>
using have_same_datatype =
    bool_and<std::is_same<tensor_datatype<T>, tensor_datatype<Ts>>::value...>;

template <typename T, typename... Ts>
inline constexpr bool have_same_datatype_v = have_same_datatype<T, Ts...>::value;

// Checks for usage of tensors
template <typename T>
using is_nonconst_tensor =
    std::integral_constant<bool, is_indexable<std::remove_reference_t<T>, tensor_datatype<T>>::value
                                     && is_at_least_columnwise<T>::value>;

template <typename T>
concept tensorable = is_nonconst_tensor<T>::value;

template <typename T>
concept real_tensorable = tensorable<T> && std::is_same_v<tensor_datatype<T>, double>;

template <typename T>
using is_nonconst_strided_tensor =
    std::integral_constant<bool,
                           is_indexable<std::remove_reference_t<T>, tensor_datatype<T>>::value>;

template <typename T>
concept strided_tensorable = is_nonconst_strided_tensor<T>::value;

template <typename T>
concept real_strided_tensorable =
    strided_tensorable<T> && std::is_same_v<tensor_datatype<T>, double>;

template <typename... Ts>
using are_nonconst_tensor = bool_and<is_nonconst_tensor<Ts>::value...>;

template <typename T>
using is_const_tensor =
    std::integral_constant<bool,
                           is_const_indexable<std::remove_reference_t<T>, tensor_datatype<T>>::value
                               && is_at_least_columnwise<T>::value>;

template <typename T>
concept const_tensorable = is_const_tensor<T>::value;

template <typename T>
concept real_const_tensorable = const_tensorable<T> && std::is_same_v<tensor_datatype<T>, double>;

template <typename... Ts>
using are_const_tensor = bool_and<is_const_tensor<Ts>::value...>;

template <typename T>
using is_const_strided_tensor = std::integral_constant<
    bool, is_const_indexable<std::remove_reference_t<T>, tensor_datatype<T>>::value>;

template <typename T>
concept const_strided_tensorable = is_const_strided_tensor<T>::value;

template <typename T>
concept real_const_strided_tensorable =
    const_strided_tensorable<T> && std::is_same_v<tensor_datatype<T>, double>;

// Checks for arrays as specializations with rank 1
template <typename T>
using is_nonconst_array = bool_and<is_nonconst_tensor<T>::value, tensor_rank<T>::value == 1>;

template <typename T>
concept arrayable = is_nonconst_array<T>::value;

template <typename T>
concept real_arrayable = arrayable<T> && std::is_same_v<tensor_datatype<T>, double>;

template <typename... Ts>
using are_nonconst_array = bool_and<is_nonconst_array<Ts>::value...>;

template <typename T>
using is_const_array = bool_and<is_const_tensor<T>::value, tensor_rank<T>::value == 1>;

template <typename T>
concept const_arrayable = is_const_array<T>::value;

template <typename T>
concept real_const_arrayable = const_arrayable<T> && std::is_same_v<tensor_datatype<T>, double>;

template <typename... Ts>
using are_const_array = bool_and<is_const_array<Ts>::value...>;

// Checks for matrix as specializations with rank 2
template <typename T>
using is_nonconst_matrixlike = bool_and<is_nonconst_tensor<T>::value, (tensor_rank<T>::value == 2)>;

template <typename T>
concept matrixable = is_nonconst_matrixlike<T>::value;

template <typename T>
concept real_matrixable = matrixable<T> && std::is_same_v<tensor_datatype<T>, double>;

template <typename... Ts>
using are_nonconst_matrixlike = bool_and<is_nonconst_matrixlike<Ts>::value...>;

template <typename T>
using is_const_matrixlike = bool_and<is_const_tensor<T>::value, (tensor_rank<T>::value == 2)>;

template <typename T>
concept const_matrixable = is_const_matrixlike<T>::value;

template <typename T>
concept const_real_matrixable = const_matrixable<T> && std::is_same_v<tensor_datatype<T>, double>;

template <typename... Ts>
using are_const_matrixlike = bool_and<is_const_matrixlike<Ts>::value...>;

// Matrix utility functions

/// Converts from BLAS/Lapack operation codes to the corresponding
/// transposition and conjugation flags
///
/// Mappings:
/// - 'N' -> (false, false)
/// - 'T' -> (true, false)
/// - 'C' -> (true, true)
///
/// \param trcode BLAS/Lapack operation code
/// \return Tuple of transposition and conjugation flags
constexpr std::tuple<bool, bool> matrix_trans_conj(const char* trcode)
{
  if (trcode[0] == 'N' || trcode[0] == 'n') {
    return std::make_tuple(false, false);
  } else if (trcode[0] == 'T' || trcode[0] == 't') {
    return std::make_tuple(true, false);
  } else if (trcode[0] == 'C' || trcode[0] == 'c') {
    return std::make_tuple(true, true);
  } else {
    assert(false && "Invalid transposition code");
    return std::make_tuple(false, false);
  }
}

// Checks for tensor3 as specializations with rank 3
template <typename T>
using is_nonconst_tensor3like =
    bool_and<is_nonconst_tensor<T>::value, (tensor_rank<T>::value == 3)>;

template <typename T>
concept tensor3able = is_nonconst_tensor3like<T>::value;

template <typename T>
concept real_tensor3able = tensor3able<T> && std::is_same_v<tensor_datatype<T>, double>;

template <typename... Ts>
using are_nonconst_tensor3like = bool_and<is_nonconst_tensor3like<Ts>::value...>;

template <typename T>
using is_const_tensor3like = bool_and<is_const_tensor<T>::value, (tensor_rank<T>::value == 3)>;

template <typename T>
concept const_tensor3able = is_const_tensor3like<T>::value;

template <typename T>
concept real_const_tensor3able = const_tensor3able<T> && std::is_same_v<tensor_datatype<T>, double>;

template <typename... Ts>
using are_const_tensor3like = bool_and<is_const_tensor3like<Ts>::value...>;

} // namespace qleve

#endif // QLEVE_TENSOR_TRAITS_HPP
