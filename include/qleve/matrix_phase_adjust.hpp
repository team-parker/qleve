// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file include/qleve/matrix_multiply.hpp
///
/// Generic interface to general matrix multiplies (GEMM)

#ifndef QLEVE_MATRIX_PHASE_ADJUST_HPP
#define QLEVE_MATRIX_PHASE_ADJUST_HPP

#include <algorithm>
#include <complex>
#include <utility>

#include <qleve/blas_interface.hpp>
#include <qleve/detail.hpp>
#include <qleve/pod_types.hpp>
#include <qleve/tensor.hpp>
#include <qleve/tensor_traits.hpp>

namespace qleve
{

/// Regularize a transformation matrix by adjusting the
/// phases so that the largest element of each column is real and
/// positive.
template <matrixable Matrix>
void phase_adjust(Matrix&& A)
{
  const auto [nrows, ncols] = A.shape();

  for (std::size_t j = 0; j < ncols; ++j) {
    auto col = A.pluck(j);
    const auto max_el = std::max_element(col.data(), col.data() + col.size(),
                                         [](auto a, auto b) { return std::abs(a) < std::abs(b); });
    const auto mx = *max_el;
    const auto phi = detail::conj(mx) / std::abs(mx);
    col *= phi;
  }
}

} // namespace qleve

#endif // QLEVE_MATRIX_PHASE_ADJUST_HPP
