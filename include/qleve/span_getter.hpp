// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// -------------------------------------------------------------------
/// \file include/qleve/span_getter.hpp
///
/// Enable std::get to work with std::span with fixed size so that a std::span can be
/// on the right hand side of a structured binding.
///
/// After including this file, you can use the following structure
///
/// std::span<double, 3> sp;
/// const auto [x, y, z] = sp;

#ifndef QLEVE_SPAN_GETTER_HPP
#define QLEVE_SPAN_GETTER_HPP

#ifdef __cpp_lib_span

namespace std
{

template <typename T, size_t N>
struct tuple_size<std::span<T, N>> {
  static constexpr size_t value = N;
};

template <size_t Index, typename T, size_t N>
struct tuple_element<Index, std::span<T, N>> {
  using type = T;
};

template <size_t Index, typename T, size_t N>
T get(const std::span<T, N>& x)
{
  static_assert(Index < N, "Out of index");
  return x[Index];
}

} // namespace std

#endif // __cpp_lib_span

#endif // QLEVE_SPAN_GETTER_HPP
